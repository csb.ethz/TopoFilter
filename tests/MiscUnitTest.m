function tests = MiscUnitTest()
%MISCUNITTEST Unit tests for miscleanous package functions.
%
% Run with:
%   runtests('MiscUnitTest')
%
    tests = functiontests(localfunctions);
end

%% Tests

function testParamCouplingApply(testCase)

    couplingCa = {
        3	[4,5];
        [4,5]	3;
        [3,6]	1:8;
        1	2:8};
    d = 10; % two extra zero dimensions
    pAll = [ones(1,8) 0 0];
    p0 = [ 0 0 0 0 1 1 0 0 0 0 ]; % => p0
    p1 = [ 1 0 1 0 0 0 0 0 0 0 ]; % => pAll
    p2 = [ 0 0 1 0 0 1 0 0 0 0 ]; % => pAll
    p3 = [ 0 0 0 1 1 1 0 0 0 0 ]; % => pAll because first [4 5] => 3, then [3 6] => 1:8
    p4 = [ 0 0 1 1 1 0 0 0 0 0 ]; % => p4
    p5 = [ 0 0 0 1 1 0 0 0 0 0 ]; % => p4

    % test input validation
    verifyError(testCase, @() paramCouplingApply(couplingCa, true(1,2)), '');
    verifyError(testCase, @() paramCouplingApply(ones(1,d-1,2), true(1,d)), '');

    cM = paramCouplingCaToMatrix(couplingCa, d);
    % test coupling
    verifyEqual(testCase, paramCouplingApply(cM, p0), logical(p0));
    verifyEqual(testCase, paramCouplingApply(cM, p1), logical(pAll));
    verifyEqual(testCase, paramCouplingApply(cM, p2), logical(pAll));
    verifyEqual(testCase, paramCouplingApply(cM, p3), logical(pAll));
    verifyEqual(testCase, paramCouplingApply(cM, p4), logical(p4));
    verifyEqual(testCase, paramCouplingApply(cM, p5), logical(p4));

    % test projectable mask
    verifyEqual(testCase, paramCouplingApply(cM, p5, p5), logical(p5));
    verifyEqual(testCase, paramCouplingApply(cM, p1, p5), p1 | p5);

end

function testParamCouplingValidate(testCase)
    couplingCa = {
        3	[4,5];
        [4,5]	3;
        [3,6]	1:8;
        1	2:8};

        d = 10; % two extra zero dimensions
        pAll = [ones(1,8) 0 0];
        p0 = [0 ones(1,7) 0 0]; % 1 not projectable => error, rule 3
        p1 = [0 1 0 1 1 1 1 1 0 0]; % 1, 3 not projectable => error, rule 2
        p2 = [0 1 1 1 1 0 1 1 0 0]; % 1, 6 not projectable => ok (rules 3, 4 are inactive)

        verifyWarningFree(testCase, @() paramCouplingValidate(couplingCa, pAll))
        verifyError(testCase, @() paramCouplingValidate(couplingCa, p0), 'PARAMCOUPLINGVALIDATE:InvalidParamCoupling');
        verifyError(testCase, @() paramCouplingValidate(couplingCa, p1), 'PARAMCOUPLINGVALIDATE:InvalidParamCoupling');
        verifyWarningFree(testCase, @() paramCouplingValidate(couplingCa, p2))

end