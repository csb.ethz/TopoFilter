function [coupledParameterIdxsCell, coupledColIdxsCell] = coupleParamIdxs(...
    paramCouplingApplyFcn, couplingMatrix, fixedIdxs, projectableIdxs,...
    colIdxs, d, varargin)
%
    nProjectable = numel(projectableIdxs);
    assert(nProjectable == numel(colIdxs),...
        'Invalid input arguments: inconsistent size of both indices vectors.');
    if isempty(varargin)
        couplingRank = 1;
    else
        couplingRank = varargin{1};
        assert(isposint(couplingRank),...
            'copulingRank has to be a positive integer.')
    end

    locCouplingP = ProjectionSet();

    if nProjectable > 0

        % convert indices vectors to logical vectors
        fixedVec = false(1,d);
        fixedVec(fixedIdxs) = true;

        projectableVec = false(1,d);
        projectableVec(projectableIdxs) = true;

        %% create cell array w/ coupled parameters for projectable indices
        % create all unique projectable params combinations up to the couplingRank
        couplingRank = min(couplingRank, nProjectable); % can only combine rows up to n0
        for r=1:couplingRank
            % generate all rank r projections
            I = nchoosek(1:nProjectable, r);
            nI = size(I,1);
            for i=1:nI
                currProjectableIdxs = projectableIdxs(I(i,:));
                % current rank r projection vector
                projVec = fixedVec;
                % project chosen rank r params
                projVec(currProjectableIdxs) = true;
                % apply coupling
                projVecThen = paramCouplingApplyFcn(couplingMatrix, projVec, projectableVec);
                locCouplingP.add(projVecThen(projectableIdxs));
            end
        end

    end % if nProjectable > 0 ...

    % prep output indices sets
    nCouplings = locCouplingP.num;
    coupledParameterIdxsCell = cell(1,nCouplings);
    coupledColIdxsCell = cell(1,nCouplings);
    for i=1:nCouplings
        icouple = logical(locCouplingP.at(i));
        coupledParameterIdxsCell{i} = projectableIdxs(icouple);
        coupledColIdxsCell{i} = colIdxs(icouple);
    end

end
