function [ model ] = loadModel( settingsOrFilename )
%LOADMODEL ... if not loaded before.
%
%   `IQMmodel` function wrapper.
%
    %
    if ischar(settingsOrFilename)
        settings.modelFilename = settingsOrFilename;
    elseif ~isstruct(settingsOrFilename)
        error('LOADMODEL:IllegalArgument', 'Expecting model file name or settings structure.');
    else
        settings = settingsOrFilename;
    end
    if ~isfield(settings, 'model') || isempty(settings.model)
        if ~isfield(settings, 'modelFilename') || isempty(settings.modelFilename)
            error('LOADMODEL:MissingModel', 'Please specify `modelFilename` string.');
        else
            model = IQMmodel(settings.modelFilename);
        end
    else
        if ~(isIQMmodel(settings.model) || (exist(settings.model,'file') == 3))
            error('LOADMODEL:InvalidModel', 'The `model` field must be either a valid `IQMmodel`, or a mex-compiled model.');
        end
        model = settings.model;
    end


end
