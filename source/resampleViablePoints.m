function [viablePoints, cost, resampled] = resampleViablePoints(localModifiableParameterIdxs, viablePoints, cost, threshold, options, paramSpecs)
%RESAMPLEVIABLEPOINTS If necessary carries out global or local re-sampling with
%   `getViablePoints` method.
%
%   Re-sampling occures if current number of paramters is less or equal to
%   a thresold of maximum of relative and absolute number of parameters.
%
%   Returns::
%       `viablePoints`, `cost`: matrix of newly re-sampled viable points and
%           a vector of evaluated costs as returned by `getViablePoints`.
%       `resampled`: flag indicating wether points were re-sampled (`true`), or
%           just passed on from inputs
%
    %
    resampled = false;
    if isempty(localModifiableParameterIdxs) % nothing to re-sample
        % viablePoints, cost: in = out
        return
    end
    n = size(viablePoints,1);
    if (n < options.nvmin)
        fprintf('[INFO] resampleViablePoints, resampling: current number of viable points: %d <= required number of viable points: %d.\n', n, options.nvmin);
        bmin = paramSpecs.bmin(localModifiableParameterIdxs);
        bmax = paramSpecs.bmax(localModifiableParameterIdxs);
        [viablePoints, cost, resampled] = getViablePoints(threshold, bmin, bmax, options.nmont, options.nelip, 'viablePoints', viablePoints, 'cost', cost, 'dropMontPoints', options.dropMontPoints);
    end
end
