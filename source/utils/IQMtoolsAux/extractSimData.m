function [simdata, tps] = extractSimData(nsol, name, expdata, varargin)
%EXTRACTSIMDATA Extract from numerical simulations `nsol` data for observable
% `name`, corresponding to experimental data `expdata`. Extracted data is
% then used to calculate value of the cost function.
%
% Here, the numerical simluation data of the variable `name` is interpolated at
% time points of the experimental measurements.
%
% Inputs:
%     expdata (numeric array): 4 column matrix with observables' simdata; each
%                              row is a quadruplet of: time point, measurement
%                              value, min value, and max value (taken
%                              measurement error into account)
%     nsol (struct): IQMPsimulate output structure
%     name (string): name of the observable
%     Optional arguments for custom implementations (here, not used)
%
% Outputs:
%     simdata (numeric vector): vector of simulation data
%     tps (numeric vector): optional vector of time points, corresponding to
%                           `simdata` points (it is not used in calculation of
%                           the cost)
%

    tps = expdata(:,1);
    simt = nsol.time';
    simd = getIQMsimulateValues(nsol, name);
    % interpolate simulation values at measurements time points
    simdata = interp1(simt, simd, tps);
end
