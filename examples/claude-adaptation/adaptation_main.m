
function [] = adaptation_main( varargin )

%% Initialise
numWorkers = 0;
try
    ppool = gcp('nocreate'); % If no pool, do not create new one.
    if ~isempty(ppool)
        numWorkers = ppool.NumWorkers;
    end
catch ME
    warning('Parallel Computing Toolbox not found.\nThe follwoing error was caught while calling gcp:\n\t%s: %s\n',ME.identifier, ME.message);
end
rng('shuffle')


outDirname = 'results';

%%  We choose the interaction signs to define which model will be used
%modelsignsCellArray = {'000000000', '000001100', '000101110', '001110100'};
modelsignsCellArray = {'000101110', '001110100'};
%modelsignsCellArray = {'000101110'};

% Note: the original example was run in parallel over models, however you diary
%       will not log properly if you do running on local parpool, hence using
%       here TopoFilter parallelization instead
%parfor (iModelsigns = 1:length(modelsignsCellArray), numWorkers)
for (iModelsigns = 1:length(modelsignsCellArray))

    modelsigns = modelsignsCellArray{iModelsigns};

    fprintf('\n################################################\n')
    fprintf('# Model %s', modelsigns)
    fprintf('\n################################################\n')

    modelDirname = fullfile(outDirname,modelsigns);
    mkdir(modelDirname);
    loaded = load([ 'initpoint_' modelsigns '.mat']);
    initpoint = loaded.x;
    modelname = ['model_' modelsigns '.txtbc'];

%%
    timestamp = @() datestr(now, 'yyyy.mm.dd HH:MM');

    fprintf('Program started at %s\n',timestamp());
    tic;

    start = datestr(now, 'yymmdd_HHMMSS');

    %% Tunable experiments setting if not provided via function options (+ validation)
    parser = inputParser;

    parser.FunctionName = 'ADAPTATION_MAIN: ';

    % Results file
    addParameter(parser, 'outputFilename', fullfile(modelDirname,...
       sprintf('%s.mat',datestr(now, 'yymmdd_HHMMSS'))), @ischar);

    % Number of runs
    addParameter(parser, 'numRuns', 1, @isposint);
    % Dry run: calc only threshold (and p0, if not given)
    addParameter(parser, 'dryRun', false, @islogical);

    % Sample sizes: not directly options itself, but used below for setting
    % the actual options.
    % Maximum number of cost function evauations in one sampling (roughly).
    addParameter(parser, 'nfeval', 6e2, @isposint);
    % Ratio of nfeval's going respectively into nmont and nelip
    addParameter(parser, 'rfeval', [1 2], @(x) numel(x)==2 && all(isposint(x)));
    % Proportion of nfeval giving maximal size of a sample of viable points
    addParameter(parser, 'pvmax', 1.1, @(x) isnumeric(x) && (x > 0));

    % Flags for backtracking and recursive search
    addParameter(parser, 'backtrack', false, @islogical);
    addParameter(parser, 'recursive', true, @islogical);

    % Flag for saving viable points after each sampling
    addParameter(parser, 'saveViablePoints', true, @islogical);
    % Flag for removing the MEX files after the runs
    % Set to `false` if you're independently running `TFmain` using the same
    % model file (e.g. when aditionally externally parallelising experiments)
    addParameter(parser, 'cleanupMexFiles', false, @islogical);

    % Parallel computation level
    % 0 - none
    % 1 - over the OAT viability checks in getViableProjections
    % 2 - over projections (in the recursive search) and OAT checks (pre-recursion)
    % 3 - over runs
%    addParameter(parser, 'parallelize', 0, isinrange(3));
    addParameter(parser, 'parallelize', 1, isinrange(3));

    % Param space exploration specification
    addParameter(parser, 'paramSpecsFilename', './paramSpecs.txt', @ischar);
    addParameter(parser, 'paramCouplingFcn', @adaptation_paramCoupling, @isf);

    % Init param value
    % 0 - from the paramters specification, or from the model (in that order)
    addParameter(parser, 'p0', initpoint, @isnumeric);

    addParameter(parser, 'extractSimDataFcn', @adaptation_extractSimData, @isf);
    addParameter(parser, 'calcCostFcn', @adaptation_calcCost, @isf);
    addParameter(parser, 'calcThresholdFcn', @adaptation_calcThreshold, @isf);

%             % ODE integrator options
%             % options structure with as specified in `IQMPsimulate`
%             simopts.reltol = 1e-8;
%             simopts.abstol = 1e-8;
% %             simopts.maxnumsteps = 1e6;
%             addParameter(parser, 'odeIntegratorOptions', simopts, @isstruct);

    addParameter(parser, 'odeIntegratorsolNrTimepoints', 3000, @isint);
    % number of integration attempts in case of an error; each next with
    % 10-times previous `maxnumsteps` (default: `1e5`)
    addParameter(parser, 'odeIntegratorMaxnumstepsTry', 1, @isposint);


    % Parse the input arguments
    parse(parser, varargin{:});
    p = parser.Results;


    %% Model, experiment and data files
    % SBML (TXT/XML) or IQMmodel (TXT/Matlab) format
    p.modelFilename = modelname;
    % IQMexperiment format; string or cell array of string;
    % one experiment expected per each data file
    p.expFilename = {'./experiment.exp'};

    % IQMmeasurement format (CSV or XLS); string or cell array of string;
    % in case of XLS each sheet (except for first) is treated like a single CSV
    p.expDataFilename = {'./expData.xls'};
    % Indices for data files to use (helpful w/ mult-sheet XLS); [] = all
    p.expDataIdxs = [];

    %% Sample sizes
    % Split nfeval according to given proportions
    nfevalv = splitlrm(p.nfeval,  p.rfeval/sum(p.rfeval));
    % Maximum target sample size in the Markov chain Monte Carlo (MCMC) sampling
    p.nmont = nfevalv(1);
    % Guiding number of model evaluations in the Ellipsoids-based sampling
    p.nelip = nfevalv(2);
    % Minimum nr of viable parameter samples before (re-)sampling
    % Try to ensure that for every projection found there is enough points to
    % directly call Volint; this is not a guarantee as (re)-sampling may still
    % fail to find  the minimum required number of points
    dim_max = length(initpoint);
    p.nvmin = 10*(dim_max+1);
    % Maximum nr of viable parameter samples before sparsifying
    p.nvmax = ceil(p.pvmax*sum(nfevalv));

    %% Run the actual program
    diary(fullfile(modelDirname,...
        sprintf('parall0_%ds_%s.txt',p.nfeval,datestr(now, 'yymmdd_HHMM'))));

    [ runs, ~, settings ] = TFmain(p);

    %% Save results
    save_TFmain(runs, settings);

    diary off;

    %% Cleanup
    fprintf('Program finished at %s\n',timestamp());

end % for

end % function

function save_TFmain(runs, settings)
    save(settings.outputFilename, 'runs');
end
