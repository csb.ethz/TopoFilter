function tests = ProjectionSetTest()
%PROJECTIONSETTEST Unit tests for ProjectionSet class.
%
% Run with:
%   runtests('ProjectionSetTest')
%
    tests = functiontests(localfunctions);
end

%% Tests

function testInitEmptySet(testCase)
    vP = ProjectionSet();
    verifyEqual(testCase, vP.num, 0, 'Empty count not 0.');
    verifyEqual(testCase, vP.dim, 0, 'Dimensions does not agree.');

    vP = ProjectionSet(0);
    verifyEqual(testCase, vP.num, 0, 'Empty count not 0.');
    verifyEqual(testCase, vP.dim, 0, 'Dimensions does not agree.');

    d = 2;
    vP = ProjectionSet(d,0);
    verifyTrue(testCase, isempty(vP));
    verifyEqual(testCase, vP.num, 0, 'Empty count not 0.');
    verifyEqual(testCase, vP.dim, d, 'Dimensions does not agree.');

    verifyError(testCase, @() vP.at(1), 'PROJECTIONSSET:AT');
end

function testSetWithZeroProjection(testCase)
    p = [ 0 0 0 ];
    vP = ProjectionSet(numel(p),1);
    verifyTrue(testCase, vP.add(p), 'Couldn''t add zero projection');

    p1 = [ 1 0 0 ];
    p2 = [ 1 0 1 ];
    vP2 = ProjectionSet(numel(p1),2);
    verifyTrue(testCase, vP2.add(p1));
    verifyTrue(testCase, vP2.add(p2));

    addedIdxs = vP.addAll(vP2);
    verifyEqual(testCase, vP.num, 3, 'Wrong projections count.');
    verifyEqual(testCase, addedIdxs, transpose(1:vP2.num), 'Wrong added projections indices.');
end

function testAdd(testCase)
    p1 = [ 1 0 0 ];
    p2 = [ 1 0 1 ];
    p3 = [ 1 1 0 ];
    p4 = [ 0 1 0 ];
    vP = ProjectionSet(numel(p1),2);

    pTooSmall = [ 1 1 ];
    verifyError(testCase, @() vP.add(pTooSmall), 'PROJECTIONSSET:ADD'); % incompatible projection dim
    verifyError(testCase, @() vP.add(p1,pTooSmall), 'PROJECTIONSSET:ADD'); % incompatible param sample dim

    verifyTrue(testCase, vP.add(p1));
    verifyEqual(testCase, vP.num, 1, 'Wrong projections count.');
    verifyTrue(testCase, vP.add(transpose(p2)), 'Transposition shouldn''t matter.');
    verifyEqual(testCase, vP.num, 2, 'Wrong projections count.');
    verifyTrue(testCase, vP.add(p3));
    verifyEqual(testCase, vP.num, 3, 'Wrong projections count.');
    verifyFalse(testCase, vP.add(p2));
    verifyEqual(testCase, vP.num, 3, 'Wrong projections count.');
    verifyTrue(testCase, vP.add(p4), 'Should be able to add strictly smaller projection.');
    verifyEqual(testCase, vP.rowmat, logical([p1; p2; p3; p4]), 'Addition order is not preserved.');
end

function testAddParam(testCase)
    d = 3;
    p1 = [1 0 0];
    p2 = [0 1 0];
    ps2 = [Inf 0 pi];

    projectionSetClsCa = {@ProjectionSet, @MaxProjectionSet};
    for i=1:numel(projectionSetClsCa)
        projectionSetCls = projectionSetClsCa{i};
        vP = projectionSetCls();
        verifyTrue(testCase, vP.add(p1));
        verifyFalse(testCase, vP.add(p1));
        verifyTrue(testCase, vP.add(p2,ps2));
        verifyEqual(testCase, vP.num, 2, 'Wrong projections count.');
        [p1ret, ps1ret] = vP.at(1);
        verifyEqual(testCase,  p1ret, logical(transpose(p1)));
        verifyEqual(testCase,  ps1ret, nan(d,1));
        [p2ret, ps2ret] = vP.at(2);
        verifyEqual(testCase,  p2ret, logical(transpose(p2)));
        verifyEqual(testCase,  ps2ret, transpose(ps2));
    end
end

function testMaxProjectionSetPunique(testCase)
    p1 = [1 0 0];
    p2 = [0 1 0];
    p3 = [0 0 1];
    singletons = [p3; p2; p1];
    singletonsSroted = sortrows(singletons);
    p23 = [0 1 1];
    singletonsWithDouble = [singletons; p23];
    maxSingletonsWithDouble = [p1; p23];
    maxSingletonsWithDoubleSorted = sortrows(maxSingletonsWithDouble);
    rowmaxSingletonsWithDouble = [p23; p23; p1; p23];

    % stable
    [C, IA, IC] = MaxProjectionSet.punique(singletons, 'stable');
    verifyEqual(testCase, C, singletons);
    verifyEqual(testCase, singletons(IA,:), C);
    verifyEqual(testCase, C(IC,:), singletons);
    [C, IA, IC] = MaxProjectionSet.punique(singletonsWithDouble, 'stable');
    verifyEqual(testCase, C, maxSingletonsWithDouble);
    verifyEqual(testCase, singletonsWithDouble(IA,:), C);
    verifyEqual(testCase, C(IC,:), rowmaxSingletonsWithDouble);
    % sorted (default)
    [C, IA, IC] = MaxProjectionSet.punique(singletons, 'sorted');
    verifyEqual(testCase, C, singletonsSroted);
    verifyEqual(testCase, singletons(IA,:), singletonsSroted);
    verifyEqual(testCase, C(IC,:), singletons);
    [C2, IA2, IC2] = MaxProjectionSet.punique(singletons);
    verifyEqual(testCase, C, C2);
    verifyEqual(testCase, IA, IA2);
    verifyEqual(testCase, IC, IC2);
    [C, IA, IC] = MaxProjectionSet.punique(singletonsWithDouble, 'sorted');
    verifyEqual(testCase, C, maxSingletonsWithDoubleSorted);
    verifyEqual(testCase, singletonsWithDouble(IA,:), C);
    verifyEqual(testCase, C(IC,:), rowmaxSingletonsWithDouble);
    [C2, IA2, IC2] = MaxProjectionSet.punique(singletonsWithDouble);
    verifyEqual(testCase, C, C2);
    verifyEqual(testCase, IA, IA2);
    verifyEqual(testCase, IC, IC2);
end

function testAddAll(testCase)
    vPA(3) = ProjectionSet();
    vP = ProjectionSet();
    vP.addAll(vPA);
    verifyTrue(testCase, isempty(vP));


    % TODO: add p* into testCase.TestData in setupOnce and split into subtests
    d = 3;
    p1 = [1 0 0];
    p2 = [0 1 0];
    p3 = [0 0 1];
    p12 = [1 1 0];
    p13 = [1 0 1];
    p23 = [0 1 1];
    p123 = [1 1 1];


    vP1 = ProjectionSet(d,1);
    verifyEqual(testCase, vP1.num, 0, 'Wrong projections count: vP1.');
    verifyTrue(testCase, vP1.add(p1));
    verifyEqual(testCase, vP1.num, 1, 'Wrong projections count: vP1.add(p1).');


    vPSmaller = ProjectionSet(d-1,2);
    verifyTrue(testCase,vPSmaller.add(p1(1:(d-1))));
    verifyTrue(testCase,vPSmaller.add(p2(1:(d-1))));

    vPSingletons = ProjectionSet();
    vPSingletons.addAll(vP1);
    verifyError(testCase, @() vPSingletons.addAll(vPSmaller), 'PROJECTIONSSET:ADDALL'); % incompatible param sample dim
%    fprintf('vP2: %s\n',mat2str(vP2.rowmat));
    verifyFalse(testCase, vPSingletons.add(p1));
    verifyTrue(testCase, vPSingletons.add(p2));
    verifyEqual(testCase, vPSingletons.num, 2, 'Wrong projections count.');
    verifyEqual(testCase,  vPSingletons.at(2), logical(transpose(p2)));
    verifyTrue(testCase, vPSingletons.add(p3));
    vPSingletonsRowmat = logical([p1; p2; p3]);
    % in order of one-by-one addition
    verifyEqual(testCase, vPSingletons.rowmat, vPSingletonsRowmat);
%    fprintf('vP2: %s\n',mat2str(vP2.rowmat));


    vPA(1) = vP1;
    vPA(2) = vPSingletons;
    vPA(3) = vPSingletons;
    vPSum = ProjectionSet();
    vPSum.addAll(vPA);
%    fprintf('vP3: %s\n',mat2str(vP3.rowmat));
    verifyEqual(testCase, vPSum.num, vPSingletons.num, 'Wrong projections count: duplicates.');
    %verifyEqual(testCase, sum(arrayfun(@(vP) vP.num, vPA)) - vPSum.num, vP1.num + vPSingletons.num, 'Wrong projections count: duplicates.');
    verifyFalse(testCase, vPSum.add(p1));
    verifyFalse(testCase, vPSum.add(p2));
    verifyFalse(testCase, vPSum.add(p3));
    verifyTrue(testCase, vPSum.add(p123));
    verifyEqual(testCase, vPSum.num, vPSingletons.num+1, 'Wrong projections count: added one.');
    verifyTrue(testCase, isempty(vPSum.addAll(vPA)), 'Wrong added projections indices: nothing new added.');
%    fprintf('vP3: %s\n',mat2str(vP3.rowmat));


    vPSum = ProjectionSet();
    vPSum.addAll(vP1);
%    fprintf('vP3: %s\n',mat2str(vP3.rowmat));
    [Iadded, ~, IIaddedDuplicates, IselfPresent] = vPSum.addAll(vPSingletons);
%    fprintf('vP3: %s\n',mat2str(vP3.rowmat));
%    fprintf('Added idxs: %s\n',mat2str(addedIdxs));
    verifyEqual(testCase, vPSum.num, vPSingletons.num, 'Wrong number of projections after addition.');
    verifyEqual(testCase, numel(Iadded), vPSingletons.num-vP1.num, 'Wrong number of added indices.');
    verifyEqual(testCase, numel(Iadded), size(vPSingletons.at(Iadded),2));
    verifyEqual(testCase, IselfPresent, 1, 'Expecting the previously present singleton to be identified as attempted to be added.');
    verifyEqual(testCase, Iadded, 1+IIaddedDuplicates, 'Added all but the first singleton, with no duplicates.');
    % note: in order of addition
    pvPm1mat = transpose(logical([p2; p3]));
    verifyEqual(testCase,vPSingletons.at(Iadded), pvPm1mat, 'Wrong added projections indices: added p2 and p3.');
    vP4 = vPSingletons.reduceTo(Iadded);
%    fprintf('vP4: %s\n',mat2str(vP4.rowmat));
    verifyEqual(testCase, vPSum.num, vPSingletons.num, 'Wrong projections count: reduction original.');
    verifyEqual(testCase, vP4.num, vPSingletons.num-vP1.num, 'Wrong projections count: reduced.');
    verifyEqual(testCase,vP4.rowmat,transpose(pvPm1mat));

    vP5 = MaxProjectionSet();
    vP5.add([0 0 0]); % will not be removed by further add() and addAll(), only by reduce()
    [Iadded, Ipresent, IIaddedDuplicates, IselfPresent] = vP5.addAll(vP1);
    verifyEqual(testCase, Iadded, (1:vP1.num), 'Expected to add all non-zero, non-duplicate projections.');
    verifyTrue(testCase, isempty(Ipresent), 'Expected none of the added non-zero projections to be already present in a zero projection singleton.');
    verifyTrue(testCase, isempty(IselfPresent), 'Expecting none of the previously present projections to be identified as attempted to be added.');
    verifyEqual(testCase, Iadded(IIaddedDuplicates), Iadded, 'No duplicates expected');
    verifyEqual(testCase, vP5.num, 2, 'Wrong projections count: added singleton to a smaller zero projection, both should be present.');
    vP5.addAll(vPSingletons);
    verifyEqual(testCase, vP5.num, vPSingletons.num+1, 'Wrong projections count: all added singletons are maximal and empty was in the set before.');
    vP5 = vP5.reduce();
    verifyEqual(testCase, vP5.num, vPSingletons.num, 'Wrong projections count: all singletions are maximal.');

    vPDoubles = ProjectionSet();
    verifyTrue(testCase, vPDoubles.add(p12));
    verifyTrue(testCase, vPDoubles.add(p23));
    verifyTrue(testCase, vPDoubles.add(p13));
    vPDoublesRowmat = logical([p12; p23; p13]);
    % in order of one-by-one addition
    verifyEqual(testCase, vPDoubles.rowmat, vPDoublesRowmat);

    vPA(1) = vPDoubles;
    vPA(2) = vP1;
    vPA(3) = vPSingletons;
    vPARowmat = [vPDoubles.rowmat; vP1.rowmat; vPSingletons.rowmat];
    vPSum = ProjectionSet();
    [Iadded, ~, IIaddedDuplicates] = vPSum.addAll(vPA);
    verifyEqual(testCase, vPSum.rowmat, vPARowmat(Iadded,:));
    verifyEqual(testCase, vPARowmat(Iadded(IIaddedDuplicates),:), vPARowmat, 'Indexing with duplicates error: expecting all projections when adding to the empty projection set.');
    %
    vPSum2 = vP1.copy();
    [Iadded, Ipresent, IIaddedDuplicates, IselfPresent] = vPSum2.addAll(vPA);
    verifyEqual(testCase, vPSum2.rowmat(1:(vP1.num),:), vP1.rowmat, 'Expecting previously present projections head.');
    verifyEqual(testCase, vPSum2.rowmat((vP1.num+1):end,:), vPARowmat(Iadded,:), 'Expecting all actually added projections tail.');
    verifyEqual(testCase, vPARowmat(Ipresent,:), vPARowmat((vPDoubles.num+(1:2)),:), 'Expecting previously present singleton duplicates.');
    verifyEqual(testCase, vPARowmat(Iadded(IIaddedDuplicates),:), [vPARowmat(1:(vPDoubles.num),:); vPARowmat((vPDoubles.num+3):end,:)], 'Expecting all projections except previously present singleton duplicates.');
    verifyEqual(testCase, IselfPresent, 1, 'Expecting the previously present singleton to be identified as attempted to be added.');


    vP5 = MaxProjectionSet();
    vP5.addAll(vPSum);
    % note: 'stable' rowmat
    verifyEqual(testCase, vP5.rowmat, vPDoubles.rowmat, 'Wrong projections count: all doubles are maximal.');
    verifyTrue(testCase, vP5.add(p123));
    verifyEqual(testCase, vP5.rowmat, [vPDoubles.rowmat; logical(p123)], 'Wrong projections count: doubles are not removed in addition of a triple.');
    vP5 = vP5.reduce();
    verifyEqual(testCase, vP5.rowmat, logical(p123), 'Wrong projections count: triple is the only maximal.');

    vP5 = MaxProjectionSet();
    verifyTrue(testCase, vPSum.add(p123));
    vP5.addAll(vPSum);
    verifyEqual(testCase, vP5.rowmat, logical(p123), 'Wrong projections count: triple is the only maximal.');
end
