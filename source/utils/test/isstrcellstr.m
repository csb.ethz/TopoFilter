function [ bool ] = isstrcellstr(x)
%ISSTRCELLSTR Test if `x` is a string or cell array of strings.
%
    bool = ischar(x) || iscellstr(x);

end
