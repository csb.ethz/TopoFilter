function [ cost, residuals ] = adaptation_calcCost( x, y, npar, varargin )

if (debuglevel > 1)
    fprintf('[DEBUG] adaptation_calcCost, npar = %d.\n',npar);
end

x = x(:);
y = y(:);
residuals = x - y;

if length(x) == 3 && abs(x(2) - x(1))>0,
    O1    = x(1);
    Opeak = x(2);
    O2    = x(3);
    S     = abs((Opeak - O1)/(O1*0.2)); % sensitivity
    P     = 1/abs((O2 - O1)/(O1*0.2)); %Precision
    cost = (1/S)^2 + (10/P)^2 + (0.1/O2)^2;
    % O2 > 0.1, S > 1 and P/10 > 1: cost < 3
else
    cost = 1e6;
end

end
