function [ bool ] = isfhandle(x)
%ISFHANDLE Shorthand for: isa(x,'function_handle')
%
    bool = isa(x,'function_handle');
end
