function [viablePoints, cost, sampled] = getViablePoints(varargin)
%GETVIABLEPOINTS Wrapper for the parameter space exploration sampling using
%   the HYPERSPACE methods.
%
%   If local sampling is done (`local` input argument flag is true), only
%   a ellipsoinds based re-sampling is done (`ELexp`), using a set of provided
%   `viablePoints`. Otherwise global sampling is done first with the
%   out-of-equlibrium adaptive MCMC (cf. `MCexp`), starting from first of
%   `viablePoints`.
%
%   Sampling can be carried out in both log or normal space.
%

    %% Specify input requirements
    parser = inputParser;
    parser.FunctionName = 'GETVIABLEPOINTS: ';
    addRequired(parser, 'viabilityThreshold', @isnumeric);
    addRequired(parser, 'bmin', @isnumeric);
    addRequired(parser, 'bmax', @isnumeric);
    addRequired(parser, 'nmont', @isposint);
    addRequired(parser, 'nelip',  @isposint);
    addParameter(parser, 'viablePoints', [], @isnumeric);
    addParameter(parser, 'cost', [], @isnumeric);
    addParameter(parser, 'local', false, @islogical);
    addParameter(parser, 'dropMontPoints', true, @islogical);

    %% Parse the input arguments
    parse(parser, varargin{:});
    viabilityThreshold = parser.Results.viabilityThreshold;
    bmin = parser.Results.bmin';
    bmax = parser.Results.bmax';
    nmont = parser.Results.nmont;
    nelip = parser.Results.nelip;
    local = parser.Results.local;
    dropMontPoints = parser.Results.dropMontPoints;
    viablePoints0 = parser.Results.viablePoints;
    cost0 = parser.Results.cost;


    nViablePoints = size(viablePoints0,1);
    if isempty(nViablePoints)
        error('GETVIABLEPOINTS:NoPointsProvided', 'Expecting at least one viable point.');
    end
    assert(nViablePoints == numel(cost0));
    % assert(size(unique(viablePoints0,'rows'),1) == nViablePoints);

    rng('shuffle'); % Rem: required if submitted to a cluster node under parfor


    sampled = false;

    %% Monte Carlo exploration with Elias method
    if ~local && nmont > 0
        % TODO: it would be cleaner to move multi-points start to HYPERSPACE
        % (would make MCexp interface same as ELexp one)
        nmontv = eqdrawers(nmont, nViablePoints);
        % Note: if `nmont <= nViablePoints` then `nmontv = [1 ... 1 0 ... 0]`,
        %       with `nmont` 1s and `nViablePoints - nmont` 0s
        n_nmontv_pos = min(nmont,nViablePoints);
        nmontCumsum = 0;
        newViablePoints = zeros(nmont,size(viablePoints0,2));
        newCost = nan(nmont,1);
        % randomly pick viable points
        iViablePoint = randperm(nViablePoints);
        for i=1:n_nmontv_pos % TODO: consider parfor (then w/o re-distribution of missing samples)
            nmontTarget = nmontv(i); % nmontTarget >= 1
            iVp = iViablePoint(i);
            x0 = viablePoints0(iVp,:);
            if nmontTarget == 1
                Vm.V = x0;
                Vm.cost = cost0(iVp);
            else
                fprintf('[INFO] getViablePoints, MC sampling viable points %d to %d.\n',(nmontCumsum+1),(nmontCumsum+nmontTarget));
                try
                    Vm = MCexp('evalModel',viabilityThreshold,x0,bmax,bmin,nmontTarget);
                    sampled = true;
                catch ME
                    Vm.V = x0;
                    Vm.cost = cost0(iVp);
                    warning('GETVIABLEPOINTS:MCexpFail','MC sampling failed with the following error:\n\t%s: %s\n',ME.identifier, ME.message);
                end
            end
            nmontFound = size(Vm.V,1);
            fprintf('[INFO] getViablePoints, MC sampling found %d new points (on top of the 1 init point).\n',nmontFound-1);
            nmontMissing = nmontTarget-nmontFound;
            if nmontMissing > 0 % re-distribute missing samples among remaining points
                nmontv((i+1):end) = nmontv((i+1):end) + eqdrawers(nmontMissing, nViablePoints-i);
                nmontv(i) = nmontFound; % (not used) book real count
            end
            % assert(size(unique(Vm.V,'rows'),1) == nmontFound);
            iStart = nmontCumsum+1;
            iEnd = nmontCumsum+nmontFound;
            newViablePoints(iStart:iEnd,:) = Vm.V;
            newCost(iStart:iEnd) = Vm.cost;
            nmontCumsum = nmontCumsum+nmontFound;
        end
        viablePoints = newViablePoints(1:nmontCumsum,:);
        cost = newCost(1:nmontCumsum);
        fprintf('[INFO] getViablePoints, MC sampling found total %d viable points.\n',size(viablePoints,1));
        % assert(size(unique(viablePoints,'rows'),1) == nmontCumsum);
    else
        viablePoints = viablePoints0;
        cost = cost0;
    end


    %% Ellipsoid exploration with Elias method
    try
        Ve = ELexp('evalModel',viabilityThreshold,viablePoints,bmax,bmin,nelip);
        sampled = true;
    catch ME
        Ve.V = viablePoints0;
        Ve.cost = cost0;
        if dropMontPoints, sampled = false; end
        warning('GETVIABLEPOINTS:ELexpFail','Ellipsoids-based sampling failed with the following error:\n\t%s: %s\n',ME.identifier, ME.message);
    end
    if dropMontPoints
        actionStr = 'left';
        viablePoints = Ve.V;
        cost = Ve.cost;
    else
        actionStr = 'added';
        viablePoints = vertcat(viablePoints,Ve.V);
        cost = vertcat(cost,Ve.cost);
    end
    fprintf('[INFO] getViablePoints, Ellipsoids-based sampling %s %d viable points.\n',actionStr,size(Ve.V,1));

    if isempty(viablePoints) % false: ELexp contains input points (cf. check at the begining); left as a safety-net for future
        error('GETVIABLEPOINTS:NoPointsFound', 'No viable points were found. Try increasing nmont and/or nelip.');
    end

end
