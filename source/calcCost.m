function [ cost, residuals ] = calcCost( x, y, npar, varargin )
%CALCCOST Compute error of model observables values x with respect to measured
% values y, as a negative log-likelihood of the centralised multivariate normal
% distribution at x-y residuals.
%
% Number of model parameters in npar is not used here.
%
% For more details, in particular for a description of the extra arguments, see
% mvnnloglike.
%

% just in case - flatten both
x = x(:);
y = y(:);
n = numel(x);
if n ~= numel(y)
    error('CALCCOST:IncompatibleDimensions', 'Number of observed data points does not agree with number of measured data points (%d vs. %d)',n,numel(y));
end
residuals = x - y;

cost = mvnnloglike( residuals, varargin{:} );

end
