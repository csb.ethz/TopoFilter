%% Simulate IQM model

% input: settings (req: model or modelFilename, experiments or expFilename,
%        expData or expDataFilename), points (req: rowmat, colnames[, islog,
%        cost, projected w/ names and values fields])[, cleanup = false]
% output: nsols, cost[, mexNames, mexCleanups (empty if settings.model was
%         already a cell array of mex's)]

% 0) Setup

% % claude-decoding
% settings.modelFilename = 'decoding_011.txtbc';
% settings.expFilename = {'exp-control.exp','exp-alpha.exp','exp-input.exp'};
% settings.expDataFilename = 'expData.xls';
% settings.calcCostFcn = @decoding_calcCost;
% settings.calcThresholdFcn = @decoding_calcThreshold;

% % kuepfer-tor
% %settings.modelFilename = 'model_01_02_03_04-observables.txt';
% settings.modelFilename = 'model_02-observables.txt';
% settings.expFilename = {'exp00-SteadyState.exp', 'exp01-Rapa500.exp', 'exp02-Rapa109.exp'};
% settings.expDataFilename = 'expData.xls';

% toy-ligand-fluorescent
settings.modelFilename = 'model.txt';
settings.expFilename = {'exp-SteadyState.exp'};
settings.expDataFilename = 'expData.xls';



% 1a) Load saved viable points

% % claude-decoding
% loaded = load('decoding_011_160704_093148_viablePoints_sampled_2074323_0000000000000000000000000.mat');

% % kuepfer-tor
% %loaded = load('model_01_02_03_04_160225_110149_viablePoints_sampled_2115758_000000000000000000000000111111111111111111.mat');
% loaded = load('model_02-160920_174721_viablePoints_sampled_2148105_00000000000000000000000011111.mat');

% toy-ligand-fluorescent
loaded = load('model_160229_152734_viablePoints_projected_2000000_10.mat');

points = loaded.viablePoints;


% 1b) Load initial point from params specification
%points = table2struct(loadParamSpecs('paramSpecs.txt'),'ToScalar',true);
%assert(all(~isnan(points.p0)),'No p0 given in param spec.');
%points.rowmat = transpose(points.p0);
%points.colnames = points.names;
%points.islog = []; % p0 in paramSpec is given in normal scale, and islog
%                   % there indicates only wether to sample is param space

%% prepare points
variableParamValuesHeadRowmat = points.rowmat;
[npoints,nparams] = size(variableParamValuesHeadRowmat);
if ~isfield(points, 'projected')
    projected.names = {};
    projected.values = [];
else
    projected = points.projected;
end
paramNames = vertcat(points.colnames, projected.names);
fixedParamValuesTail = projected.values;
if ~isfield(points, 'islog') || isempty(points.islog)
    islog = false(1,nparams);
else
    islog = points.islog;
end
if ~isfield(points, 'cost') || isempty(points.cost)
    expectedCost = [];
else
    expectedCost = points.cost;
end

%% prepare models and data
% TODO: wrap that into a fun (e.g. prepareModels ) and allow for re-usage
%       of MEX's (loadModel, loadExperiments, and mexModel are already
%       idempotent);
%       tech issue: cleanup objects - make a flag wether to make them or
%       not and return
%       also: refactor same piece from TFmain
model = loadModel(settings);
experiments = loadExperiments(settings);

nExp = numel(experiments);
mexNames = cell(nExp,1);
mexCleanups = cell(nExp,1);
for i = 1:nExp
    experiment = experiments{i};
    expmodel = IQMmergemodexp(model,experiment);
    [mexNames{i}, mexCleanups{i}] = mexModel(expmodel);
end

% data
expData = loadExpData(settings);


%% simulate and compute costs
n = min(1000, npoints); % once in a function: rm the 1000 limit
cost = nan(n,1);
tolcost = 1e-6;

% prepare simulation function
evalModelDirect(mexNames, expData, paramNames, islog, fixedParamValuesTail);

fprintf('   cost @ {''%s''}:\n',strjoin2(paramNames,''';''')); % once in a function: as DEBUG info
for k = n:-1:1
    [cost(k), paramValues] = evalModelDirect(variableParamValuesHeadRowmat(k,:)');
    if (~isempty(expectedCost)) && (abs((cost(k)-expectedCost(k))/cost(k)) >= tolcost)
        warning('Point #%d, different cost than the expected cost of %.3g.',k,expectedCost(k));
    end
    fprintf(' %6.5g @ %s\n',cost(k),mat2str(paramValues)); % once in a function: as DEBUG info
end
