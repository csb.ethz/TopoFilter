function [ p0, error0 ] = getViableInitPoint( paramSpecs, viabilityThreshold )
%GETVIABLEINITPOINT ...
%
% Attention: input `paramSpecs.p0` contains points in columns, whereas returned `p0` in rows
%

% filter-out points with NaN values
paramSpecs.p0 = paramSpecs.p0(:, ~any(isnan(paramSpecs.p0), 1));
if (~size(paramSpecs.p0, 2))
    errStrPref = 'found';
    % Try the model values, or, if they don't satisfy specified bounds, random
    if all((paramSpecs.values <= paramSpecs.bmax) & (paramSpecs.values >= paramSpecs.bmin))
        p0prop = paramSpecs.values';
    else
        p0prop = getRandInitPoint(paramSpecs);
    end
    evalFunHandle = @evalModel;
    p0 = p0prop;
    error0 = evalFunHandle(p0);
    nSearchTry = 0;
    % TODO: make maxNSearchTry a TFmain option
    maxNSearchTry = 1e2;
    % TODO: make simplexOPTIONS and a TFmain option
    simplexOPTIONS.maxfunevals = 1000000;
    simplexOPTIONS.maxiter = 50000;
    simplexOPTIONS.maxitertemp = 2000;
    simplexOPTIONS.maxitertemp0 = 100;
    simplexOPTIONS.maxtime = 60;
    simplexOPTIONS.tempfactor = 1/2;
    simplexOPTIONS.tolx = 1e-3;
    simplexOPTIONS.tolfun = 1e-6;
    simplexOPTIONS.lowbounds = paramSpecs.bmin;
    simplexOPTIONS.highbounds = paramSpecs.bmax;
    simplexOPTIONS.outputFunction = '';
    simplexOPTIONS.silent = 0;
    % TODO: parallelize init point search: batches over #workers
    while nSearchTry < maxNSearchTry && error0 > viabilityThreshold
        nSearchTry = nSearchTry + 1;
        %% Get initial point for parameter exploration w/ simplexIQM optimization
%         [p0] = evolstrat(evalFunHandle,1, d, p0prop', simplexOPTIONS.lowbounds', simplexOPTIONS.highbounds');
        simplexOPTIONS.tempstart = error0*100;
        [p0, error0, exitflag] = simplexIQM(evalFunHandle, p0prop, simplexOPTIONS);
        p0 = reshape(p0, size(p0prop));
        if exitflag == 0
            warning('GETVIABLEINITPOINT:SimplexFail', 'simplexIQM did not find optimum.');
        end
        % new proposal in case of another iteration
        p0prop = getRandInitPoint(paramSpecs);
    end
else
    errStrPref = 'provided';
    p0 = paramSpecs.p0';

    %% Check bounds; consistently with HYPERSPACE: non-strict (cf. OEAMC)
    outOfBoundsMask = any(bsxfun(@lt, p0, paramSpecs.bmin') | bsxfun(@gt, p0, paramSpecs.bmax'), 2);
    if any(outOfBoundsMask)
        if sum(outOfBoundsMask) == size(p0, 1)
            warnOrErrHandle = @error;
            errStrPref2 = ['All of ' errStrPref];
            errStrSuf2 = '.';
        else
            warnOrErrHandle = @warning;
            errStrPref2 = ['Some of ' errStrPref];
            errStrSuf2 = sprintf('; points indices: %s.', mat2str(find(outOfBoundsMask)));
        end
        warnOrErrHandle('GETINITVIABLEPOINT:OutOfBounds',...
            '%s starting points do not fit specified parameters bounds%s',...
            errStrPref2, errStrSuf2);
        p0 = p0(~outOfBoundsMask, :);
    end

    %% calculate errors
    error0 = zeros(size(p0, 1), 1);
%     p0log(:,find(paramSpecs.islog)) = log10(p0(:,find(paramSpecs.islog)));
    for i = 1:size(p0, 1)
        error0(i) = evalModel(p0(i, :));
    end
end

%% Check viability threshold
nonViableMask = error0 > viabilityThreshold;
if any(nonViableMask)
    if sum(nonViableMask) == numel(error0)
        warnOrErrHandle = @error;
        errStrPref2 = ['All of ' errStrPref];
    else
        warnOrErrHandle = @warning;
        errStrPref2 = ['Some of ' errStrPref];
    end
    warnOrErrHandle('GETINITVIABLEPOINT:NotViable',...
        '%s starting points are not viable i.e. their errors: %s, are greater than viability threshold: %.3d.',...
        errStrPref2, mat2str(error0(nonViableMask)'), viabilityThreshold);
    p0 = p0(~nonViableMask, :);
    error0 = error0(~nonViableMask);
end

end


function [ p0prop ] = getRandInitPoint(paramSpecs)
    p0prop = (rand(size(paramSpecs.bmax, 1), 1).*(paramSpecs.bmax - paramSpecs.bmin) + paramSpecs.bmin)';
end