function [ expData ] = loadExpData( settingsOrFilename, varargin )
%LOADEXPDATA ... Read and process the experimental data, if not done before.
%
%   Wrapper for ExpData(...)
%
    %
    if ischar(settingsOrFilename)
        settings.expDataFilename = settingsOrFilename;
    elseif ~isstruct(settingsOrFilename)
        error('LOADEXPDATA:IllegalArgument', 'Expecting experimental data file name, or cell array of file names, or settings structure.');
    else
        settings = settingsOrFilename;
    end
    if nargin > 1
        settings.expDataIdxs = varargin{1};
    end
    if ~isfield(settings, 'expData') || isempty(settings.expData)
        if ~isfield(settings, 'expDataFilename') || isempty(settings.expDataFilename)
            error('LOADDATA:MissingExpData', 'Please specify `expDataFilename` string or cell array of strings.');
        else
            if isfield(settings, 'expDataIdxs')
                if ~all(isposint(settings.expDataIdxs))
                    error('LOADDATA:IllegalArgument', 'Expecting `expDataIdxs` to be a vector of indices (positive integers).');
                end
            else
                settings.expDataIdxs = [];
            end
            expData = ExpData(settings.expDataFilename, settings.expDataIdxs,...
                settings);
        end
    else
        expData = settings.expData;
    end
end
