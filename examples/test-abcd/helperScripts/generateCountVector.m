function counts = generateCountVector(runs)
%GENERATECOUNTVECTOR Given a `runs`-struct, the function
%	returns a vector that counts the occurences of each model.
%	The index in the vector corresponds to integer value of
%	binary representation of the model would have (e.g. the
%	count for the model with projected parameters 0000011 will
%	be in counts(3)).
	numRuns = length(runs);
	counts = zeros(2^7, 1);
	for i = 1:numRuns
		% Create an array of strings representing the
		% viable models found in each run in binary
		foundModels = num2str(runs{i}.viableProjectionsCollection);
		foundModels = reshape(foundModels(foundModels~=' '), [size(foundModels,1),7]);
		% Increase count for those models
		counts(bin2dec(foundModels)) = count(bin2dec(foundModels))+1;
	end
end