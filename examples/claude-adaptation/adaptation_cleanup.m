function [] = adaptation_cleanup()
%ADAPTATION_CLEANUP Cleanup files and folders created while computing and
% post-processing the adaptation example results

cellfun(@rmdir_rec, {'results', 'merged', 'OutV', 'Arrays'});

mex_files = dir(sprintf('*.%s',mexext));
cellfun(@delete, {mex_files.name})

end

function [removed] = rmdir_rec(dirName)
    removed = false;
    if (exist(dirName, 'dir') == 7)
        removed = rmdir(dirName, 's');
    end
end
