function [testfun] = isinrange(n)
%ISINRANGE Return test function to check if its argument is an integer in a 0:n
% range.
%
% Note: `Inf` counts as a positive integer.
%
    testfun = @(x) isint(x) && (x <= n);

end
