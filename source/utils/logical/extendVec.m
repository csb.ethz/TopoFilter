function [extVec] = extendVec(mask, defaults, vec)
% EXTENDVEC Extend vector `vec` to default values `defaults`, according to
%  logical vector `mask`. Returns extended vector `extVec` where:
%               `extVec(mask) == defaults(mask)`, and
%               `extVec(~mask) == vec` or `extVec(~mask) == vec'`,
%
%  Remark: shape of `defaults` determines shape of the `extVec`; it's poosible
%  that `defaults` is a column vector whereas `vec` is a row vector, or
%  vice-versa, as long as numbers of elements agree (`mask` is used for indexing).
%
    extVec = defaults;
    extVec(~logical(mask)) = vec;
end
