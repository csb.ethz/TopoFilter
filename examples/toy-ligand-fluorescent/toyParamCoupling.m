function C = toyParamCoupling
    % When 'k1' is projected than also always project 'k2'
    C = { 1, 2 };

end
