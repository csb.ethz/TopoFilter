function tps = adaptation_getPeakTimePoints(nsol, obs)

stateindex = find(ismember(nsol.states, obs));

timeindex = find(nsol.time == 2000);

signal1 = stepinfo(nsol.statevalues(1:timeindex,stateindex), nsol.time(1:timeindex));
signal2 = stepinfo(nsol.statevalues((timeindex+1):end,stateindex), nsol.time((timeindex+1):end));
pks = findpeaks(nsol.statevalues((timeindex+1):end,3));

T1 = signal1.SettlingTime;
T2 = signal2.SettlingTime;


%% %% for plotting
% simdata = nsol;
%
% statestoplot = {'A' 'B' 'C'};
% variablestoplot = {'I'};
%
% inputcolor  = [120/255 174/255 64/255];
% outputcolor = [69/255 112/255 180/255];
% Acolor = [233/255 78/255 27/255];
% Bcolor = [109/255 54/255 135/255];
%
%
% % Plot dynamics
% color = {Acolor; Bcolor; outputcolor; inputcolor};
%
% figure(1);
% clf;
%
% stateindices = ones(1,length(statestoplot));
% for l =1:length(statestoplot),
%     stateindices(l) = find(ismember(simdata.states,statestoplot{l}));
% end
%
% varindices = ones(1,length(variablestoplot));
% for l =1:length(variablestoplot),
%     varindices(l) = find(ismember(simdata.variables,variablestoplot{l}));
% end
%
% y= [ simdata.statevalues(:,stateindices) simdata.variablevalues(:,varindices)];
% h = plot(simdata.time,y);
%
% set(h, {'color'},color);
% hold on;
% legend([ simdata.states(stateindices) simdata.variables(:,varindices)]);
% %     axis([0 tmax 0 max(y(:))]);
% axis([0 max(simdata.time) 0 1]);
% %     axis([0 600 0 200]);
% xlabel('time(min)')
% ylabel('Concentration (nM)')
% title('Dynamic Response')
%
%%

    if numel(pks)>1 ,
        if T1 < 1500 && T2 < 2900 && 2*(pks(2)- pks(end)) < (pks(1)-pks(end)),
        tps = floor([nsol.time(timeindex - 1) signal2.PeakTime (T2 + 1)]);
        else
        tps = [0];
        end
    else
    tps = [0];
    end
end