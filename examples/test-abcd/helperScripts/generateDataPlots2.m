function generateDataPlots2(paramValues, expData, outputstring)
    close all;

    %% Simulate the model
    maxT = 500;
    model = IQMmodel('model.txt');
    [paramNames,~] = IQMparameters(model);
    [~,~,ICs] = IQMstates(model);

    errVal = 1e-12;             %value for relative and absolute tolerance
    options.reltol = errVal;    %relative tolerance
    options.abstol = errVal;    %absolute tolerance
    options.maxnumsteps = 1e6;  %maximum number of steps
    simu = IQMPsimulate('mexModel', maxT, ICs, paramNames, paramValues, options);

    stds = zeros(size(expData.measurements));
    for i = 1:size(expData.measurements,1)
        for j = 1:size(expData.measurements,2)
            pos = (j-1)*size(expData.measurements,1) + i;
            stds(i, j) = sqrt(expData.S(pos,pos));
        end
    end


    %% Generate plot
    fig = figure;
    subplot(211);
    hold on;
    box on;
    plot(transpose(simu.time), simu.statevalues(:,1),'k');
    errorbar(expData.timePoints,expData.measurements(1,:),stds(1,:), 'k.');
    xlim([0 550]);
    ylim([50 150]);
    xlabel('t');
    ylabel('[A](t)');
    subplot(212);
    hold on;
    box on;
    plot(transpose(simu.time), simu.statevalues(:,3),'k');
    errorbar(expData.timePoints,expData.measurements(2,:),stds(2,:), 'k.');
    xlim([0 550]);
    ylim([20 60]);
    xlabel('t');
    ylabel('[C](t)');

    % set(gca, 'Position', get(gca, 'OuterPosition') - get(gca, 'TightInset') * [-1 0 1 0; 0 -1 0 1; 0 0 1 0; 0 0 0 1]);

    % Here we preserve the size of the image when we save it.
    width = 2.3;     % Width in inches
    height = 2;    % Height in inches
    % set(gcf,'InvertHardcopy','on');
    set(gcf,'PaperUnits', 'inches');
    set(gcf, 'PaperUnits', 'inches');
    set(gcf, 'PaperSize', [width height]);
    set(gcf, 'PaperPositionMode', 'manual');
    set(gcf, 'PaperPosition', [0 0 width height]);

    % Save the file as PDF
    set(gcf, 'renderer', 'painters');
    print(['~/Polybox/TopoFiltering/Report/figures/model2/' outputstring],'-dpdf','-r300')
end
