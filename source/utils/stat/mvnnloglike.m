function [ nloglike ] = mvnnloglike( x, invS, varargin )
%MVNNLOGLIKE Compute the negative log-likelihood of the centralised multivariate
%  normal distribution at x (vector). Requires inverse of the covariance matrix
%  invS.
%
%  Covariance matrix inverse invS can be given as a vector. In such case it is
%  interpreted as a diagonal of reciprocals of variances of independent marginal
%  distributions, i.e. invS is transformed by diag(invS).
%
% nloglike = mvnnloglike( x, invS, logDetS )
%  For numerical stability, you can pass explicitly computed value of log of
%  determinant of the covariance matrix. Otherwise, it is computed as
%  -log(det(invS)).
%
% nloglike = mvnnloglike( x, invS, false )
%  Alternatively, you can pass the false value as the log determinant, which
%  indicates that normalization of the likelihood should not be done at all.
%  In this case the neg. log-likelihood value is computed simply as x'*invS*x.
%

n = numel(x);
% if given only weights per point, use the diagonal covariance matrix
if (size(invS,1) == 1 || size(invS,2) == 1)
    invS = diag(invS);
end
if ~(issymmetric(invS) && n == size(invS,1))
    error('MVNNLOGLIKE:IncompatibleDimensions', 'Covariance matrix inverse is either not square symmetric, or it''s size does not agree with the dimension of the point (%d by %d vs. %d)',size(S,1),size(S,2),n);
end

% if not given, compute logDetS = log(1/det(invS)) = -log(det(invS))
if isempty(varargin)
    logDetS = -log(det(invS));
else
    logDetS = varargin{1};
end

nloglike = transpose(x)*invS*x;
if isnumeric(logDetS)
    nloglike = 0.5*(n*log(2*pi) + logDetS + nloglike);
elseif ~islogical(logDetS) || logDetS
    error('MVNNLOGLIKE:IllegalArgument', 'Logarithm of determinant of the covariance matrix must be either numeric or "false".');
end

end
