function [ret] = clockStamp()
% CLOCKSTAMP An integer time stamp build out of the `clock` value as:
%    sum(floor(1000*clock))
%
%
    ret = sum(floor(1000*clock));
end
