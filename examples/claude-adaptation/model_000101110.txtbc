********** MODEL NAME
Three nodes

********** NOTES
% three nodes biochemical interactions.
% Ma et al, Cell, 2009: Defining Network Topologies able to achieve
% Biochemical Adaptation

********** MODEL STATE INFORMATION
A(0)      = 0.5        % activated by input signal
B(0)      = 0.5        % regulatory node
C(0)      = 0.5        % output and regulatory node

********** MODEL PARAMETERS
actAA     = 0
actBA     = 0
actCA     = 0
actAB     = 1
actBB     = 0
actCB     = 1
actAC     = 1
actBC     = 1
actCC     = 0

EA        = 0.5      % amount of constitutive enzyme: fixed
EB        = 0.5      % amount of constitutive enzyme: fixed
EC        = 0.5      % amount of constitutive enzyme: fixed

kIA       = 0.5      % activation of A by input Vmax: to sample
kEA       = 0.5      % activation of A by constitutive enzyme Vmax: to sample
kEB       = 0.5      % activation of B by constitutive enzyme Vmax: to sample
kEC       = 0.5      % activation of C by constitutive enzyme Vmax: to sample

kAA       = 0.5      % activation of A by A Vmax: to sample
kAB       = 0.5      % activation of B by A Vmax: to sample
kAC       = 0.5      % activation of C by A Vmax: to sample
kBA       = 0.5      % activation of A by B Vmax: to sample
kBB       = 0.5      % activation of B by B Vmax: to sample
kBC       = 0.5      % activation of C by B Vmax: to sample
kCA       = 0.5      % activation of A by C Vmax: to sample
kCB       = 0.5      % activation of B by C Vmax: to sample
kCC       = 0.5      % activation of C by C Vmax: to sample

KEA       = 100      % activation of A by constitutive enzyme Km: to sample
KEB       = 100      % activation of B by constitutive enzyme Km: to sample
KEC       = 100      % activation of C by constitutive enzyme Km: to sample
KIA       = 100      % activation of A by input Km: to sample

KAA       = 1e9      % activation of A by A Km: to project
KAB       = 1e9      % activation of B by A Km: to project
KAC       = 1e9      % activation of C by A Km: to project
KBA       = 1e9      % activation of A by B Km: to project
KBB       = 1e9      % activation of B by B Km: to project
KBC       = 1e9      % activation of C by B Km: to project
KCA       = 1e9      % activation of A by C Km: to project
KCB       = 1e9      % activation of C by C Km: to project
KCC       = 1e9      % activation of B by C Km: to project
                  
********** MODEL VARIABLES
I =  piecewiseIQM(0.6, ge(time,2000), 0.5) 
actEA     = 1 - (actAA + actBA + actCA) > 0
actEB     = 1 - (actAB + actBB + actCB) > 0
actEC     = 1 - (actAC + actBC + actCC) > 0
********** MODEL REACTIONS
        
                     => A                                         : RA%  
                  vf = I*kIA*((1 - A)/(1 - A + KIA)) + (2*actAA - 1)* A*kAA*((A*(1-actAA) + actAA*(1 - A))/(A*(1-actAA) + actAA*(1 - A) + KAA)) + (2*actBA - 1)* B*kBA*((A*(1-actBA) + actBA*(1 - A))/(A*(1-actBA) + actBA*(1 - A) + KBA)) + (2*actCA - 1)* C*kCA*((A*(1-actCA) + actCA*(1 - A))/(A*(1-actCA) + actCA*(1 - A) + KCA)) + (2*actEA - 1)*EA*kEA*((A*(1-actEA) + actEA*(1 - A))/(A*(1-actEA) + actEA*(1 - A) + KEA))
                                                       
                     
                     => B                                         : RB%  
                  vf =                                 (2*actAB - 1)* A*kAB*((B*(1-actAB) + actAB*(1 - B))/(B*(1-actAB) + actAB*(1 - B) + KAB)) + (2*actBB - 1)* B*kBB*((B*(1-actBB) + actBB*(1 - B))/(B*(1-actBB) + actBB*(1 - B) + KBB)) + (2*actCB - 1)* C*kCB*((B*(1-actCB) + actCB*(1 - B))/(B*(1-actCB) + actCB*(1 - B) + KCB)) + (2*actEB - 1)*EB*kEB*((B*(1-actEB) + actEB*(1 - B))/(B*(1-actEB) + actEB*(1 - B) + KEB))                                                        
                  
                     => C                                         : RC%  
                  vf =                                 (2*actAC - 1)* A*kAC*((C*(1-actAC) + actAC*(1 - C))/(C*(1-actAC) + actAC*(1 - C) + KAC)) + (2*actBC - 1)* B*kBC*((C*(1-actBC) + actBC*(1 - C))/(C*(1-actBC) + actBC*(1 - C) + KBC)) + (2*actCC - 1)* C*kCC*((C*(1-actCC) + actCC*(1 - C))/(C*(1-actCC) + actCC*(1 - C) + KCC)) + (2*actEC - 1)*EC*kEC*((C*(1-actEC) + actEC*(1 - C))/(C*(1-actEC) + actEC*(1 - C) + KEC))
                                                   
                  
********** MODEL FUNCTIONS


********** MODEL EVENTS


********** MODEL MATLAB FUNCTIONS