function mainCluster(nRuns,nfevalExp,nWorkers,enumLevel,exhaustiveRank)

%% Quick check; no non-empty results expected (cf. tests/toyTest.m: testEssentialPars4Coupled)
%expSubfolder='toy-ligand-fluorescent';
%expFunName = 'toyExperiment';
%modelFilename = 'model.txt';
%paramSpecsFilename = 'paramSpecs_4a.txt';

% For an actual output test
expSubfolder='kuepfer-tor';
expFunName = 'torExperiment';
modelFilename = 'model_01_02_03_04-observables.txt';
paramSpecsFilename = 'paramSpecs_01_02_03_04.txt';

%parpoolname = 'SGE_BSSE';
parpoolname = 'local';

resultsFolder = fullfile('..','results');

debuglevel = 0;

appsroot = fullfile(pwd,'../..');

    %% initialise
    setpath(appsroot);

    rng('shuffle');

    % open parpool
    parpool(parpoolname,nWorkers);
    parpoolCleanup = onCleanup(@() delete(gcp('nocreate')));


    % cd to exp folder
    expFolder = fullfile(appsroot,'TopoFilter/examples',expSubfolder);
    currFolder = cd(expFolder);
    cdCleanup = onCleanup(@() cd(currFolder));

    %% run
    runstruct.debuglevel = debuglevel;
    runstruct.funName = expFunName;
    runstruct.runName = expSubfolder;
    runstruct.numWorkers = nWorkers;
    runstruct.numRuns = nRuns;
    runstruct.parallelize = 1;
    runstruct.enumLevel = enumLevel;
    runstruct.exhaustiveRank = exhaustiveRank;
    runstruct.modelFilename = modelFilename;
    runstruct.paramSpecsFilename = paramSpecsFilename;
    runstruct.nfeval = 10^nfevalExp;

    strfconf = sprintf('%03d_1e%d',nWorkers,nfevalExp);
    strftime = datestr(now, 'yymmdd_HHMMSS');
    outPath=fullfile(currFolder,resultsFolder,expSubfolder,strfconf);
    outBasename=fullfile(outPath,strftime);
    runstruct.outputFilename=sprintf('%s.mat',outBasename);


    if exist(outPath, 'dir') == 0
        mkdir(outPath);
    end
    dfn = sprintf('%s.log',outBasename); % diary file name
    dh = strftime; % diary header
    fprintf('Writing to diary ''%s''..\n',dfn);
    diary(dfn);

    fprintf('%s\n%s\n',dh,regexprep(dh, '.', '='));

    try
        runTf(runstruct);
    catch ME
        diary('off');
        throw(ME);
    end

    diary('off');
    disp('.. done');

end



function setpath(appsroot)
% appsroot: full path for the command line calls

    if (exist('IQMmakeMEXmodel','file') ~= 2)
        pathIQM = fullfile(appsroot,'IQM Tools');
        pathCurr = cd(pathIQM);
        %installSBPOPpackageInitial; % run this at least once on a culster
        try
            installIQMtools;
        catch ME
            cd(pathCurr);
            throw(ME);
        end
        cd(pathCurr);
    end

    if (exist('Elexp','file') ~= 2)
        addgenpath(fullfile(appsroot,'HYPERSPACE/Source'));
    end
    if (exist('TFmain','file') ~= 2)
        addgenpath(fullfile(appsroot,'TopoFilter/source'));
    end

    addgenpath(fullfile(appsroot,'scripts'), false);
    addgenpath('/usr/local/stelling/grid/matlab', false);

end

function addgenpath(path, varargin)
    if nargin > 1
        req = logical(varargin{1});
    else
        req = true;
    end
    if exist(path,'dir')
        addpath(genpath(path));
    elseif req
        error('Couldn''t find required app in ''%s''.',path);
    end
end



function [ rs ] = runTf(rs)
    if ~isfield(rs,'funName')
        rs.funName = 'myExperiment';
    end
    if ~isfield(rs,'dryRun')
        rs.dryRun = false;
    end
    if ~isfield(rs,'numRuns')
        rs.numRuns = 1;
    end
    if ~isfield(rs,'paramSpecsFilename')
        rs.paramSpecsFilename = 'paramSpecs.txt';
    end
    if ~isfield(rs,'modelFilename')
        rs.modelFilename = 'model.txt';
    end
    if ~isfield(rs,'p0')
        rs.p0 = NaN;
    end
    if ~isfield(rs,'exhaustiveRank')
        rs.exhaustiveRank = 1;
    end
    if ~isfield(rs,'enumLevel')
        rs.enumLevel = 1;
    end
    if ~isfield(rs,'parallelize')
        rs.parallelize = 1;
    end
    if ~isfield(rs,'saveViablePoints')
        rs.saveViablePoints = true;
    end
    if ~isfield(rs,'cleanupMexFiles')
        rs.cleanupMexFiles = false;
    end
    if ~isfield(rs,'nfeval')
        rs.nfeval = 1e2;
    end
    if ~isfield(rs,'rfeval')
        rs.rfeval = [4 5];
    end
    if ~isfield(rs,'pvmin')
        rs.pvmin = 0.05;
    end
    if ~isfield(rs,'pvmax')
        rs.pvmax = 1.95;
    end
    if ~isfield(rs,'debuglevel')
        rs.debuglevel = 0;
    end

    reportSetup(rs);
    debuglevel(rs.debuglevel);
%comment/uncomment to run computations/test script
%pause(5);return;

    [rs.exitStatus, rs.resultsFn, rs.elapsedTime] = feval(rs.funName,...
        'dryRun',rs.dryRun,...
        'numRuns',rs.numRuns,...
        'outputFilename',rs.outputFilename,...
        'paramSpecsFilename',rs.paramSpecsFilename,...
        'modelFilename',rs.modelFilename,...
        'p0',rs.p0,...
        'exhaustiveRank',rs.exhaustiveRank,...
        'enumLevel',rs.enumLevel,...
        'parallelize',rs.parallelize,...
        'saveViablePoints',rs.saveViablePoints,...
        'cleanupMexFiles',rs.cleanupMexFiles,...
        'nfeval',rs.nfeval,...
        'rfeval',rs.rfeval,...
        'pvmin',rs.pvmin,...
        'pvmax',rs.pvmax);
    reportResults(rs);
end

function reportSetup(rs)
    if (exist('cpuinfo','file') == 2)
        fprintf('\n\n');
        disp('CPU info');
        disp('--------');
        disp(cpuinfo);
    end

    tn = rs.runName;
    fprintf('\n\n\n%s\n%s\n',tn,regexprep(tn, '.', '-'));

    fprintf('\n\n### Setup\n');

    printkv('pwd',pwd);
    printkv('debuglevel',rs.debuglevel);
    printkv('numWorkers',rs.numWorkers);
    printkv('numRuns',rs.numRuns);
    printkv('outputFilename',rs.outputFilename);
    printkv('modelFilename',rs.modelFilename);
    printkv('paramSpecsFilename',rs.paramSpecsFilename);
    printkv('p0',rs.p0);
    printkv('exhaustiveRank',rs.exhaustiveRank);
    printkv('enumLevel',rs.enumLevel);
    printkv('parallelize',rs.parallelize);
    printkv('saveViablePoints',rs.saveViablePoints);
    printkv('cleanupMexFiles',rs.cleanupMexFiles);
    printkv('nfeval',rs.nfeval);
    printkv('rfeval',rs.rfeval);
    printkv('pvmin',rs.pvmin);
    printkv('pvmax',rs.pvmax);

    fprintf('\n\n### Run\n');
end

function reportResults(rs)
fprintf('\n\n### Results\n');

printkv('exitStatus',rs.exitStatus);
printkv('resultsFn',rs.resultsFn);
printkv('elapsedTime',rs.elapsedTime);
end

function printkv(k,v)
    fprintf('\t%s = %s\n',k,mat2str(v));
end
