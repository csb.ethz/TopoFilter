function [ exit_status, results_fn, elapsed_time ] = abcdExperiment( varargin )

    assert(exist('TFmain') == 2, 'Couldn''t find `TFmain` function. Be sure to add TopoFilter source code folder to the path.');

    timestamp = @() datestr(now, 'yyyy.mm.dd HH:MM');

    %% Initialise
	rng('shuffle');

    fprintf('Program started at %s\n',timestamp());
	tic;

	%% Tunable experiments setting if not provided via function options (+ validation)
    parser = inputParser;
    parser.FunctionName = 'ABCDEXPERIMENT: ';

    % Results file
    addParameter(parser, 'outputFilename', fullfile('./results',...
        sprintf('%s.mat',datestr(now, 'yymmdd_HHMMSS'))), @ischar);
    % Number of runs
    addParameter(parser, 'numRuns', 1, @isposint);
    addParameter(parser, 'dryRun', false, @islogical); % calc only threshold (and p0, if not given)

    % Sample sizes: not directly options itself, but used below for setting
    % the actual nmont, nelip and nvmin options.
    % Maximum number of cost function evauations in one sampling (roughly).
    addParameter(parser, 'nfeval', 1e2, @isposint);
    % Ratio of nfeval's going respectively into nmont and nelip
    addParameter(parser, 'rfeval', [4 5], @(x) numel(x)==2 && all(isposint(x)));
    % Minimal size of viable points sample as a proportion of nfeval
    addParameter(parser, 'pvmin', 0.1, @isprob);

    % Flags to control flow of the model space search
    addParameter(parser, 'recursive', true, @islogical);

    % Parallel computation level
    addParameter(parser, 'parallelize', 1, isinrange(3));
    % Param space exploration specification
    addParameter(parser, 'paramSpecsFilename', './paramSpecs.txt', @ischar);

    % Init param value
    addParameter(parser, 'p0', NaN, @isnumeric); % NaN = from the param spec or model

    % Which sheets in XLS experiment data file to use
    addParameter(parser, 'expDataIdxs', 5, @isposint)

    % Parse the input arguments
    parse(parser, varargin{:});
    p = parser.Results;

    %% Model, experiment and data files
    p.modelFilename = './model.txt';
    p.expFilename = './exp-abcd.exp';
    p.expDataFilename = './expData.xls';

    %% Sample sizes
    % Split nfeval according to given proportions
    nfevalv = splitlrm(p.nfeval,  p.rfeval/sum(p.rfeval));
    % Maximum target sample size in the Markov chain Monte Carlo (MCMC) sampling
    p.nmont = nfevalv(1);
    % Guiding number of model evaluations in the Ellipsoids-based sampling
    p.nelip = nfevalv(2);
    % Minimum nr of viable parameter samples before (re-)sampling
    p.nvmin = max(1, round(p.pvmin*sum(nfevalv)));

    %% Run the actual program
    [ runs, viabilityThreshold, settings ] = TFmain(p);

	%% Save results
    results_fn = settings.outputFilename;
    save(results_fn, 'runs', 'viabilityThreshold', 'settings');

    %% Cleanup
    elapsed_time = toc;
    fprintf('Program finished at %s\n',timestamp());

    exit_status = 0;

end
