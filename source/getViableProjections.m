function newViableProjectionsSet = getViableProjections(viablePoints, threshold, globalProjection, globalViableProjectionSet, options, evalModelOPTIONS)
%GETVIABLEPROJECTIONS Find a set of new viable projections for a given threshold
% of viability, starting with a current projection vector globalProjection
% (logical) and numeric matrix viablePoints (points in rows).
%
% Implements OAT-sum heuristic: for each point first project (coupled) params
% one-at-a-time (OAT), and using again all points test sum of each OAT viable
% projections (OAT-sum).
%
% Beware: newViableProjectionsSet may intersect with globalViableProjectionSet.
%         For OAT-sum projections skips projections given in
%         globalViableProjectionSet, but not for OAT projections (added to
%         newViableProjectionsSet if not options.dropOATProjections).
%
debugging = (debuglevel > 0);

    if (size(globalProjection, 1) > 1) % either 1x1 or col vector
        globalProjection = globalProjection'; % ensure row vector
    end
    localModifiableParameterIdxs = find(~globalProjection);
    % TODO: evalModel now knows which argin values to assign to which params
    %      so rm all this error-prone trim and extend bullshit, i.e.:
    %      viablePoints(:,local), dim (vs. gDim), dimIdxs (vs. specParamIdxs)
    %      + extendVec(global, ..)
    %      + evalModelOPTIONS.fixPossibleToFixParamIdx()
    %      + extend viablePoints in prepareViablePointsForProjection
    %      + extend ProjectionSet in topologicalFilteringIteration
    %
    viablePoints = viablePoints(:,localModifiableParameterIdxs);
    %initialize space and check the size of the necessary parameter points
    [numViablePoints, dim] = size(viablePoints);
    if debugging
        fprintf('[DEBUG] getViableProjections, dim = %d, numViablePoints = %d.\n',...
            dim, numViablePoints);
        fprintf('[DEBUG] getViableProjections, global projection (model idxs) = %s.\n',...
            modifiableModParamStr(globalProjection, evalModelOPTIONS));
        fprintf('[DEBUG] getViableProjections, local modifiable param idxs (param spec idxs) = %s.\n',...
            mat2str(localModifiableParameterIdxs));
        fprintf('[DEBUG] getViableProjections, total number of found viable projections up to this point = %d.\n',...
            globalViableProjectionSet.num);
    end
    if dim == 0
        newViableProjectionsSet = options.projectionSetCls();
        return
    end

    % prep options for the evalModel function
    fixedSpecParamsMask = evalModelOPTIONS.fixSpecParamIdxs(globalProjection);

    newViableProjectionsSet = options.projectionSetCls(dim);

%TODO
%
% README.md entry:
% |`useOATheuristics`|`logical`|`false`|use a heuristics of testing reductions for which there are points close to the boundary values (to reduce the number of tested one at a time projections)|
%
%     if options.useOATheuristics
%         bmin = evalModelOPTIONS.paramBoundLower(localModifiableParameterIdxs);
%         % TEST: why 3?
%         OATsumViableProjections = viablePoints < (repmat(3, numViablePoints, 1) * bmin);
%         % FIXME find points close enough to the projection value not to the
%         %       bmin, i.e. also to bmax or inside, dep. on the value
% %        bmax = evalModelOPTIONS.paramBoundUpper(localModifiableParameterIdxs);
% %        proj = evalModelOPTIONS.paramFixValues(localModifiableParameterIdxs);
% %        imin = (bmin > proj); imax = (proj > bmax); iint = ~imin & ~imax;
% %        closeEnough = min((bmax - bmin)/ 10, max_pts_dist / 10); % ??
% %        for vp in rows of viablePoints
% %            imin: vP - bmin < closeEnough
% %            or
% %            imax: bmax - vP < closeEnough
% %            or
% %            iint: abs(vP - proj) < closeEnough
% %        end
%     else
        [projectableSpecParamIdxsCell, projectableDimIdxsCell] = getProjectableIdxs(globalProjection, options, evalModelOPTIONS);

        %% Check one-at-a-time (OAT) projection for all (couples of) params
        % OATsumViableProjections(i,j) == true iff there exist a coupled
        % params set J containing j, such that parameter point p^i is viable
        % viable after setting p^i_J = projectionValues(J) (i.e. for all
        % params in the params couple J); we say that J is a one-at-a-time
        % (OAT) non-essential coupling of parameters.
        OATsumViableProjections = false(numViablePoints,dim);

        % options.pointsAsOuter: parallelise per samples (true) or per
        % parameters (false)
        if options.pointsAsOuter
            % for each parameter point sample find OAT non-essential parameters
            % loop over all parameter points samples

            % sort reductions by size for a speed-up (with MaxProjectionSet
            % smaller reductions will be skipped in getViableOATProjections
            % if bigger was found viable earlier)
            [~, Inumel] = sort(cellfun(@numel,projectableSpecParamIdxsCell),'descend');
            projectableSpecParamIdxsCellSorted = projectableSpecParamIdxsCell(Inumel);
            projectableDimIdxsCellSorted = projectableDimIdxsCell(Inumel);

            viableOATprojectionsSetCa = cell(numViablePoints,1);
            %for iPoint = 1:numViablePoints % DEBUG
            parfor (iPoint = 1:numViablePoints, options.numWorkers)
                [OATsumViableProjections(iPoint,:), viableOATprojectionsSetCa{iPoint}] = getViableOATProjections(viablePoints(iPoint,:), projectableSpecParamIdxsCellSorted, projectableDimIdxsCellSorted, options, evalModelOPTIONS, threshold);
            end
        else
            % for parameter find samples in which the paramter is OAT non-essential
            % loop over all parameters
            nIdxs = numel(projectableSpecParamIdxsCell);
            OATsumViableProjectionsTemp = false(size(viablePoints,1),nIdxs);
            viableOATprojectionsSetCa = cell(nIdxs,1);
            %for iIdxs = 1:nIdxs % DEBUG
            parfor (iIdxs = 1:nIdxs, options.numWorkers)
                specParamIdxs = projectableSpecParamIdxsCell{iIdxs};
                dimIdxs = projectableDimIdxsCell{iIdxs};
                [OATsumViableProjectionsTemp(:,iIdxs), viableOATprojectionsSetCa{iIdxs}] = getViableOATProjectionsByDim(viablePoints, specParamIdxs, dimIdxs, options, evalModelOPTIONS, threshold);
            end
            for iIdxs = 1:nIdxs
                dimIdxs = projectableDimIdxsCell{iIdxs};
                % | in case of assymetric coupling of params, such that viable
                % reductions either for coupled or uncoupled variant are not
                % overwritten
                OATsumViableProjections(:,dimIdxs) = OATsumViableProjections(:,dimIdxs) | OATsumViableProjectionsTemp(:,iIdxs);
            end
        end
%     end
    OATViableProjectionsSet = options.projectionSetCls();
    OATViableProjectionsSet.addAll([viableOATprojectionsSetCa{:}]);

    % apply coupling rules to rows of OATsumViableProjections which may now
    % include coupling-wise invalid projections
    for i = 1:size(OATsumViableProjections,1)
        OATsumViableProjection = OATsumViableProjections(i,:);
        extendedOATsumProjection = extendVec(globalProjection, globalProjection, OATsumViableProjection);
        extendedOATsumProjection = options.paramCouplingApplyFcn(...
            options.couplingMatrix, extendedOATsumProjection,...
            extendedOATsumProjection | evalModelOPTIONS.possibleToFixSpecParamsMask);
        OATsumViableProjections(i,:) = extendedOATsumProjection(:, localModifiableParameterIdxs);
    end

    %% Check sums of OAT viable projections
    % get all unique projections of parameters to the axes
    % Note: not using here MaxProjectionSet.punique (avialable via
    %       newViableProjectionSet instance), because we want to check
    %       smaller projections for viability when bigger ones turn out to
    %       be non-viable (bigger checked first because of 'sorted')
    [OATsumViableProjectionsUniqueSorted, ~, IOATsumViableProjectionsUnique] = ProjectionSet.punique(OATsumViableProjections, 'sorted');

    % Remove the empty projection and its associated viable points
    % (becase we sorted, then, if the empty projections exists, it is always
    % a first row in viableOATprojections)
    if ~any(OATsumViableProjectionsUniqueSorted(1,:))
        OATsumViableProjectionsUniqueSorted(1,:) = [];
        viablePoints(IOATsumViableProjectionsUnique==1,:) = [];
        IOATsumViableProjectionsUnique(IOATsumViableProjectionsUnique==1,:) = [];
        IOATsumViableProjectionsUnique = IOATsumViableProjectionsUnique - 1;
    end

    % Loop over all viable OAT-sum projections
    % Start with the largest projections in case of omitting the smaller
    % reductions (with options.projectionSetCls being @MaxProjectionSet).
    for ipunique = size(OATsumViableProjectionsUniqueSorted,1):-1:1
        viableOATsumProjection = OATsumViableProjectionsUniqueSorted(ipunique,:);
        extendedOATsumProjection = extendVec(globalProjection, globalProjection, viableOATsumProjection);
        % don't check OAT-sum projection if:
        %  1) it is in the global cache;
        %  2) it has been already found in this call (possible w/ MaxProjectionSet)
        if ~globalViableProjectionSet.has(extendedOATsumProjection) && ~newViableProjectionsSet.has(viableOATsumProjection)
            [isOAT, iOAT] = OATViableProjectionsSet.has(viableOATsumProjection);
            if isOAT % OAT-sum is OAT => already checked, add directly
                if debugging
                    fprintf('[DEBUG] getViableProjections, adding directly previosly checked OAT and OAT-sum viable projection for model param indices = %s.\n', modifiableModParamStr(extendedOATsumProjection, evalModelOPTIONS));
                end
                [~, viableOATprojectionPoint] = OATViableProjectionsSet.at(iOAT);
                newViableProjectionsSet.add(viableOATsumProjection, viableOATprojectionPoint);
            else
                if debugging
                    fprintf('[DEBUG] getViableProjections, checking OAT-sum viable projection for model param indices = %s.\n', modifiableModParamStr(extendedOATsumProjection, evalModelOPTIONS));
                end
                % get all parameter points corresponding to this viable projection
                V = viablePoints(IOATsumViableProjectionsUnique == ipunique,:);
                % check viability of viableOATprojectionI, backtrack if necessary
                addIfViableProjection(viableOATsumProjection, V, newViableProjectionsSet,...
                    threshold, globalProjection, globalViableProjectionSet,...
                    options, evalModelOPTIONS);
            end
        else
            if debugging
                fprintf('[DEBUG] getViableProjections, skipping previously found OAT-sum viable projection for model param indices = %s.\n', modifiableModParamStr(extendedOATsumProjection, evalModelOPTIONS));
            end
        end
    end

    if ~options.dropOATProjections
        % Add directly to newViableProjectionsSet directly all previously
        % checked OAT viable projections (one couple of params at a time).
        %
        % Note: OAT viable proj. that are also OAT-sum viable proj. are
        %       already added to newViableProjectionsSet.
        %
        % Do it only after the maximal projections have been checked, in
        % case a bigger projection has already been found (then skipped w/
        % MaxProjectionSet).
        %
        % Attention: doesn't check if these are already in globalViableProjectionSet
        %
        Iadded = newViableProjectionsSet.addAll(OATViableProjectionsSet);
        % OPT ensure that newViableProjectionsSet never intersects with
        %     globalViableProjectionSet
        %     ==> implement row-wise extendVec() and has()
        if debugging
            fprintf('[DEBUG] getViableProjections, additionally added %d previously found viable OAT projections.\n',...
            numel(Iadded));
            %fprintf('[DEBUG] getViableProjections: adding previously found viable OAT projection for param indices = %s.\n',...
            %    mat2str(specParamIdxs));
        end
    end

    % Reset evalModel options
    evalModelOPTIONS.varySpecParamIdxs(fixedSpecParamsMask);
end



function str = modifiableModParamStr(globalProjection, evalModelOPTIONS)
    str = mat2str(evalModelOPTIONS.modifiableModParamIdxs(globalProjection));
end



function [projectableSpecParamIdxsCell, projectableDimIdxsCell] = ...
    getProjectableIdxs(globalProjection, options, evalModelOPTIONS)
%
debugging = (debuglevel > 0);
    % global parameter space dimension
    gDim = numel(globalProjection);
    % parameter indices than do not have fixed value = modifiable
    localModifiableParameterIdxs = find(~globalProjection);
    % local (modifiable) parameter space dimension
    dim = numel(localModifiableParameterIdxs);
    projectableSpecParamIdxs = evalModelOPTIONS.possibleToFixSpecParamIdxs;
    % translate global projectable inidices to the local indices 1:dim
    projectableDimIdxs = 1:dim;
    projectableDimIdxs = projectableDimIdxs(...
        ismember(localModifiableParameterIdxs, projectableSpecParamIdxs));
    % get all parameters couplings, up to the given exhaustive search rank
    [projectableSpecParamIdxsCell, projectableDimIdxsCell] = coupleParamIdxs(...
            options.paramCouplingApplyFcn, options.couplingMatrix,...
            evalModelOPTIONS.fixedSpecParamIdxs, projectableSpecParamIdxs,...
            projectableDimIdxs, gDim, min(options.exhaustiveRank, dim));
    if debugging
        fprintf('[DEBUG] getViableProjections>getProjectableIdxs, couples of projectable param spec indices (%d): {%s} (local: {%s}).\n',...
            numel(projectableSpecParamIdxsCell),...
            strjoin2(cellfun(@mat2str, projectableSpecParamIdxsCell(:), 'UniformOutput', false),','),...
            strjoin2(cellfun(@mat2str, projectableDimIdxsCell(:), 'UniformOutput', false),','));
    end
end



function [viableOATprojectionsRow, viableOATprojectionsSet] = ...
    getViableOATProjections(viablePoint,...
        projectableSpecParamIdxsCell, projectableDimIdxsCell,...
        options, evalModelOPTIONS, threshold...
    )
%
    dim = numel(viablePoint);
    dimAll = (1:dim);
    viableOATprojectionsRow = false(1,dim);
    viableOATprojectionsSet = options.projectionSetCls();
    for iIdxs = 1:numel(projectableSpecParamIdxsCell)
        specParamIdxs = projectableSpecParamIdxsCell{iIdxs};
        dimIdxs = projectableDimIdxsCell{iIdxs};
        projection = ismember(dimAll, dimIdxs);
        % skip if a bigger reduction was not already found (w/ MaxProjectionSet)
        if ~viableOATprojectionsSet.has(projection)
            paramProjectionValues = getParamProjectionValues(evalModelOPTIONS);
            % tell evalModel which parameters are projected; here, these
            % should be only those that actually can be projected
            fixedParamSpecIdxs = evalModelOPTIONS.fixSpecParamIdxs(specParamIdxs);
            isViableProjection = addIfViablePoint(...
                viablePoint, projection, paramProjectionValues,...
                threshold, viableOATprojectionsSet...
            );
            if isViableProjection
                viableOATprojectionsRow(dimIdxs) = true;
            end
            % add back projected params as variable for the next OAT projection
            evalModelOPTIONS.varySpecParamIdxs(fixedParamSpecIdxs);
       end
    end
end

function [viableOATprojectionsCol, viableOATprojectionsSet] = ...
    getViableOATProjectionsByDim(viablePoints,...
        specParamIdxs, dimIdxs,...
        options, evalModelOPTIONS, threshold...
    )
%
    n = size(viablePoints,1);
    dim = size(viablePoints,2);
    dimAll = (1:dim);
    projection = ismember(dimAll, dimIdxs);
    viableOATprojectionsCol = false(n,1);
    viableOATprojectionsSet = options.projectionSetCls();
    paramProjectionValues = getParamProjectionValues(evalModelOPTIONS);
    % tell evalModel which parameters are projected
    fixedSpecParamIdxs = evalModelOPTIONS.fixSpecParamIdxs(specParamIdxs);
    for i = 1:n
        isViableProjection = addIfViablePoint(...
            viablePoints(i,:), projection, paramProjectionValues,...
            threshold, viableOATprojectionsSet...
        );
        if isViableProjection
            viableOATprojectionsCol(i) = true;
        end
    end
    % add back projected params as variable
    evalModelOPTIONS.varySpecParamIdxs(fixedSpecParamIdxs);
end


function paramProjectionValues = getParamProjectionValues(evalModelOPTIONS)
    paramProjectionValues = evalModelOPTIONS.paramFixValues(evalModelOPTIONS.unfixedSpecParamIdxs);
end

function [isViableProjection, added] = addIfViablePoint(...
        paramPoint, projection, paramProjectionValues,...
        threshold, projectionSet...
)
% Beware: modifies projectionSet
%
    projectedParamPoint = paramPoint(:,~projection);
    % check if cost is smaller than the defined threshold
    isViableProjection = (evalModel(projectedParamPoint) < threshold);
    if isViableProjection
        % save this projection and the witness parameter values
        projectedParamPoint = extendVec(projection, paramProjectionValues, projectedParamPoint);
        added = projectionSet.add(projection, projectedParamPoint);
    end
end


function addIfViableProjection(projectedParams, V, viableProjectionsSet, ...
    threshold, globalProjection, globalViableProjectionSet, ...
    options, evalModelOPTIONS)
%ADDIFVIABLEPROJECTION Add projectedParams projection to
% viableProjectionsSet if at least one candidate point V is viable.
% Optionally, backtrack if no conadidate point is viable.
%
% Inputs:
%     projectedParams (logical): vector describing a current (local)
%         projection
%     V (numeric): matrix of candidate points (in rows) to check for
%         viability (check breaks at the first viable point found)
%     viableProjectionsSet (ProjectionSet): set to add projection to if
%         a viable point has been found
%     ...
%
debugging = (debuglevel > 0);

% OPT return max likelihood parameter sample out of tested ones &
%    the likelihood, not the last one checked
%    Requires: tracking cost, e.g. globally by extra column in the `viablePoints` param

    paramProjectionValues = getParamProjectionValues(evalModelOPTIONS);
    % tell evalModel which parameters are now projected
    currentGlobalProjection = extendVec(globalProjection, globalProjection, projectedParams);
    fixedParamSpecIdxs = evalModelOPTIONS.fixSpecParamIdxs(currentGlobalProjection);

    % loop over all the maximally projected parameter points to find if
    % there is at least one which is valid
    isViableProjection = false;
    for k = 1:size(V,1)
        isViableProjection = addIfViablePoint(...
            V(k,:), projectedParams, paramProjectionValues, threshold,...
            viableProjectionsSet...
        );
        % break on the first witness
        if isViableProjection
            if debugging
                fprintf('[DEBUG] getViableProjections>addIfViableProjection: found viable projection for param indices = %s.\n', ...
                    modifiableModParamStr(currentGlobalProjection, evalModelOPTIONS));
            end
            break;
        end
    end

    % set back modifiable params before optional tracking back up the
    % model tree
    evalModelOPTIONS.varySpecParamIdxs(fixedParamSpecIdxs);

    % BACKTRACKING
    % if the projection was not found to be viable try out all possible
    % subsets of the current projection where one parameter is set to be
    % essential
    if (~isViableProjection) && options.backtrack
        parameterIdxs = find(projectedParams);
        for k = 1:numel(parameterIdxs)
            newProjection = projectedParams;
            newProjection(parameterIdxs(k)) = false;
            % if this projection has not been identified as viable
            % globally or in a previous backtracking procedure check it
            extendedNewProjection = extendVec(globalProjection, globalProjection, newProjection);
            if ~globalViableProjectionSet.has(extendedNewProjection) && ~viableProjectionsSet.has(newProjection)
                if debugging
                    fprintf('[DEBUG] getViableProjections>addIfViableProjection: backtracking for param indices = %s.\n',...
                        modifiableModParamStr(extendedNewProjection, evalModelOPTIONS));
                end
                addIfViableProjection(newProjection, V, viableProjectionsSet, ...
                    threshold, globalProjection, globalViableProjectionSet,...
                    options, evalModelOPTIONS);
            else
                if debugging
                    fprintf('[DEBUG] getViableProjections>addIfViableProjection: skipping backtracking for already found viable projection for param indices = %s.\n',...
                        modifiableModParamStr(extendedNewProjection, evalModelOPTIONS));
                end
            end
        end
    end
end
