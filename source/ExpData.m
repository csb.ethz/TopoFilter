classdef ExpData < CachableProperties
%EXPDATA Experimental data is a collection of measurements for multiple
% experiments. Measurements within experiment contain time series for multiple
% observables with min and max values to account for measurement error.
%
% Properties (object fields):
%    experiments   (struct array) : array of single experiment data structures
%                                   (see below)
%    S             (numeric array): square matrix of measurements covariances
%    extractSimDataFcn (function handle): function to extract simulation data
%    extractSimDataFcnArgs (cell array): cell array with extra args for
%                                        extractSimDataFcn
%    calcCostFcn (function handle): function to compute cost for numerical
%                                   simulations with respect to the data
%    calcCostFcnArgs (cell array): cell array with extra args for calcCostFcn
%    calcThresholdFcn (function handle): function to compute cost threshold for
%                                        the data
%    calcThresholdFcnArgs (cell array): cell array with extra args for
%                                       calcThresholdFcn
%
% Dependent properties:
%    threshold  (double)        : calcThresholdFcn value for this data and
%                                 the calcThresholdFcnArgs
%
% Single experiment structure fields:
%    name         (string)    : string name of the experiment
%    observables  (cell array): string names of observable variables for
%                               measurements
%    measurements (cell array): 4 column matrices, one for each observable;
%                               columns correspond, respectively, to time point,
%                               measurement value, min value, and max value
%
% Cachable read-only properties (MATLAB methods w/o arugments):
%    tps            (cell array): cell array of vector of (unique) time points
%                                 in each experiment
%    maxT       (numeric vector): maximum time point for each experiment
%    values     (numeric vector): measurement values in a flat vector form, in
%                                 order of first experiments, then observables.
%    minvalues  (numeric vector): minimal values in a flat vector form
%    maxvalues  (numeric vector): maximal values in a flat vector form
%    sd         (numeric vector): standard deviations of all measurements in
%                                 a flat vector form, calculated as a each
%                                 measurement max value minus min value divided
%                                 by two
%    invS       (numeric array) : inverse of the covariance matrix
%    logDetS    (double)        : logarithm of the determinant of the covariance
%                                 matrix
%
% Note: to force re-computation of cachable properties pass '-f' or '--force'
%       argument, i.e. obj.cachable_property('-f')
% Note: S matrix is also a property (computed as diag(expData.sd()) in the
%       constructor), but it is not read-only (can be manually overriden), hence
%       as a field.
%
    properties
        experiments
        S
        extractSimDataFcn
        extractSimDataFcnArgs
        calcCostFcn
        calcCostFcnArgs
        calcThresholdFcn
        calcThresholdFcnArgs
    end
    properties (Dependent, Access = public)
        threshold
    end
    properties (Access = ?CachableProperties)
        p_tps = {};
        p_maxT = [];
        p_values = [];
        p_minvalues = [];
        p_maxvalues = [];
        p_sd = [];
        p_invS = [];
        p_logDetS = [];
    end
    methods (Access = protected)
      %% Cachable properties (computation)
      function values = compTps(expData)
          values = cell(numel(expData.experiments),1);
          for i=1:numel(values)
              experiment = expData.experiments(i);
              values{i} = unique(flattenData({experiment.measurements}, @(m) m(:,1)));
          end
      end
      function values = compMaxT(expData)
          values = cellfun(@max, expData.tps);
      end
      function values = compValues(expData)
          values = flattenData({expData.experiments.measurements}, @(m) m(:,2));
      end
      function minvalues = compMinvalues(expData)
          minvalues = flattenData({expData.experiments.measurements}, @(m) m(:,3));
      end
      function maxvalues = compMaxvalues(expData)
          maxvalues = flattenData({expData.experiments.measurements}, @(m) m(:,4));
      end
      function sd = compSd(expData)
          sd = (expData.maxvalues - expData.minvalues) / 2;
      end
      function invS = compInvS(expData)
          if isdiagonal(expData.S)
              % avoiding numerical problems (and computation time)
              invS = diag(1./diag(expData.S));
          else
              invS = inv(expData.S);
          end
      end
      function logDetS = compLogDetS(expData)
          if isdiagonal(expData.S)
              % avoiding numerical problems (and computation time)
              logDetS = sum(log(diag(expData.S)));
          else
              logDetS = log(det(expData.S));
          end
      end
    end
    methods
      %% Constructors
      function expData = ExpData(filenames, experimentsIdx, opts)
      % EXPDATA Reads IQMmeasurement experimental data from IQMmeasurment input
      % format files (multi-sheet XLS files, or multiple CSV files), with
      % required min and max values for each measurement.
      %
      % Arguments:
      %
      %   filenames (char or char cell array): file name or list of file names
      %     with the IQMmeasurment input data. Note: for multiple experiments one
      %     can use either single XLS file with multiple sheets or multiple CSV
      %     files.
      %
      %   experimentsIdx (int vector; optional): list of indices to filter
      %     experiments when read from multi-sheet XLS files.
      %     TODO use measurement data names as i dentificators (under assumption
      %     of their uniquenss)
      %
      %   opts (struct, optional): structure with the following options
      %     corresponding directly to ExpData properties (see there for
      %     description): extractSimDataFcn, extractSimDataFcnArgs, calcCostFcn,
      %     calcCostFcnArgs, calcThresholdFcn, calcThresholdFcnArgs
      %
          filenames = idOrCell(filenames);
          dataAll = cell(numel(filenames),1);
          for i = 1:length(filenames)
              dataAll{i} = IQMmeasurement(filenames{i});
          end
          dataAll = flattenCell(dataAll);

          if isempty(experimentsIdx)
              experimentsIdx = 1:numel(dataAll);
          end
          nExp = numel(experimentsIdx);

          % pre-allocate known struct; alt: start w/ last index
          experiments = repmat(struct('name',[],'measurements',[],'observables',[]), nExp, 1);
          for i = 1:nExp
              sbexp = dataAll{experimentsIdx(i)};
              sbexpstr = IQMstruct(sbexp);
              expDataI.name = sbexpstr.name;
              [expDataI.measurements, expDataI.observables] = decoupleIQMmeasurementdata(sbexp);
              experiments(i) = expDataI;
          end

          expData.experiments = experiments;
          expData.S = diag(expData.sd.^2);

          opts = validateFcnOpt(opts, 'extractSimDataFcn', @extractSimData,...
            'extractSimDataFcnArgs');
          expData.extractSimDataFcn = opts.extractSimDataFcn;
          expData.extractSimDataFcnArgs = opts.extractSimDataFcnArgs;

          opts = validateFcnOpt(opts, 'calcCostFcn', @calcCost,...
            'calcCostFcnArgs', {expData.invS, expData.logDetS});
          expData.calcCostFcn = opts.calcCostFcn;
          expData.calcCostFcnArgs = opts.calcCostFcnArgs;

          opts = validateFcnOpt(opts, 'calcThresholdFcn', @calcThresholdQuantile,...
            'calcThresholdFcnArgs', {size(expData.S,1), expData.logDetS});
          expData.calcThresholdFcn = opts.calcThresholdFcn;
          expData.calcThresholdFcnArgs = opts.calcThresholdFcnArgs;

      end

      %% Dependent properties
      function ret = get.threshold(expData)
      %THRESHOLD non-cachable property
      %
          ret = expData.calcThresholdFcn(expData.calcThresholdFcnArgs{:});
      end

      %% Cachable properties (wrappers)
      function ret = tps(expData, varargin)
      %TPS cachable property, with option to force re-computation
          ret = getCacheProperty(expData, 'p_tps', @expData.compTps, varargin{:});
      end
      function ret = maxT(expData, varargin)
      %MAXT cachable property, with option to force re-computation
          ret = getCacheProperty(expData, 'p_maxT', @expData.compMaxT, varargin{:});
      end
      function ret = values(expData, varargin)
      %VALUES cachable property, with option to force re-computation
          ret = getCacheProperty(expData, 'p_values', @expData.compValues, varargin{:});
      end
      function ret = minvalues(expData, varargin)
      %VALUES cachable property, with option to force re-computation
          ret = getCacheProperty(expData, 'p_minvalues', @expData.compMinvalues, varargin{:});
      end
      function ret = maxvalues(expData, varargin)
      %VALUES cachable property, with option to force re-computation
          ret = getCacheProperty(expData, 'p_maxvalues', @expData.compMaxvalues, varargin{:});
      end
      function ret = sd(expData, varargin)
      %SD cachable property, with option to force re-computation
          ret = getCacheProperty(expData, 'p_sd', @expData.compSd, varargin{:});
      end
      function ret = invS(expData, varargin)
      %INVS cachable property, with option to force re-computation
          ret = getCacheProperty(expData, 'p_invS', @expData.compInvS, varargin{:});
      end
      function ret = logDetS(expData, varargin)
      %LOGDETS cachable property, with option to force re-computation
          ret = getCacheProperty(expData, 'p_logDetS', @expData.compLogDetS, varargin{:});
      end


      %% Methods
      function simDataPts = extractSimData(expData, nsols)
      %EXTRACTSIMDATA From an array of IQMPsimulate numerical solutions extract
      % simluation data points corresponding to experimental data.
      %
          nExp = numel(expData.experiments);
          assert(nExp == numel(nsols), ...
              'Simulation data expected for each measurement data experiment.');
          simDataCa = cell(nExp, 1);
          for i = 1:nExp
              nsol = nsols(i);
              m = expData.experiments(i);
              nY = numel(m.observables);
              simdat = cell(nY,1);
              for k=1:nY
                  simdat{k} = expData.extractSimDataFcn(nsol,...
                      m.observables(k), m.measurements{k},...
                      expData.extractSimDataFcnArgs{:});
              end
              simDataCa{i} = simdat;
          end
          simDataPts = flattenData(simDataCa);
      end

      function varargout = cost(expData, nsols, npar)
      %COST extractSimData and evaluate calcCostFcn (compute error between
      % simulated and experimental data).
      %
          N = nargout(@expData.calcCostFcn);
          [varargout{1:N}] = expData.calcCostFcn( ...
              expData.extractSimData(nsols), expData.values, npar, ...
              expData.calcCostFcnArgs{:});
      end
  end
end


function flat = flattenData(dataCa, fHandle, varargin)
    if ~exist('fHandle','var')
        fHandleWrapper = @(x) x;
    else
        fHandleWrapper = @(x) fHandle(x, varargin{:});
    end
    flatCa = cellfun(fHandleWrapper, flattenCell(dataCa) ,'UniformOutput', false);
    flat = vertcat(flatCa{:});
end


function [ opts ] = validateFcnOpt(optsIn, fcnOptName, fcnDefault,...
    fcnArgsOptName, varargin )
%VALIDATEFCNOPT ...
%
    if isempty(varargin)
        fcnArgsDefault = {};
    else
        fcnArgsDefault = varargin{1};
    end
    opts = optsIn;
    if isfield(opts, fcnOptName)
        if ~isf(opts.(fcnOptName))
            error('VALIDATEFCNOPT:IllegalArgument',...
                'Expecting `%s` to be a function name or handle.', fcnOptName);
        end
    else
        opts.(fcnOptName) = fcnDefault;
    end
    if isfield(opts, fcnArgsOptName)
        fcnArgs = opts.(fcnArgsOptName);
        if ~iscell(fcnArgs)
            error('VALIDATEFCNOPT:IllegalArgument',...
                'Expecting `%s` to be a cell array of extra arguments for the `%s` function.',...
                    fcnArgsOptName, fcnOptName);
        elseif isempty(fcnArgs)
            % set default parameters if none given
            % Note: assuming here that the function either needs the same
            % default args as the default function or that it does not care for
            % them (ignores them e.g. via varargin)
            opts.(fcnArgsOptName) = fcnArgsDefault;
        end
    else
        opts.(fcnArgsOptName) = fcnArgsDefault;
    end
end
