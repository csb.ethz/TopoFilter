%% plot viable points

%loaded = load('results/160222_182339_viablePoints_sampled_2091542_000000000000000000000000111111111111111111.mat');
%loaded = load('results/160225_110149_viablePoints_sampled_2115758_000000000000000000000000111111111111111111.mat');
loaded = load('results/160519_173920_viablePoints_sampled_2148053_00');
viablePoints = loaded.viablePoints;

V = viablePoints.rowmat;
c = viablePoints.cost;
l = viablePoints.colnames;
islog = viablePoints.islog;


nV = size(V,2);
maxsbn = 16; % max nr of subplot rows and columns

idxcheck = false(nV,nV); % sanity check: prep

d = eqdrawers(nV,ceil(nV/maxsbn));
ki = 0;
for oi=1:numel(d)
    n = d(oi);    
    kj = 0;    
    for oj=1:oi
        m = d(oj);

        sb = @(i,j) subplot(n,m,(j-1)*n+i);
        figure;
        for i = 1:n
            for j = 1:m
                ic = i+ki;
                jc = j+kj;
                % skip duplicates on a diagonal
                if jc >= ic, break; end;
                idxcheck(ic,jc) = true; % sanity check: flag
                
                x = V(:,ic); y = V(:,jc);
                lx = l{ic}; ly = l{jc};
                if (islog(ic)), lx = sprintf('log_{10}(%s)',lx); end
                if (islog(jc)), ly = sprintf('log_{10}(%s)',ly); end
                sb(i,j);
                scatter(x,y,[],c,'filled'); xlabel(lx); ylabel(ly);
            end
        end

        if n == 1, colorbar;
        elseif oi == oj
            sb(1,m); scatter(1:numel(c),1:numel(c),[],c,'filled'); colorbar;
        end

        kj = kj+m;
    end
    
    ki = ki+n;
end

assert(numel(find(idxcheck)) == nV*(nV-1)/2); % sanity check: execute
