function result = isdiagonal(A)
%ISDIAGONAL isdiag (>=R2014b), or own implementation (<=R2014a) to check whether
%  matrix is diagonal.
    if ~exist('isdiag', 'builtin')
        [I,J] = find(A);
        if ~isempty(I)
            result = all(I == J);
        else
            % make the simple choice that an all zero matrix
            % or an empty array is by definition diagonal, since
            % it has no non-zero off diagonals.
            result = true;
        end
    else
        result = isdiag(A);
    end
end
