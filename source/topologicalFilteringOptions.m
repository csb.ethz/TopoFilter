function [ optStruct ] = topologicalFilteringOptions( settings )
%TOPOLOGICALFILTERINGOPTIONS Prepare options for the `topologicalFiltering`
%   function.
%
%   Nothing fancy here, just setting the struct fields.
%

% topologicalFilteringOptions
optStruct.nmont = settings.nmont;
optStruct.nelip = settings.nelip;
optStruct.nvmin = settings.nvmin;
optStruct.nvmax = settings.nvmax;
optStruct.dropMontPoints = settings.dropMontPoints;
optStruct.saveViablePoints = settings.saveViablePoints;
optStruct.recursive = settings.recursive;

optStruct.parallelize = settings.parallelize;
optStruct.numWorkers = settings.numWorkers;

optStruct.outputFilename = settings.outputFilename;

optStruct.projectionSetCls = settings.projectionSetCls;

% static options for getViableProjections
optStruct.getViableProjectionsOPTIONS = getViableProjectionsOptions(settings);

end
