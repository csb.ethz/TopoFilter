function projVecThen = paramCouplingApply(coupling, projectedVec, projectableVec)
%PARAMCOUPLINGAPPLY Apply parameters coupling to a given logical projection
% vector, respecting optional logical vector denoting projectable parameters,
% i.e. a parameter will be additionally projected if it is both projected
% via coupling rule and if it is actually projectable. If not specified, all
% parameters are considered projectable.
%
% Returns logical vector with input projection extended by coupling rules.
%
% Arguments:
%    coupling (cell array or array): n x 2 cell array as returned by
%       paramCoupling, or n x d x 2 logical array as returned by
%       paramCouplingCaToMatrix
%    projectedVec (numeric/logical): 1 x d projected params vector
%    projectableVec (numeric/logical): 1 x d projectable params mask
%

    % ensure logical and row-vec
    projVec = logical(projectedVec(:)');
    d = numel(projVec);

    if iscell(coupling)
        couplingMatrix = paramCouplingCaToMatrix(coupling, d);
    else
        couplingMatrix = logical(coupling);
    end
    assert(ndims(couplingMatrix) == 3 && size(couplingMatrix, 3) == 2 && size(couplingMatrix, 2) == d,...
        'Expected a n x %d x 2 array of params couplings.', d);
    couplingMatrixIf = couplingMatrix(:,:,1);
    couplingMatrixThen = couplingMatrix(:,:,2);

    if nargin < 3
        projectableVec = true(1, d);
    else
        assert((islogical(projectableVec) || isnumeric(projectableVec)) && numel(projectableVec) == d,...
            'Expected a vector of projectable params with %d elements, not %d.', d, numel(projectableVec));
        projectableVec = logical(projectableVec(:)');
    end

    projVecThen = projVec;
    % look for applicable coupling rows, i.e. such that all of the coupling
    % premise params are projected and all of the coupling conclusion
    % params are projectable; sum up resulting couplings for all such rows
    for j=1:size(couplingMatrixIf,1)
        couplingVecThen = couplingMatrixThen(j,:);
        if all(couplingMatrixIf(j,:) <= projVecThen)
            projVecThen = projVecThen | (couplingVecThen & projectableVec);
        end
    end

end % function
