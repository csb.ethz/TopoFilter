function [true_idxs, false_idxs] = splitIdx(idxs, mask)
%SPLITIDX Split indices idxs according to a logical mask into sets of indices
% for which mask is true and for which it is false, i.e. such that
%    all(mask(true_idxs)) == true
%    ~any(mask(false_idxs)) == true
% and, for logical indices idxs:
%    idxs == true_idxs | false_idxs
% or, for integer indices idxs:
%    sort(idxs) = sort(horzcat(true_idxs, false_idxs))
% (+/- vertcat).
%
    assert(islogical(mask), 'Mask must be a vector of logicals.');
    if islogical(idxs) %|| (numel(idxs)==numel(mask) && ~any(idxs - logical(idxs)))
        true_idxs = mask & idxs;
        false_idxs = idxs & ~true_idxs;
    else
        submask = mask(idxs);
        true_idxs = idxs(submask);
        false_idxs = idxs(~submask); % == setdiff(idxs, true_idxs)
    end
end
