function [ bool ] = isf(x)
%ISF Test if x can be called with feval: isfhandle or exist(x, 'file').
%
    bool = isfhandle(x) || exist(x, 'file');
end
