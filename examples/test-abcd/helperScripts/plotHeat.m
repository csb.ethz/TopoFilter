% Script to plot count data as heatmaps to visualize
% influence of two factors (e.g. number of timepoints
% standard deviation).

timepoints = [3 5 8];
stds = [5 10 15];
counts = zeros(2^7, length(timepoints), length(stds));
for i = 1:length(timepoints)
	for j = 1:length(stds)
		load(sprintf('../results/%d_%d.mat', timepoints(i), stds(j)));
		counts(:,i, j) = generateCountVector(runs);
	end
end

foundProjIdxs = find(sum(sum(counts, 3), 2));
titleStr = dec2bin(foundProjIdxs,7);

figure;

nrows = 4;
ncols = 7;

%colormap('hot');
for i = 1:length(foundProjIdxs)
    subplot(nrows, ncols, i);
    h = imagesc(squeeze(counts(foundProjIdxs(i),:,:)),[0 10]);
    title(titleStr(i,:));
    set(gca,'Xtick',1:3,'XTickLabel',stds)
    set(gca,'Ytick',1:3,'YTickLabel',timepoints)
    if (mod(i-1,ncols) == 0)
       ylabel('# timepoints');
    end
    if i > length(titleStr)-ncols
       xlabel('std');
    end
end

subplot(nrows, ncols, i+1);
imagesc(0:10);
title('legend');
set(gca,'Xtick',1:11,'XTickLabel',0:10)
set(gca,'YTickLabel','');

subplot(nrows, ncols, i+2);
imagesc(squeeze(sum(counts)));
colorbar;
set(gca,'Xtick',1:3,'XTickLabel',stds);
set(gca,'Ytick',1:3,'YTickLabel',timepoints);
title('Sum');

% Here we preserve the size of the image when we save it.
width = 8/5 * ncols;     % Width in inches
height = 3/2 * nrows;    % Height in inches
% set(gcf,'InvertHardcopy','on');
set(gcf,'PaperUnits', 'inches');
set(gcf, 'PaperUnits', 'inches');
set(gcf, 'PaperSize', [width height]);
set(gcf, 'PaperPositionMode', 'manual');
set(gcf, 'PaperPosition', [0 0 width height]);

% % Save the file as PDF
%set(gcf, 'renderer', 'painters');
%print(['~/Polybox/TopoFiltering/Report/figures/model2/' outputstring],'-dpdf','-r300')