Kuepfer yTOR example
====================

Source
------
Kuepfer, L. et al., 2007, "Ensemble modeling for analysis of cell signaling
dynamics", Nature Biotechnology, 25(9), pp.1001–1006.

Filtering
---------
Using model #2 from the ensemble of models, as well as part of the data and
experiments described in the paper.

Parameter specification according to original optimisation problem description,
limited to 25 parameters. Fixed parameters with low optimal value variablility
among models viable with respect to used data, i.e. models nr 1-6.
