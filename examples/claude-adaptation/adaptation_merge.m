function [] = adaptation_merge()
enumerated_models = dir('results/');
projectionsCellArray.projsign = {};
projectionsCellArray.models = {};
projectionsCellArray.projections = {};
mkdir('merged')
    for k = 3:length(enumerated_models),
        model = enumerated_models(k).name;
        indexsigns = regexpi(model,'\d{9}');
        modelsigns = model(indexsigns:(indexsigns+8));
        modelsignsord = modelsigns;
        modelsignsord(1:9) = modelsigns([1 4 7 2 5 8 3 6 9]);
        projectionlist = dir(['results/' model '/*viablePoints*']);
        for l = 1:length(projectionlist);
            projection = projectionlist(l).name;
            indexproj = regexpi(projection,'\d{26}');
            projectionbit = projection((indexproj+17):(indexproj+25));
            projsign = '++++++++++++++++++';
            for m = 2:2:18,
                s = str2double(modelsignsord(m/2));
                p = str2double(projectionbit(m/2));
                projsign(m-1:m) = num2str((2*s-1)*(1-p),'%+d');
            end
            projectionsCellArray.projsign{end+1} = projsign;
            projectionsCellArray.models{end+1} = model;
            projectionsCellArray.projections{end+1} = projection;
%             copyfile(['results/' model '/' projection],['results/merged/' projsign]);
        end
    end
    A = projectionsCellArray.projsign;
    display(sprintf('%d total number of projections before merging',length(A)));

    [uniqueA, i, ~] = unique(A);
    display(sprintf('%d total number of projections after merging',length(uniqueA)));
    for j = 1:length(i),
        modelname = projectionsCellArray.models(i(j));
        projname = projectionsCellArray.projections{i(j)};
        projsign = projectionsCellArray.projsign{i(j)};
        projinfo = load(['results/' modelname{1} '/' projname]);
        projinfo.models = modelname;
        projinfo.projections = projname;
        save(['merged/' projsign '.mat'],'projinfo');
    end
    [~, i, ~] = unique(A,'first');
    indexToDupes = find(not(ismember(1:numel(A),i)));

    for u = 1: length(indexToDupes),
        v = find(ismember(A,A{indexToDupes(u)}));
        mergedViablePoints.rowmat = [];
        mergedViablePoints.cost = [];
        mergedViablePoints.colnames = {};
        mergedViablePoints.islog = [];
        mergedViablePoints.projections = [];
        mergedViablePoints.projected = struct([]);
        modelnames = projectionsCellArray.models(v);
        projnames = projectionsCellArray.projections(v);
        projinfo = struct;
        projinfo.models = modelnames;
        projinfo.projections = projnames;
        for x = 1:length(v),
            loaded = load(['results/' modelnames{x} '/' projnames{x}]);
            mergedViablePoints.rowmat = [mergedViablePoints.rowmat; loaded.viablePoints.rowmat];
            mergedViablePoints.cost = [mergedViablePoints.cost; loaded.viablePoints.cost];
            mergedViablePoints.colnames = loaded.viablePoints.colnames;
            mergedViablePoints.islog = loaded.viablePoints.islog;
            mergedViablePoints.projections = loaded.viablePoints.projection;
            mergedViablePoints.projected = loaded.viablePoints.projected;
            projinfo.viablePoints = mergedViablePoints;
        end
        filename = A{indexToDupes(u)};
        delete(['merged/' filename '.mat']);
        save(['merged/' filename '.mat'],'projinfo');
    end
end


