function [ cost, paramValues ] = evalModelDirect( varargin )
%EVALMODELDIRECT Simplified version of the evalModel function, for a direct
% evaluation of the experiment cost. Initialise first:
%
% evalModelDirect(mexNames, expData, paramNames, islog)
% evalModelDirect(mexNames, expData, paramNames, islog, fixedParamValuesTail)
% evalModelDirect(mexNames, expData, paramNames, islog, [], opts)
%

persistent fdat;

if ~isnumeric(varargin{1})

    fdat.debugging = (debuglevel > 1);

    fdat.mexNames = varargin{1};
    fdat.expData = varargin{2};
    fdat.paramNames = varargin{3};
    fdat.islog = varargin{4};
    if nargin > 4
        fdat.fixedParamValuesTail = varargin{5};
    else
        fdat.fixedParamValuesTail = [];
    end

    fdat.nPar = numel(fdat.paramNames);
    fdat.nParFix = numel(fdat.fixedParamValuesTail);
    assert(fdat.nParFix <= fdat.nPar, ...
        ['Number of parameters with fixed values is greater than the number '...
         'of all parameter names.']);
    assert((fdat.nPar-fdat.nParFix) == numel(fdat.islog), ...
        ['Size of a flag of logarithmic scale for parameters is inconsistent '...
         'with number of variable parameters (size of all parameter names '...
         'cell minus size of fixed parameter values vector).']);


    % Parse options
    if nargin > 5, opts = varargin{6}; else opts = struct(); end
    parser = inputParser;
    parser.FunctionName = 'EVALMODELDIRECT: ';
    parser.KeepUnmatched = true;
    addParameter(parser, 'solNrTimepoints', 0, @isint);
    addParameter(parser, 'simOpts', struct(), @isstruct);
    parse(parser, opts);
    fdat.opts = parser.Results;


    cost = [];
    paramValues = [];

else
    assert(~isempty(fdat), 'First, setup the persistent data for the function.');

    variableParamValuesHead = varargin{1};

    ntps = fdat.opts.solNrTimepoints;
    maxT = fdat.expData.maxT;


    variableParamValuesHead = variableParamValuesHead(:);
    variableParamValuesHead(fdat.islog) = 10.^variableParamValuesHead(fdat.islog);
    % Note: fixed parameter values are always given in original scale
    paramValues = vertcat(variableParamValuesHead, fdat.fixedParamValuesTail);


    for i = numel(fdat.mexNames):-1:1
            tmax = maxT(i);
            if ntps == 0, tspan = tmax; else tspan = 0:(tmax/ntps):tmax; end
            nsols(i) = IQMPsimulate(fdat.mexNames{i}, tspan, [],...
                fdat.paramNames, paramValues, fdat.opts.simOpts);
    end


    cost = fdat.expData.cost(nsols, numel(variableParamValuesHead));


    if fdat.debugging
        fprintf('[DEBUG] evalModelDirect(%s) = %f.\n', mat2str(paramValues), cost);
    end

end

end % function
