function [ paramSpecs ] = loadParamSpecs( settingsOrFilename )
%LOADPARAMSPECS ... Read and process parameters which can be modified, and in
%   which range.
%
%   `readtable` wrapper
%
    %
    if ischar(settingsOrFilename)
        settings.paramSpecsFilename = settingsOrFilename;
    elseif ~isstruct(settingsOrFilename)
        error('LOADPARAMSPECS:IllegalArgument',...
            'Expecting parameters specification file name or settings structure.');
    else
        settings = settingsOrFilename;
    end

    if ~isfield(settings, 'paramSpecs') || isempty(settings.paramSpecs)

        if ~isfield(settings, 'paramSpecsFilename') || isempty(settings.paramSpecsFilename)
            error('LOADPARAMSPECS:MissingParamSpecs',...
                'Please specify `paramSpecsFilename` string.');
        else
            % table enforces fields homogenity and same columns size w/ '.COLNAME'
            % access as in struct
            paramSpecs = readtable(settings.paramSpecsFilename);
        end
    else
        paramSpecs = settings.paramSpecs;
    end

    %% Param specs table: validate and complement
    % check if columns exist with the `isfield` test for struct
    pSpecStruct = table2struct(paramSpecs);
    colnames = {'names', 'projections', 'bmin', 'bmax', 'islog'};
    for i=1:numel(colnames)
        if ~isfield(pSpecStruct, colnames{i})
            error('LOADPARAMSPECS:InvalidParamSpecs',...
                'Parameters specification has no `%s` column.', colnames{i});
        end
    end
    paramSpecs.islog = logical(paramSpecs.islog);
    % params specs have row per parameter
    d = size(paramSpecs, 1);
    % optional p0 column
    if ~isfield(pSpecStruct,'p0')
        paramSpecs.p0 = nan(d, 0);
    end
    % p0 explicitly provided in settings has precedence over one in parameters specification
    if isfield(settings, 'p0') && ~(numel(settings.p0) == 1 && isnan(settings.p0))
        % check size; expected points in rows or columns, so transpose if nr of columns
        % does not fit expected points dimensions but nr of rows does
        if size(settings.p0, 2) == d
            p0 = settings.p0';
        elseif size(settings.p0, 1) == d
            p0 = settings.p0;
        else
            error('LOADPARAMSPECS:InvalidSettingsP0',...
                [
                    'Initial parameters matrix "p0" has size %d by %d, whereas '...
                    'expecting n by %d or %d by n matrix, where n is nr of points '...
                    'and %d is nr of parameters according to theirs specification.'
                ],...
                size(settings.p0, 1), size(settings.p0, 2), d, d, d);
        end
        % filter-out points with NaN values
        p0 = p0(:, ~any(isnan(p0), 1));
        % override if anything left, otherwise warn and keep as specified
        if size(p0, 2) > 0
            paramSpecs.p0 = p0;
        else
            warning('LOADPARAMSPECS:InvalidSettingsP0',...
                'All provided initial points contain NaN values and cannot be used.')
        end
    end

end
