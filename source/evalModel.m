function [ varargout ] = evalModel( argin )
%EVALMODEL Evaluate model simulation cost with respect to experimental data, at
%   a given a parameter point, or set a persistent struct for further
%   evaluations of the function.
%
% Output: none in case of setting the persistent data; otherwise cost and
%         numerical simulations struct array (one for each sim experiment).
%
debugging = (debuglevel > 1);

    persistent persistentData

    if isobject(argin)
        persistentData = argin;
        if debugging % (debuglevel > 0)
            fprintf('[DEBUG] evalModel, set persistentData: parNames = (%s), possibleToFixSpecParamIdxs = %s, impossibleToFixSpecParamIdxs = %s.\n',...
                strjoin2(persistentData.paramNames,','),...
                mat2str(persistentData.possibleToFixSpecParamIdxs),...
                mat2str(persistentData.impossibleToFixSpecParamIdxs));
        end
        return;
    end
    if (isempty(persistentData))
        error('EVALMODEL:MissingPersistentData', 'Initialise first by calling `evalModel` with a persistent data structure single arg.');
    end

    localModifiableParamValues = argin;

    %% Prepare the simulation

    % parameter names, initial conditions, sim time
    paramNames = persistentData.paramNames;
    maxT = persistentData.expData.maxT;
    if (numel(maxT) ~= numel(persistentData.mexNames))
        error('EVALMODEL:InconsistentNumberOfExperiments', 'Simulation MEX expected for each measurement data experiment (%d vs. %d)',numel(persistentData.mexNames),numel(maxT));
    end


    %% parameter point: project values and transform from log-space
    nParam = numel(persistentData.modifiableModParamIdxs);
    paramValues = persistentData.paramDefaultValues;

    % iterate over i and l simultaneously
    l=0; % localModifiableParamValuesIdx
    for i = 1:nParam
        isLog = persistentData.inLogSpace(i); % isLog must be decalared here
        if persistentData.isFixedSpecParam(i)
            % set projection value
            parVal = persistentData.paramFixValues(i);
            assert(isfinite(parVal),'Ill-specified projection value "%f"',parVal);
            % don't map from log-space since projection values are never mapped
            % to the log-space in the first place (cf. logParamSpecs)
            isLog = false;
        else
            % set input value using current argin index; increment first
            l = l+1;
            parVal = localModifiableParamValues(l);
        end
        % map from log-space if necessary
        if isLog
            parVal = 10^parVal;
        end
        % save value at param spec index i
        paramValues(i) = parVal;
    end
    assert(l == numel(localModifiableParamValues), 'l=%d, numel(argin)=%d', l, numel(argin));
    assert(numel(paramNames)==numel(paramValues));


    %% Simulate model per experiment
    % integrator options
    options = persistentData.odeIntegratorOptions;
    if ~isfield(options, 'maxnumsteps') || isempty(options.maxnumsteps)
        options.maxnumsteps = 1e5; % IQMPsimulate default value
    end
    nmaxtry = options.maxnumstepsTry;
    ntps = options.solNrTimepoints;

    % integrate
    ntry = 0;
    success = false;
    while ~success && (ntry < nmaxtry);
        ntry=ntry+1;
        try
            % start with the last index for an implicit pre-allocation of
            % the nsols variable

            for i = numel(persistentData.mexNames):-1:1
                tmax = maxT(i);
                if ntps == 0, tspan = tmax; else tspan = 0:(tmax/ntps):tmax; end
                nsols(i) = IQMPsimulate(persistentData.mexNames{i}, tspan,...
                    [], paramNames, paramValues, options);
            end
            success = true;
        catch ME
            if ntry >= nmaxtry,
                warning('evalModel:IntegrationFailure', 'ODE integration failed with the following error:\n\t%s: %s\n', ME.identifier, ME.message);
            else
                options.maxnumsteps = 10*options.maxnumsteps;
                fprintf('[WARN] evalModel, ODE integration failed; increasing max. number of steps between output points of the integrator.\n');
            end
        end
    end
    % compute the simulation cost wrt the data

    if success
        %assert(exist('nsols','var') == 1);
        cost = persistentData.expData.cost(nsols, numel(localModifiableParamValues));
    else
        cost = Inf;
        nsols = struct([]);
    end
    if debugging
        fprintf('[DEBUG] evalModel(%s) = %f.\n', mat2str(paramValues), cost);
    end


    varargout{1} = cost;
    if nargout > 1
        varargout{2} = nsols;
    end

end

