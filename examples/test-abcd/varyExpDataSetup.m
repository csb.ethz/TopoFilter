addpath(genpath('../../source'))

varyTps = false; % else vary stds
fname = fullfile('../../results/varyStd');

%% Beginning of program
rng('shuffle');

c = clock();
fprintf('Program started at %d:%d\n',c(4),c(5));
tic;


modelFilename = './model.txt';
paramSpecsFilename = './paramSpecs.txt';

% Specify parameters for exploration
nmont = 5e1; %maximum number integrations in Metropolis Monte Carlo
nelip = 1e2; %maximum number integrations in ellipsoids

% Specify whether backtracking and/or recursive search should be performed
backtrack = true;
recursive = true;

% Specify number of runs
numRuns = 10;

if varyTps
	vals = [10 20 30];
else
	vals = [0.05 0.1 0.15];
end

defaultTps = 20;
defaultStd = 0.1;

counts = zeros(2^7, length(vals));
thresholds = zeros(1, length(vals));

for expIdx = 1:length(vals)

	if varyTps
		expData = getExpData(vals(expIdx), defaultStd);
	else
		expData = getExpData(defaultTps, vals(expIdx));
	end

	startFile;

    thresholds(expIdx) = threshold;

	for i = 1:size(viableProjectionsCollection,1)
	    decProjection = bin2dec(num2str(viableProjectionsCollection(i,:)));
	    counts(decProjection, expIdx) = counts(decProjection, expIdx)+1;
	end

	save(fname);
end



%% Cleanup
delete('./functionfinal.m', 'mexModel.mexa64');
c = clock();
fprintf('Program finished at %d:%d\n',c(4),c(5));
toc;
