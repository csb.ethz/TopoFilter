function [bool] = isint(x)
%ISINT Test if `x` is an integer, including `Inf`.
%
%  Cf. built-in function isinteger which checks the array type:
%      isinteger(3)
%      isinteger(uint64(3))
%      isinteger([uint64(3) uint64(2)]) % non-vectorised result
%      isint(3)
%      isint([3 2])
%      isint({3 2}) % false
%
    bool = isreal(x);
    if bool
        bool = ((isfinite(x) & ( x == floor(x) )) | isinf(x));
    end
end
