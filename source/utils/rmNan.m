function [C, ia] = rmNan(A, dim)
% RMNAN Remove columns (rows) of matrix A, containing at least one NaN value.
%
% Inputs:
%     A (numeric): matrix to filter
%    dim (posint): dimension to filter against NaNs; by default 2 (columns)
%
% Outpus:
%    C (numeric): matrix w/o any NaN columns (rows)
%    ia (posint): indices vector of matrix A for remaining columns (rows)
%
    if nargin < 2
        dim = 2;
    else
        if (~isscalar(dim) || ~isposint(dim) || dim > ndims(A))
            error('RMNAN:InvalidArgument', 'Invalid input argument: dim.')
        end
    end
    ia = ~anyNanAtD(A,dim);
    idxCa = repmat({':'}, 1, ndims(A));
    idxCa{dim} = ia;
    C = A(idxCa{:});
end

function iad = anyNanAtD(A, d)
    dv = 1:ndims(A);
    dv(d) = [];
    if isempty(dv)
        iad = [];
    else
        iad = any(isnan(A),dv(1));
        for i=2:numel(dv)
            iad = any(iad,dv(i));
        end
        iad = squeeze(iad);
    end
end
