function [simdata, tps] = decoder_extractSimData(nsol, obs, varargin)

    simd = getIQMsimulateValues(nsol, obs);
    simt = nsol.time;

    simdata = nan(1, 3);
    tps = nan(1, 3);

    [simdata(1), itp1] = max(simd);
    tps(1) = simt(itp1);
    tps(2) = 400; % t0
    tps(3) = tps(2) + 360; % t0+6h
    simdata(2) = simd(simt == tps(2));
    simdata(3) = simd(simt == tps(3));

end
