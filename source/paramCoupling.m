function C = paramCoupling()
%PARAMCOUPLING Return a two column cell array C, where each row specifies
% coupling of projections of parameters as follows: if parameters with indices
% C{i,1} are projected then project also parameters with indices C{i,2}.
%
% This is a default implemenation, that defines no coupling at all. Specify
% parameters coupling via paramCouplingFcn option of the experiment. Please note
% that you can specify also paramCouplingApplyFcn to define diractly a dynamic
% coupling (i.e. copuling dependant on the current projection). Doing such
% overrides paramCouplingFcn.
%
% NOTE: order of coupling rules matters because coupling rules are applied only
%       once per projection, in order they are defined, cumulatively; e.g.
%       C = { 1, 2; 2, 3 } is equivalent to C = { 1, [2 3]; 2, 3 }, but
%       C = { 2, 3; 1, 2 } is not. If in doubt, always put rules with an index
%       on RHS before rules in which this index appears on the LHS.
%
% Example 1: Symmetric coupling
% Always project together params in rows 1 and 2 in params specification:
%     C = { 1, 2; 2, 1 };
%
% Example 2: Assymetric coupling
% When param in row 2 in params specification (think: v_max) is projected
% then project also param in row 1 in params specification (think: Km),
% but not the other way around:
%     C = { 2, 1 };
%
% Example 3: Symmetric multi-parameter coupling
% If 1 (Vmax) is projected than also project 2, 3, 4 (Km1, Km2, Km3), and vice
% versa, if all Kmi are projected (think: to infinity, s.t. Xi/Kmi = 0 in the
% regulatory part of reaction rate), then project also Vmax.
%     C = { 1, [2 3 4]; [2 3 4], 1 };
%

    % no coupling
    C = {};

end
