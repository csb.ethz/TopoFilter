classdef MaxProjectionSet < ProjectionSet
%MAXPROJECTIONSSET Set of maximal projections.
%
% Same as ProjectionSet, but has check only for maximal projections. By adding
% a projeciton larger than a projection already in the set, the set may contain
% also non-maximal projections. However, the set can always be reduced to
% maximal projections only.
%
    methods(Static)
        function [Crowmat, IArowmat, ICrowmat] = punique(Arowmat, varargin)
        %PUNIQUE For the logical array Arowmat returns the same rows as in
        % Arowmat but with maximal vectors only.
        %
        % To preserve order of rows in Crowmat give 'stable' as an
        % additional input argument (by default 'sorted'; cf. unique).
        %
        % Attention: ICrowmat is only such that row-wise
        %     Crowmat(ICrowmat,:) >= Arowmat
        % (not '=' as in unique).
        %
            if isempty(varargin) || ~strcmp('stable',varargin{1})
                stable = false;
            else
                stable = true;
            end
            n = size(Arowmat,1);
            isMax = true(n,1);
            Idup = (1:n)'; % Arowmat(i) <= Arowmat(Idup(i))
            for i=1:n
                pI = Arowmat(i,:);
                for j=(i+1):n
                    if isMax(j) % skip non-maximal (<= is transitive)
                        pJ = Arowmat(j,:);
                        % for 'stable' output, first check j<=i, then i<=j
                        % (i.e. i<j, so in case i==j, i and not j is saved)
                        if ProjectionSet.pleq(pJ,pI)
                            % j-th projection is not greater than i-th
                            isMax(j) = false;
                            Idup(Idup == j) = i;
                        elseif ProjectionSet.pleq(pI,pJ)
                            % i-th projection is not greater than j-th
                            isMax(i) = false;
                            Idup(Idup == i) = j;
                            % skip to i+1-th projection
                            break
                        end
                    end
                end
            end % Idup(i) == j iff Arowmat(i) <= Arowmat(j) and isMax(j)

            % stable
            Crowmat = Arowmat(isMax,:);
            IArowmat = find(isMax);
            IAtoIC = cumsum(isMax); % shift indices of duplicates in Arowmat to Crowmat
            %IAtoIC(~isMax) = NaN % doesn't matter
            ICrowmat = IAtoIC(Idup);
            %alt, double loop one-liner: ICrowmat = arrayfun(@(x)find(IArowmat==x,1),Idup)

            % sorted (default)
            if ~stable
                [Crowmat, Isorted] = sortrows(Crowmat);
                IArowmat = IArowmat(Isorted);
                Isortedinv(Isorted) = (1:numel(Isorted)); % row vec
                ICrowmat = Isortedinv(ICrowmat)'; % force col vec
            end
        end
    end
    methods(Static, Access = protected)
        function obj = p_new(varargin)
            obj = MaxProjectionSet(varargin{:});
        end
    end
    methods (Access = protected)
        function bool = p_hasAt(self, i, pCol)
            bool = ProjectionSet.pleq(pCol, self.at(i));
        end
    end
    methods (Access = public)
        function self = MaxProjectionSet(varargin)
        %MAXPROJECTIONSSET Initialise MaxProjectionSet instance: a) empty (see
        % ProjectionSet for arguments), or b) reduced from a ProjectionSet
        % instance.
        %
        %
            self = self@ProjectionSet(varargin{:});
        end
        function [reduced, Imax] = reduce(self)
        %REDUCE Return a copy of the projections set reduced to maximal
        % projections.
        %
            % Note: same implementation works in the ProjectionSet class, but
            %       the method doesn't make sense there since ther it is a set
            %       w/ equality comparison, whereas here it's inequality
            %       (cf. p_hasAt), and after addition two or more projections
            %       may be in the has() relation
            [~, Imax] = self.punique(self.rowmat);
            reduced = self.reduceTo(Imax);
        end
        function [answer, idx] = has(self, projection)
        %HAS Check if greater or equal projection is already in the set.
        % Returns logic answer and also index of the first greater or equal
        % projection found if true, otherwise 0.
        %
            % Uses overloaded p_hasAt
            [answer, idx] = has@ProjectionSet(self, projection);
        end
        function added = add(self, projection, varargin)
        %ADD Add projection if a equal or greater is not already an element
        % of the set.
        %
        % Beware: after the addition of a projection, the set may contain
        %         non-maximal projections, smaller than the added
        %         projection; if required, use reduce() afterwards.
        %
            % Uses overloaded p_hasAt
            added = add@ProjectionSet(self, projection, varargin{:});
        end
        function [IPadded, IPpresent, IIPaddedDuplicates, IselfPresent] = addAll(self, P)
        %ADDALL Add all projections from P, that are not smaller or equal
        % than any projection in the set.
        %
        % See addAll@ProjectionSet for in/out args description.
        % Note: duplicates here are defined by inequality, not equality.
        %
        % Beware: analogously to add, the set may contain non-maximal
        %         projections after application; if required, use reduce().
        %
            % Uses overloaded (static) punique
            [IPadded, IPpresent, IIPaddedDuplicates, IselfPresent] = addAll@ProjectionSet(self, P);
        end
    end
end
