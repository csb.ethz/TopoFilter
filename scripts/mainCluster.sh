#!/bin/bash

#nWorkers=$1;
#if [ -z "$nWorkers" ]; then
#    echo "Missing argument: number of parallel workers per matlab job."
#    exit 1
#fi
enumLevel=0; # 1, 2
exhaustiveRank=1; # 1, 2

nWorkersArray=(4 1); # 64 16 4 1
nfevalExpArray=(3 2); # 4 3 2

nRuns=1; # 1, ..., 10 (per each repeat)
nRepeats=1; # 1, ..., 10 (per each nWorkers and nfevalExp setup)

runInParallel=false; # true, false (if each nWorkers and nfevalExp matlab call in parallel)
runLag=90; # 90, 120 (seconds between each parallel matlab call)

totRuns=$(echo "${nRuns}*${nRepeats}" | bc); # (per each workers and nfevalExp setup)
nRounds=$(echo "${#nfevalExpArray[@]}*${nRepeats}" | bc);
nCalls=$(echo "${#nWorkersArray[@]}*${nRounds}" | bc);
echo -n "Running ${nRepeats}x${nRuns} runs for each (nWorkers,nfevalExp) pair in ${nCalls} "
if [ "${runInParallel}" = true ]; then
	sumWorkers=$(echo ${nWorkersArray[@]} | tr ' ' '+' | bc);
	totWorkers=$(echo "${sumWorkers}*${nRounds}" | bc);
	echo "parallel matlab calls (total ${totWorkers} workers)."
else
	nSeq=$(echo "${nRepeats}*${#nWorkersArray[@]}*${#nfevalExpArray[@]}" | bc);
	echo "sequential matlab calls (${nRounds} rounds with $(echo ${nWorkersArray[@]} | tr ' ' ',') workers, subsequently)."
fi

for nWorkers in ${nWorkersArray[@]}; do
for nfevalExp in ${nfevalExpArray[@]}; do
for repeat in $(seq 1 ${nRepeats}); do
matlabCmd="matlab -nosplash -nodisplay";
matlabStr="mainCluster(${nRuns},${nfevalExp},${nWorkers},${enumLevel},${exhaustiveRank});exit";
if [ "${runInParallel}" = true ]; then
	echo ${matlabCmd} -r \"${matlabStr}\" \&
	${matlabCmd} -r "${matlabStr}" &
	sleep ${runLag}
else
echo ${matlabCmd} -r \"${matlabStr}\"
	${matlabCmd} -r "${matlabStr}"
fi
done
done
done

wait # sync point (in case of parallel runs
