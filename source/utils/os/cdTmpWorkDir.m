function [ tmpWorkingDir, prevWorkingDir ] = cdTmpWorkDir()
%CDTMPWORKDIR add current working directory to a MATLAB path and cd into
%   a temporary working directory.

    prevWorkingDir = pwd;
    addpath(genpath(prevWorkingDir))
    tmpWorkingDir = tempname;
    mkdir(tmpWorkingDir)
    cd(tmpWorkingDir)

end

