function ret = mapAllCell(cellArray, fHandlerOrName, varargin)
%MAPCELL Recurisvely map all non-cell array elements of a cell-array.
%
%  mapCell(cellArray, fHandlerOrName, ... function args)
%
    if ischar(fHandlerOrName)
        fHandlerOrName = str2func(fHandlerOrName);
    end
    ret = p_mapAllCell(cellArray, fHandlerOrName, varargin);
end

function ret = p_mapAllCell(ca, fun, varargin)
    ret = cell(size(ca));
    for i=1:numel(ca)
        ce = ca{i};
        if(~iscell(ce))
            ret{i} = fun(ce, varargin{:});
        else
            ret{i} = p_mapAllCell(ce, fun, varargin{:});
        end
    end
end
