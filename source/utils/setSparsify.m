function [C, ia] = setSparsify(A, n, varargin)
%SETSPARSIFY Sparsify set of rows of A, by selecting n rows (all if A has fewer
% rows). Rows are chosen from n computed clusters randomly or, as points closest
% to any point from a set K, if given. Indices of the selected rows are returned
% in column vector ia.
%
% Inputs:
%      A (numeric): matrix to sparsify rows of which
%      n (int): number of rows to select
%
%      varargin
%          K (numeric): matrix of points (in rows) for selection of closest
%                       points in clusters from A; all dimensions (columns)
%                       which contain a NaN value are ignored in the distance
%                       computation.
%
% Examples:
%      A = rand(10,3)
%      % 3 points closest to any of the all 0 or all 1 corners of the hypercube
%      [C, ia] = setSparsify(A, 3, [0 0 0; 1 1 1])
%      [C, ia] = setSparsify(A, 3, [NaN 0 0])
%
    d = size(A,2);
    if nargin < 3
        K = [];
    else
        K = varargin{1};
        if ~isnumeric(K) || size(K,2) ~= d
            error('SETSPARSIFY:InvalidArgument','Invalid input argument: K.');
        end
    end
    n0 = size(A,1);
    if n == 0
        ia = [];
    elseif n0 <= n
        ia = (1:n0)';
    else
        Ia = kmeans(A, n, 'EmptyAction','singleton');
        if isempty(K)
            [~, ia] = randrows(A, n);
        else
            [L, ik] = rmNan(K, 2);
            [~, da] = dsearchn(L, A(:,ik));
            ia = arrayfun(@(i) imingroup(da, Ia, i), (1:n)');
        end
    end
    C = A(ia,:);
end

function imin = imingroup(x, I, i)
    Ig = (I == i);
    [~, igmin] = min(x(Ig));
    Icut = find(Ig, igmin, 'first');
    imin = Icut(end);
end
