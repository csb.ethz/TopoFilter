function [ exit_status, results_fn, elapsed_time ] = torExperiment( varargin )

    assert(exist('TFmain') == 2, 'Couldn''t find `TFmain` function. Be sure to add TopoFilter source code folder to the path.');

    timestamp = @() datestr(now, 'yyyy.mm.dd HH:MM');

    %% Initialise
	rng('shuffle');

    fprintf('Program started at %s\n',timestamp());
	tic;


    %% Tunable experiments setting if not provided via function options (+ validation)
    parser = inputParser;
    parser.FunctionName = 'TOREXPERIMENT: ';

    % Results file
    addParameter(parser, 'outputFilename', fullfile('./results',...
        sprintf('%s.mat',datestr(now, 'yymmdd_HHMMSS'))), @ischar);
    % Number of runs
    addParameter(parser, 'numRuns', 2, @isposint);
    addParameter(parser, 'dryRun', false, @islogical); % calc only threshold (and p0, if not given)

    % Sample sizes: not directly options itself, but used below for setting
    % the actual nmont, nelip and nvmin options.
    % Maximum number of cost function evauations in one sampling (roughly).
    addParameter(parser, 'nfeval', 1e2, @isposint);
    % Ratio of nfeval's going respectively into nmont and nelip
    addParameter(parser, 'rfeval', [4 5], @(x) numel(x)==2 && all(isposint(x)));
    % Minimal size of viable points sample as a proportion of nfeval
    addParameter(parser, 'pvmin', 0.1, @isprob);
    % Maximal size of viable points sample as a proportion of nfeval
    addParameter(parser, 'pvmax', 1.9, @(x) isnumeric(x) && (x > 0));

    % Flag for saving viable points after each sampling
    addParameter(parser, 'saveViablePoints', false, @islogical);
    % Flag for removing the MEX files after the runs
    % Set to `false` if you're independently running `TFmain` using the same
    % model file (e.g. when aditionally externally parallelising experiments)
    addParameter(parser, 'cleanupMexFiles', false, @islogical);

    % Flags to control flow of the model space search
    % * Recursive search
    addParameter(parser, 'recursive', true, @islogical);
    % * Enumeration level for the viable projections that allows to switch the
    %   heuristic between goals of: a) finding a maximal reduction (0), and
    %   b) finding as many reductions as possible (2)
    addParameter(parser, 'enumLevel', 1, isinrange(2));
    % * Number of (couples of) parameters to combine at most at once during the
    %   initial exhaustive search for the viable projections (`1` = singletons,
    %   `2` = singletons and pairs etc.)
    addParameter(parser, 'exhaustiveRank', 1, @isposint);

    % Parallel computation level
    addParameter(parser, 'parallelize', 2, isinrange(3));
    % Param space exploration specification
    % Some params are rm based on (arbitrary) value similarity criteria
    % Inital concentration are currently ignored
    addParameter(parser, 'paramSpecsFilename', './paramSpecs_02.txt', @ischar);

    % Model file
    addParameter(parser, 'modelFilename', './model_02-observables.txt', @ischar);

    % Init param value
    addParameter(parser, 'p0', NaN, @isnumeric); % NaN = from the param spec or model
    % Parse the input arguments
    parse(parser, varargin{:});
    p = parser.Results;


    %% Experiment and data files
    p.expFilename = {'./exp00-SteadyState.exp', './exp01-Rapa500.exp', './exp02-Rapa109.exp'};
    p.expDataFilename = './expData.xls';

    %% Sample sizes
    % Split nfeval according to given proportions
    nfevalv = splitlrm(p.nfeval,  p.rfeval/sum(p.rfeval));
    % Maximum target sample size in the Markov chain Monte Carlo (MCMC) sampling
    p.nmont = nfevalv(1);
    % Guiding number of model evaluations in the Ellipsoids-based sampling
    p.nelip = nfevalv(2);
    % Minimum nr of viable parameter samples before (re-)sampling
    p.nvmin = max(1, round(p.pvmin*sum(nfevalv)));
    % Maximum nr of viable parameter samples before sparsifying
    p.nvmax = ceil(p.pvmax*sum(nfevalv));

    %% Run the actual program
    [ runs, viabilityThreshold, settings ] = TFmain(p);


	%% Save results
    results_fn = settings.outputFilename;
    save(results_fn, 'runs', 'viabilityThreshold', 'settings');


    %% Cleanup
    elapsed_time = toc;
    fprintf('Program finished at %s\n',timestamp());

    exit_status = 0;
end
