********** MODEL NAME
tor_nbt_2006_orig_01

********** MODEL NOTES


********** MODEL STATES
d/dt(Tap42p) = (-RXN1_r_irr_1_f+RXN1_r_irr_2_b+RXN3_r_irr_8_f+RXN6_r_irr_15_f-RXN10_r_irr_27_f+RXN10_r_irr_28_b)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tor12p) = (-RXN1_r_irr_1_f+RXN1_r_irr_2_b+RXN1_r_irr_3_f-RXN8_r_irr_18_f+RXN8_r_irr_19_b+RXN8_r_irr_20_f-RXN12_r_irr_31_f+RXN12_r_irr_32_b-RXN13_r_irr_33_f+RXN13_r_irr_34_b+RXN13_r_irr_35_f)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tap42pTor12p) = (+RXN1_r_irr_1_f-RXN1_r_irr_2_b-RXN1_r_irr_3_f)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tap42pp) = (+RXN1_r_irr_3_f-RXN2_r_irr_4_f-RXN3_r_irr_6_f+RXN3_r_irr_7_b-RXN5_r_irr_11_f+RXN5_r_irr_12_b-RXN6_r_irr_13_f+RXN6_r_irr_14_b+RXN16_r_irr_43_b-RXN18_r_irr_46_f+RXN18_r_irr_47_b-RXN19_r_irr_48_f+RXN19_r_irr_49_b-RXN20_r_irr_50_f+RXN20_r_irr_51_b)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Pph2122p) = (-RXN2_r_irr_4_f-RXN4_r_irr_9_f+RXN4_r_irr_10_b+RXN16_r_irr_43_b)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tap42ppPph2122p) = (+RXN2_r_irr_4_f-RXN16_r_irr_43_b-RXN17_r_irr_44_f+RXN17_r_irr_45_b-RXN18_r_irr_46_f+RXN18_r_irr_47_b)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(PP2A1) = (-RXN3_r_irr_6_f+RXN3_r_irr_7_b+RXN3_r_irr_8_f+RXN4_r_irr_9_f-RXN4_r_irr_10_b-RXN8_r_irr_21_f+RXN8_r_irr_22_b+RXN8_r_irr_23_f-RXN14_r_irr_36_f+RXN14_r_irr_37_b+RXN14_r_irr_38_f)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tap42ppPP2A1) = (+RXN3_r_irr_6_f-RXN3_r_irr_7_b-RXN3_r_irr_8_f)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Cdc55pTpd3p) = (-RXN4_r_irr_9_f+RXN4_r_irr_10_b)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(PP2A2) = (-RXN6_r_irr_13_f+RXN6_r_irr_14_b+RXN6_r_irr_15_f+RXN7_r_irr_16_f-RXN7_r_irr_17_b-RXN9_r_irr_24_f+RXN9_r_irr_25_b+RXN9_r_irr_26_f-RXN15_r_irr_39_f+RXN15_r_irr_40_b+RXN15_r_irr_41_f)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tap42ppPP2A2) = (+RXN6_r_irr_13_f-RXN6_r_irr_14_b-RXN6_r_irr_15_f)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Sit4p) = (-RXN5_r_irr_11_f+RXN5_r_irr_12_b-RXN7_r_irr_16_f+RXN7_r_irr_17_b)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Sap) = (-RXN7_r_irr_16_f+RXN7_r_irr_17_b)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tip41p) = (-RXN8_r_irr_18_f+RXN8_r_irr_19_b+RXN8_r_irr_23_f+RXN9_r_irr_26_f-RXN10_r_irr_27_f+RXN10_r_irr_28_b-RXN19_r_irr_48_f+RXN19_r_irr_49_b)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tip41pp) = (+RXN8_r_irr_20_f-RXN8_r_irr_21_f+RXN8_r_irr_22_b-RXN9_r_irr_24_f+RXN9_r_irr_25_b-RXN13_r_irr_33_f+RXN13_r_irr_34_b+RXN14_r_irr_38_f+RXN15_r_irr_41_f-RXN17_r_irr_44_f+RXN17_r_irr_45_b-RXN20_r_irr_50_f+RXN20_r_irr_51_b)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tip41pTor12p) = (+RXN8_r_irr_18_f-RXN8_r_irr_19_b-RXN8_r_irr_20_f)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tip41ppPP2A1) = (+RXN8_r_irr_21_f-RXN8_r_irr_22_b-RXN8_r_irr_23_f)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tip41ppPP2A2) = (+RXN9_r_irr_24_f-RXN9_r_irr_25_b-RXN9_r_irr_26_f)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tip41pTap42p) = (+RXN10_r_irr_27_f-RXN10_r_irr_28_b)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tap42ppSit4p) = (+RXN5_r_irr_11_f-RXN5_r_irr_12_b)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Fpr1p) = (-RXN11_r_irr_29_f+RXN11_r_irr_30_b)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Rapamycin) = (-RXN11_r_irr_29_f+RXN11_r_irr_30_b)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Fpr1pRapamycin) = (+RXN11_r_irr_29_f-RXN11_r_irr_30_b-RXN12_r_irr_31_f+RXN12_r_irr_32_b)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tor12pFpr1pRapamycin) = (+RXN12_r_irr_31_f-RXN12_r_irr_32_b)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tip41ppp) = (+RXN13_r_irr_35_f-RXN14_r_irr_36_f+RXN14_r_irr_37_b-RXN15_r_irr_39_f+RXN15_r_irr_40_b)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tip41ppTor12p) = (+RXN13_r_irr_33_f-RXN13_r_irr_34_b-RXN13_r_irr_35_f)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tip41pppPP2A1) = (+RXN14_r_irr_36_f-RXN14_r_irr_37_b-RXN14_r_irr_38_f)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tip41pppPP2A2) = (+RXN15_r_irr_39_f-RXN15_r_irr_40_b-RXN15_r_irr_41_f)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tip41ppTap42ppPph2122p) = (+RXN17_r_irr_44_f-RXN17_r_irr_45_b)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tap42ppTap42ppPph2122p) = (+RXN18_r_irr_46_f-RXN18_r_irr_47_b)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tip41pTap42pp) = (+RXN19_r_irr_48_f-RXN19_r_irr_49_b)/Cytosol {isSpecie:Cytosol:concentration}
d/dt(Tip41ppTap42pp) = (+RXN20_r_irr_50_f-RXN20_r_irr_51_b)/Cytosol {isSpecie:Cytosol:concentration}

Tap42p(0) = 89.13599999999999568
Tor12p(0) = 69.920000000000001705
Tap42pTor12p(0) = 0
Tap42pp(0) = 0
Pph2122p(0) = 318.88999999999998636
Tap42ppPph2122p(0) = 0
PP2A1(0) = 0
Tap42ppPP2A1(0) = 0
Cdc55pTpd3p(0) = 469.73000000000001819
PP2A2(0) = 0
Tap42ppPP2A2(0) = 0
Sit4p(0) = 216.84999999999999432
Sap(0) = 611.77999999999997272
Tip41p(0) = 187.13999999999998636
Tip41pp(0) = 0
Tip41pTor12p(0) = 0
Tip41ppPP2A1(0) = 0
Tip41ppPP2A2(0) = 0
Tip41pTap42p(0) = 0
Tap42ppSit4p(0) = 0
Fpr1p(0) = 1419.0999999999999091
Rapamycin(0) = 0
Fpr1pRapamycin(0) = 0
Tor12pFpr1pRapamycin(0) = 0
Tip41ppp(0) = 0
Tip41ppTor12p(0) = 0
Tip41pppPP2A1(0) = 0
Tip41pppPP2A2(0) = 0
Tip41ppTap42ppPph2122p(0) = 0
Tap42ppTap42ppPph2122p(0) = 0
Tip41pTap42pp(0) = 0
Tip41ppTap42pp(0) = 0

********** MODEL PARAMETERS
k1 = 971.515399 {isParameter}
k2 = 72.711159 {isParameter}
k3 = 62.872053 {isParameter}
k4 = 265.174972 {isParameter}
k5 = 759.461785 {isParameter}
k6 = 3.510951 {isParameter}
k7 = 83.891406 {isParameter}
k8 = 639.052570 {isParameter}
k9 = 649.306857 {isParameter}
k10 = 989.766692 {isParameter}
k11 = 990.606141 {isParameter}
k12 = 982.152141 {isParameter}
k13 = 1.605453 {isParameter}
k14 = 1.842562 {isParameter}
k15 = 964.554396 {isParameter}
k16 = 324.432033 {isParameter}
k17 = 994.969095 {isParameter}
k18 = 0.010550 {isParameter}
k19 = 986.138359 {isParameter}
k20 = 13.644817 {isParameter}
k21 = 476.107981 {isParameter}
k22 = 0.000100 {isParameter}
k23 = 995.975445 {isParameter}
k24 = 666.011534 {isParameter}
k25 = 985.443032 {isParameter}
k26 = 0.010086 {isParameter}
k27 = 591.670684 {isParameter}
k28 = 45.483159 {isParameter}
k29 = 979.776564 {isParameter}
k30 = 65.651731 {isParameter}
k31 = 813.778341 {isParameter}
k32 = 7.135140 {isParameter}
k33 = 0.021387 {isParameter}
k35 = 435.210928 {isParameter}
k36 = 982.447965 {isParameter}
k37 = 405.098556 {isParameter}
k38 = 67.460390 {isParameter}
k39 = 977.940319 {isParameter}
k40 = 4.381082 {isParameter}
k41 = 1000.000000 {isParameter}
k42 = 982.008247 {isParameter}
k43 = 229.113426 {isParameter}
Env = 1 {isCompartment:}
Cytosol = 1 {isCompartment:Env}
totTap42ppAt20 = 0 {isParameter}
totTip41pTap42pAt20 = 0 {isParameter}
totTap42pSit4pAt20 = 0 {isParameter}
totSit4pAt20 = 0 {isParameter}
totSapAt20 = 0 {isParameter}


********** MODEL VARIABLES
TotTap42pp = (Tap42pp) + (Tap42ppPph2122p) + (Tap42ppPP2A1) + (Tap42ppPP2A2) + (Tap42ppSit4p) + (Tip41ppTap42ppPph2122p) + 2*(Tap42ppTap42ppPph2122p) + (Tip41pTap42pp) + (Tip41ppTap42pp)
RelTap42pp = piecewiseIQM(1,le(time,20), TotTap42pp/totTap42ppAt20)
TotTip41pTap42p = (Tip41pTap42p) + (Tip41ppTap42ppPph2122p) + (Tip41pTap42pp) + (Tip41ppTap42pp)
RelTip41pTap42p = piecewiseIQM(1,le(time,20), TotTip41pTap42p/totTip41pTap42pAt20)
TotTap42pSit4p = (Tap42ppSit4p)
RelTap42pSit4p = piecewiseIQM(1,le(time,20), TotTap42pSit4p/totTap42pSit4pAt20)
TotTor12p = (Tor12p) + (Tap42pTor12p) + (Tip41pTor12p) + (Tor12pFpr1pRapamycin) + (Tip41ppTor12p)
TotTap42p = (TotTap42pp) + (Tap42p) + (Tap42pTor12p) + (Tip41pTap42p)
TotPP2A1 = (PP2A1) + (Tap42ppPP2A1) + (Tip41ppPP2A1) + (Tip41pppPP2A1)
TotPph2122p = (TotPP2A1) + (Pph2122p) + (Tap42ppPph2122p) + (Tip41ppTap42ppPph2122p) + (Tap42ppTap42ppPph2122p)
TotCdc55pTpd3p = (TotPP2A1) + (Cdc55pTpd3p)
TotTip41p = (Tip41p) + (Tip41pp) + (Tip41pTor12p) + (Tip41ppPP2A1) + (Tip41ppPP2A2) + (Tip41pTap42p) + (Tip41ppp) + (Tip41ppTor12p) + (Tip41pppPP2A1) + (Tip41pppPP2A2) + (Tip41ppTap42ppPph2122p) + (Tip41pTap42pp) + (Tip41ppTap42pp)
TotFpr1p = (Fpr1p) + (Fpr1pRapamycin) + (Tor12pFpr1pRapamycin)
TotPP2A2 = (PP2A2) + (Tap42ppPP2A2) + (Tip41ppPP2A2) + (Tip41pppPP2A2)
TotSit4p = (TotPP2A2) + (Sit4p) + (Tap42ppSit4p)
TotSap = (TotPP2A2) + (Sap)
RelPP2A2ToSit4p = piecewiseIQM(1,le(time,20), TotPP2A2/totSit4pAt20)
RelPP2A2ToSap = piecewiseIQM(1,le(time,20), TotPP2A2/totSapAt20)


********** MODEL REACTIONS
RXN1_r_irr_1_f = k1 * Tap42p * Tor12p
RXN1_r_irr_2_b = k2 * Tap42pTor12p
RXN1_r_irr_3_f = k3 * Tap42pTor12p
RXN2_r_irr_4_f = k4 * Tap42pp * Pph2122p
RXN3_r_irr_6_f = k6 * Tap42pp * PP2A1
RXN3_r_irr_7_b = k7 * Tap42ppPP2A1
RXN3_r_irr_8_f = k8 * Tap42ppPP2A1
RXN4_r_irr_9_f = k9 * Cdc55pTpd3p * Pph2122p
RXN4_r_irr_10_b = k10 * PP2A1
RXN5_r_irr_11_f = k11 * Tap42pp * Sit4p
RXN5_r_irr_12_b = k5 * Tap42ppSit4p
RXN6_r_irr_13_f = k12 * Tap42pp * PP2A2
RXN6_r_irr_14_b = k7 * Tap42ppPP2A2
RXN6_r_irr_15_f = k8 * Tap42ppPP2A2
RXN7_r_irr_16_f = k13 * Sit4p * Sap
RXN7_r_irr_17_b = k10 * PP2A2
RXN8_r_irr_18_f = k14 * Tip41p * Tor12p
RXN8_r_irr_19_b = k15 * Tip41pTor12p
RXN8_r_irr_20_f = k3 * Tip41pTor12p
RXN8_r_irr_21_f = k16 * Tip41pp * PP2A1
RXN8_r_irr_22_b = k17 * Tip41ppPP2A1
RXN8_r_irr_23_f = k8 * Tip41ppPP2A1
RXN9_r_irr_24_f = k18 * Tip41pp * PP2A2
RXN9_r_irr_25_b = k19 * Tip41ppPP2A2
RXN9_r_irr_26_f = k8 * Tip41ppPP2A2
RXN10_r_irr_27_f = k20 * Tip41p * Tap42p
RXN10_r_irr_28_b = k21 * Tip41pTap42p
RXN11_r_irr_29_f = k22 * Fpr1p * Rapamycin
RXN11_r_irr_30_b = 5 * k22 * Fpr1pRapamycin
RXN12_r_irr_31_f = k23 * Tor12p * Fpr1pRapamycin
RXN12_r_irr_32_b = k24 * Tor12pFpr1pRapamycin
RXN13_r_irr_33_f = k25 * Tip41pp * Tor12p
RXN13_r_irr_34_b = k26 * Tip41ppTor12p
RXN13_r_irr_35_f = k27 * Tip41ppTor12p
RXN14_r_irr_36_f = k28 * Tip41ppp * PP2A1
RXN14_r_irr_37_b = k29 * Tip41pppPP2A1
RXN14_r_irr_38_f = k30 * Tip41pppPP2A1
RXN15_r_irr_39_f = k31 * Tip41ppp * PP2A2
RXN15_r_irr_40_b = k32 * Tip41pppPP2A2
RXN15_r_irr_41_f = k33 * Tip41pppPP2A2
RXN16_r_irr_43_b = k35 * Tap42ppPph2122p
RXN17_r_irr_44_f = k36 * Tip41pp * Tap42ppPph2122p
RXN17_r_irr_45_b = k37 * Tip41ppTap42ppPph2122p
RXN18_r_irr_46_f = k38 * Tap42pp * Tap42ppPph2122p
RXN18_r_irr_47_b = k39 * Tap42ppTap42ppPph2122p
RXN19_r_irr_48_f = k40 * Tip41p * Tap42pp
RXN19_r_irr_49_b = k41 * Tip41pTap42pp
RXN20_r_irr_50_f = k42 * Tip41pp * Tap42pp
RXN20_r_irr_51_b = k43 * Tip41ppTap42pp


********** MODEL FUNCTIONS


********** MODEL EVENTS


********** MODEL MATLAB FUNCTIONS

