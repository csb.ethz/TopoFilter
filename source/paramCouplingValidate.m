function [] = paramCouplingValidate(coupling, projectableVec)
%PARAMCOUPLINGVALIDATE Validate parameters coupling. A coupling rule is valid
% if and only if the following implication is true: if all LHS parameters of a
% rule are projectable then all RHS indices are also projectable.
%
% Arguments:
%    coupling (cell array or array): n x 2 cell array as returned by
%       paramCoupling, or n x d x 2 logical array as returned by
%       paramCouplingCaToMatrix
%    projectableVec (numeric/logical): 1 x d projectable params mask
%

    % ensure logical and row-vec
    projectableVec = logical(projectableVec(:)');
    d = numel(projectableVec);

    if iscell(coupling)
        couplingMatrix = paramCouplingCaToMatrix(coupling, d);
    else
        couplingMatrix = logical(coupling);
    end
    assert(ndims(couplingMatrix) == 3 && size(couplingMatrix, 3) == 2 && size(couplingMatrix, 2) == d,...
        'Expected a n x %d x 2 array of params couplings.', d);
    couplingMatrixIf = couplingMatrix(:,:,1);
    couplingMatrixThen = couplingMatrix(:,:,2);

    for j=1:size(couplingMatrixIf,1)
        couplingVecThen = couplingMatrixThen(j,:);
        if all(couplingMatrixIf(j,:) <= projectableVec) && ~all(couplingVecThen <= projectableVec)
            error('PARAMCOUPLINGVALIDATE:InvalidParamCoupling', 'Parameters coupling rule #%d is inconsistent with specification of projectable parameters: RHS parameters %s are not projectable.',...
                j, mat2str(find(couplingVecThen > projectableVec)));
        end
    end

end % function
