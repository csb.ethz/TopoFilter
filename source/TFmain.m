function  [ runs, viabilityThreshold, settings ] = TFmain( varargin )
dl = debuglevel;
%fprintf('[INFO] TFmain, debuglevel = %d\n', dl);

    settings = parseExperimentSettings(varargin{:});
    if (dl > 0)
        fprintf('[DEBUG] TFmain, settings:\n%s\n',evalc('disp(settings)'));
    end

    %% Load inputs
    model = loadModel(settings);

    experiments = loadExperiments(settings);

    expData = loadExpData(settings);
    % TODO: call for every tested projection w/ npar arg => adjust Chi2 df par
    % in calcThresholdQuantile
    viabilityThreshold = expData.threshold;

    paramSpecs = loadParamSpecs(settings);
    [paramSpecs.idxs, paramSpecs.values] = getIQMparametersProps(model, paramSpecs.names);
    settings.couplingMatrix = paramCouplingCaToMatrix(settings.paramCouplingFcn(), numel(paramSpecs.names));
    paramCouplingValidate(settings.couplingMatrix, ~isnan(paramSpecs.projections))

    %% Convert `outputFilename` to a full (abs) path and prep dir if required
    outputFilename = getFullPath(settings.outputFilename);
    [outputPath, ~, ~] = fileparts(outputFilename);
    if exist(outputPath, 'dir') == 0
        mkdir(outputPath);
    end

    %% Setup model evaluation
    % Create mex versions of the model for simulation per experiment
    nExp = numel(experiments);
    mexNames = cell(nExp,1);
    mexCleanups = cell(nExp,1);
    for i = 1:nExp
        experiment = experiments{i};
        expmodel = IQMmergemodexp(model, experiment);
        [mexNames{i}, mexCleanups{i}] = mexModel(expmodel, settings, i);
    end

    % Prepare evaluation options
    evalModelOPTIONS = EvalModelOptions(mexNames, expData, paramSpecs, settings.odeIntegratorOptions);

    %% Perform topological filtering for each run
    % Prep options for topological filtering
    topoFilterOPTIONS = topologicalFilteringOptions(settings);
    % Set up results variable
    runs = cell(settings.numRuns, 1);

    dryRun = settings.dryRun;
    % Parallelize if selected level is higher than "per projection" (2)
    if settings.parallelize > 2
        numWorkers = settings.numWorkers;
    else
        numWorkers = 0;
    end
    strfinfoformat = sprintf('%%s [INFO] TFmain, %%s run #%%0%di/%d\n',floor(log10(settings.numRuns))+1,settings.numRuns);
    %for (i = 1:settings.numRuns) % DEBUG
    % propagate persistent debuglevel over workers
    if settings.parallelize > 0, pctRunOnAll(sprintf('debuglevel(%d);',dl)); end
    parfor (i = 1:settings.numRuns, numWorkers)
        fprintf(strfinfoformat,strftime(),'BEGIN', i);
        [
            runs{i}.essentialParametersCollection, ...
            runs{i}.viableProjectionsCollection, ...
            runs{i}.paramSamplesCollection, ...
            runs{i}.p0, ...
            runs{i}.error0 ...
        ] = topologicalFilteringWrapper( ...
            paramSpecs, viabilityThreshold, dryRun, ...
            topoFilterOPTIONS, evalModelOPTIONS ...
        );
        fprintf(strfinfoformat,strftime(),'END', i);
    end


end

function str = strftime()
    str = datestr(now, '[yyyy.mm.dd HH:MM:SS.FFF]');
end
