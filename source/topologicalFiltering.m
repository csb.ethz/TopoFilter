function viableProjections = topologicalFiltering(threshold, viablePoints, options, paramSpecs, evalModelOPTIONS)
%TOPOLOGICALFILTERING Performs the topological filtering of the root
%   model and then recursively for more models (if option is set) and returns
%   all projections that were found to be viable together with a
%   parameter sample containing one viable parameter point per
%   projection.
%

    %% Initialise
    nRecursiveSteps = 0;

    % Filter and re-sample newly found projections at the end of each
    % topologicalFilteringIteration, if there is a need to do so
    options.prepareViablePoints = (options.saveViablePoints || options.recursive);

    dim = size(viablePoints, 2);

    allViableProjections = options.projectionSetCls(dim);
    % Assuming that a first viable point has already been checked to be viable.
    allViableProjections.add(false(1,dim), viablePoints(1,:));

    %% Pre-recursive step
    % Since filtering and sampling is done at the end of each iteartion (in
    % search tree leaves), invoke it separately for the first time.
    % Rem: do not use prepareViblePointsForProjection directly to go through
    % parfor and spawn a cluster node job if necessary.
    viablePointsCell = prepareViablePointsForProjectionSet(allViableProjections,...
        viablePoints, threshold, options, paramSpecs, evalModelOPTIONS);
    % Run initial topological filtering for the root model
    newViableProjections = topologicalFilteringIteration(...
            allViableProjections.at(1),...
            viablePointsCell{1},...
            threshold,...
            allViableProjections,...
            options,...
            paramSpecs,...
            evalModelOPTIONS...
    );
    viablePointsCell = prepareViablePointsConditionally(...
        newViableProjections, viablePointsCell{1}, threshold, options,...
        paramSpecs, evalModelOPTIONS...
    );

    % Note: allViableProjections has only the null projection, so
    %       everything new and unique (doesn't have to be if
    %       ~options.dropOATProjections) is added now
    allViableProjections.addAll(newViableProjections);

    logViableProjections(nRecursiveSteps, newViableProjections,...
        viablePointsCell, allViableProjections, evalModelOPTIONS);

    foundnew = ~isempty(newViableProjections);

    %%%% Recursive steps
    % Recursive step variables:
    % * newViableProjections: Projections identified as viable in the last
    %   recursive round.
    % * viablePointsCell: viable parameter points for each of the new viable
    %   projection.
    % * allViableProjections: all Projections identified as viable so far.
    while foundnew && options.recursive
        nRecursiveSteps = nRecursiveSteps + 1;
        numProjections = newViableProjections.num;
        fprintf('[INFO] topologicalFiltering, recursive step #%d with %d viable projections.\n',...
            nRecursiveSteps, numProjections);

        % Equality instead of inequality check for `parallelize`, because
        % currently, if option parallelize > 2, then we're already in
        % `parfor` (from `TFmain`) and this loop will never run in parallel.
        % If that's true, then it is more efficient to run the recursive
        % step sequentialy, as the viable projections found in iteration
        % can be skipped in subsequent iterations.
        if options.parallelize == 2
            % Tech: pass unnecessary numWorkers arg for consistency of the
            %       parfor calls
            [...
                newViableProjections,...
                viablePointsCell,...
                allViableProjections...
            ] = topologicalFilteringRecursiveStepParallel(...
                    newViableProjections,...
                    viablePointsCell,...
                    allViableProjections,...
                    options,...
                    threshold,...
                    paramSpecs,...
                    evalModelOPTIONS,...
                    nRecursiveSteps,...
                    options.numWorkers...
            );
        else
            [...
                newViableProjections,...
                viablePointsCell,...
                allViableProjections...
            ] = topologicalFilteringRecursiveStepSequential(...
                    newViableProjections,...
                    viablePointsCell,...
                    allViableProjections,...
                    options,...
                    threshold,...
                    paramSpecs,...
                    evalModelOPTIONS,...
                    nRecursiveSteps...
            );
        end

        logViableProjections(nRecursiveSteps, newViableProjections,...
            viablePointsCell, allViableProjections, evalModelOPTIONS);

        foundnew = ~isempty(newViableProjections);
    end

    %%%% Return
    viableProjections = allViableProjections;
    fprintf('[INFO] topologicalFiltering, FINISHED after recursive step #%d with a total nr of viable projections = %d.\n',...
        nRecursiveSteps, viableProjections.num);

end



function logViableProjections(nStep, newViableProjections,...
    viablePointsCell, allViableProjections, evalModelOPTIONS...
)
debugging = (debuglevel > 0);
    % log new viable projections
    fprintf('[INFO] topologicalFiltering, post-recursive step #%d: nr of new (unique) viable projections = %d.\n',...
        nStep, newViableProjections.num);
    fprintf('[INFO] topologicalFiltering, post-recursive step #%d: model (and param spec) param indices of viable projections found in this step:\n',...
        nStep);
    for i = 1:newViableProjections.num
        vPI = newViableProjections.at(i);
        fprintf('\t%s (%s)\n', mat2str(evalModelOPTIONS.modifiableModParamIdxs(vPI)), mat2str(find(vPI)));
        if debugging
            fprintf('\t[DEBUG] #(projected params) = %d, dim(viablePoints) = %d\n',...
                sum(vPI), size(viablePointsCell{i},2));
            assert(numel(vPI) == size(viablePointsCell{i},2),...
                'Row-desynchronised projections and points.');
        end
    end
    if debugging
        % log all viable projections so far
        fprintf('[DEBUG] topologicalFiltering, post-recursive step #%d: param indices of all found viable projections found so far\n',...
            nStep);
        for i = 1:allViableProjections.num
            fprintf('\t%s\n', mat2str(evalModelOPTIONS.modifiableModParamIdxs(allViableProjections.at(i))));
        end
    end
end





function outPointsCell = prepareViablePointsForProjectionSet(...
    projectionSet, inPointsCell, threshold, options,...
    paramSpecs, evalModelOPTIONS)
%PREPAREVIABLEPOINTSFORPROJECTIONSET Wrapper for
%  prepareViablePointsForProjection w/ parallelization per projection in
%  the set. Header is the same, except that inPointsCell may be both
%  a numeric matrix of points as well a cell array of points, separate for
%  each projection.
%
% Inputs:
%     inPointsCell (numeric or cell array): an array of viable points to
%       project and filter for each projeciton or a cell array of matrices,
%       one for each of the projections in the set.
%     ...
%     options.numWorkers (integer): number of parallel worker; default: 0.
%
% Outputs:
%     outPointsCell (cell array): cell array of matrices with viable points
%       for each of the projections.
%

% Note: this method doesn't belong to the ProjectionSet class code, as it
%       would bound the class with the evalModel infrastructure.
debugging = (debuglevel > 0);
    n = projectionSet.num;
    if ~iscell(inPointsCell) % same array for each projection
        assert(isnumeric(inPointsCell), 'Expecting a numeric array.');
        inPointsCell = {inPointsCell};
        inPointsCell = inPointsCell(ones(n,1)); % replicate
    elseif numel(inPointsCell) ~= n
        error('PREPAREVIABLEPOINTSFORPROJECTIONSET:IncompatibleSize',...
            'Incompatible size of a projection set and a cell array of points.');
    end
    outPointsCell = cell(n, 1);
    % Control parallelization using the numWorkers variable
    if ~isfield(options, 'numWorkers') || isempty(options.numWorkers)
        numWorkers = 0;
    else
        numWorkers = options.numWorkers;
    end
    if debugging
        assert(~xor(numWorkers > 0, options.parallelize > 0),...
            'numWorkers option value doesn''t agree with parallelize option value');
    end
    % parfor-specific technicalities
    projectionsRowmat = projectionSet.rowmat; % more mem efficient in the parfor loop than self.at(i)
    witnessPointsRowmat = projectionSet.parrowmat;
    %for (i = 1:n) % DEBUG
    parfor (i = 1:n, numWorkers)
        projection = projectionsRowmat(i,:);
        witnessPoint = witnessPointsRowmat(i,:);
        outPointsCell{i} = prepareViablePointsForProjection(...
            projection, witnessPoint, inPointsCell{i}, threshold, options,...
            paramSpecs, evalModelOPTIONS);
    end
end

function viablePointsCell = prepareViablePointsConditionally(...
    projectionSet, viablePoints, threshold, options,...
    paramSpecs, evalModelOPTIONS)
% PREPAREVIABLEPOINTSCONDITIONALLY If needed prepare viable points for each
% projection in the set.
%
% Inputs:
%     ...
%     options.prepareViablePoints (boolean): flag indicating wether to
%       actually prepare the points or to create empty dummies.
%
    if options.prepareViablePoints
        viablePointsCell = prepareViablePointsForProjectionSet(...
            projectionSet, viablePoints, threshold, options,...
            paramSpecs, evalModelOPTIONS);
        % sanity check
        for i=1:numel(viablePointsCell)
            if(size(viablePointsCell{i},1)==0)
                [proj, parsamp] = projectionSet.at(i);
                error('PREPAREVIABLEPOINTSCONDITIONALLY:NoViablePoints',...
                    'No viable points for projection %s after preparation of new points, whereas point %s was found viable earlier.',...
                    mat2str(int8(proj)),mat2str(parsamp));
            end
        end
    else
        viablePointsCell = mat2cell( zeros(0,numel(projectionSet.dim)),...
            zeros(projectionSet.num,1) );
    end
end





function [...
        newViableProjections,...
        newViablePointsCell,...
        allViableProjections...
] = topologicalFilteringRecursiveStepParallel(...
        currentViableProjections,...
        viablePointsCell,...
        allViableProjections,...
        options,...
        threshold,...
        paramSpecs,...
        evalModelOPTIONS,...
        nRecursiveSteps,...
        numWorkers...
)
%TOPOLOGICALFILTERINGRECURSIVESTEPPARALLEL
% Running `topologicalFilteringIteration` in parallel for each of the
% of the `currentViableProjections`.
% Downside: can't check for redundant projections while progressing,
% to skip e.g. costly re-sampling.
%
debugging = (debuglevel > 0);

    numProjections = currentViableProjections.num;

    % bulk sets for new projections found in parallel recursive iterations
    newViableProjectionsA(numProjections) = options.projectionSetCls();
    currentViableProjectionsRowmat = currentViableProjections.rowmat;
    %for (i = 1:numProjections) % DEBUG
    parfor (i = 1:numProjections, numWorkers)
        fprintf('[INFO] topologicalFilteringRecursiveStepParallel, recursive step.iteration = %d.%d.\n',...
            nRecursiveSteps,i);
        currentProjectionI = currentViableProjectionsRowmat(i,:);
        newViableProjectionsA(i) = topologicalFilteringIteration(...
            currentProjectionI,...
            viablePointsCell{i},...
            threshold,...
            allViableProjections,...
            options,...
            paramSpecs,...
            evalModelOPTIONS...
        );
    end

    % Flatten the collected data
    nProjVec = arrayfun(@(vp) vp.num, newViableProjectionsA);
    nProjTot = sum(nProjVec);
    if debugging
        fprintf('[DEBUG] topologicalFilteringRecursiveStepParallel, newViableProjectionsA(1...%d).num = %s:\n',...
            numProjections, mat2str(nProjVec));
    end

    % Update global cache
    nProjGlobalPre = allViableProjections.num; % only for the assert below
    % Note: only max projections will be added with MaxProjectionSet
    [Inew, Ipresent, IInewDuplicates] = allViableProjections.addAll(newViableProjectionsA);

    % Create local cache (output for recursive search)
    % Taking advantage of addAll being 'stable', i.e. new unique
    % projections were _appended to the end_ of allViableProjections
    % (better than for loop over Inew and (flat) newViableProjectionsA)
    nProjGlobal = allViableProjections.num;
    nProjLocal = numel(Inew);
    assert(nProjGlobal == nProjGlobalPre+nProjLocal,...
        'ProjectionSet.addAll() fail');
    newViableProjections = allViableProjections.reduceTo(...
        (nProjGlobal-nProjLocal+1):nProjGlobal);

    % Merge source viable points for all new unique projections found
    viablePointsUniqueCell = replicatePoints(...
        viablePointsCell, nProjVec,...
        Inew, Ipresent, IInewDuplicates...
    );

    % Report incl. recursive step redundancies (due to parallel iterations
    % or due to missing check of OAT reductions against the global cache)
    nProjUnique = newViableProjections.num;
    fprintf('[INFO] topologicalFilteringRecursiveStepParallel, post-recursive step #%d: nr of viable projections found unique/new = %d/%d.\n',...
        nRecursiveSteps, nProjUnique, nProjTot);
    if debugging
        fprintf('[DEBUG] topologicalFilteringRecursiveStepParallel, post-recursive step #%d: unique projections indices = %s\n',...
            nRecursiveSteps, mat2str(Inew));
    end

    % prepare points (only for the globally new projections)
    newViablePointsCell = prepareViablePointsConditionally(...
        newViableProjections, viablePointsUniqueCell,...
        threshold, options,...
        paramSpecs, evalModelOPTIONS...
    );

end



function [...
        newViableProjections,...
        newViablePointsCell,...
        allViableProjections...
] = topologicalFilteringRecursiveStepSequential(...
        currentViableProjections,...
        viablePointsCell,...
        allViableProjections,...
        options,...
        threshold,...
        paramSpecs,...
        evalModelOPTIONS,...
        nRecursiveSteps...
)
%TOPOLOGICALFILTERINGRECURSIVESTEPSEQUENTIAL
% Running `topologicalFilteringIteration` sequentially for each of the
% of the `currentViableProjections`.
%
% Note: used recursively this function is likely to find less viable
%       projections because of in-between iterations caching and hence,
%       less candidate points being merged for testing in the next
%       recursive step.
%
debugging = (debuglevel > 0);

    numProjections = currentViableProjections.num;

    % sets for new projections found in sequential recursive iterations
    newViableProjections = options.projectionSetCls();

    nProjVec = zeros(numProjections,1);
    nProjCumVec = zeros(numProjections,1);
    nProjTot = 0;

    for i = 1:numProjections

        fprintf('[INFO] topologicalFilteringRecursiveStepSequential, recursive step.iteration = %d.%d.\n',...
            nRecursiveSteps,i);
        currentProjectionIter = currentViableProjections.at(i);

        if options.recursive
            % Note: this is currently always true with how the recursive
            %       search in topologicalFiltering is constructed now,
            %       i.e. this function is called only in the
            %           while (... && options.recursive)
            %       loop
            [checkInNextStep, inewViableProjections] = newViableProjections.has(currentProjectionIter);
        else
            checkInNextStep = false;
        end
        if ~checkInNextStep

            newViableProjectionsIter = topologicalFilteringIteration(...
                currentProjectionIter,...
                viablePointsCell{i},...
                threshold,...
                allViableProjections,...
                options,...
                paramSpecs,...
                evalModelOPTIONS...
            );

            % update local cache
            % First local to merge the source points for duplicates
            % Note: only max projections will be added with MaxProjectionSet
            [InewLocal, ~, ~, IpresentLocal] = newViableProjections.addAll(newViableProjectionsIter);
            % append current source points to the points used to find same
            % projections in previous iterations
            % note: IpresentLocal are indices of newViableProjectionsIter
            J = arrayfun(@(j) find(j <= nProjCumVec(1:(i-1)), 1), IpresentLocal);
            % unique for lowering memory footprint
            viablePointsCell(J) = cellfun(...
                @(vPCj) unique(vertcat(vPCj, viablePointsCell{i}), 'rows'),...
                viablePointsCell(J), 'UniformOutput', false...
            );

            % update global cache
            % Scratching with right hand behind left ear to overcome
            % ProjectionSet implementation defficiencies:
            % a) revert newViableProjections (works because addAll is 'stable')
            newViableProjections = newViableProjections.reduceTo(1:nProjTot);
            % b) add globally unique projections (bonus: local uniqueness knowledge)
            newUniqueLocalViableProjectionsIter = newViableProjectionsIter.reduceTo(InewLocal);
            InewUniqueGlobalIter = allViableProjections.addAll(newUniqueLocalViableProjectionsIter);
            % c) re-update local cache
            newUniqueGlobalViableProjectionsIter = newUniqueLocalViableProjectionsIter.reduceTo(InewUniqueGlobalIter);
            newViableProjections.addAll(newUniqueGlobalViableProjectionsIter);
            nProjVec(i) = newUniqueGlobalViableProjectionsIter.num;
            % Note: in `else` branch, we're also using the fact that addAll
            %       is 'stable' new viable projections are always _appended
            %       to the end_ of newViableProjections set (and that this
            %       set is never re-ordered, e.g. by using reduceTo() above).

            % report incl. redundancies (due to missing check of OAT
            % reductions against the global cache)
            fprintf('[INFO] topologicalFilteringRecursiveStepSequential, nr of viable projections found unique/new = %d/%d.\n',...
                newUniqueGlobalViableProjectionsIter.num, newViableProjectionsIter.num);

        else
            % nProjVec(i) == 0
            %
            % append current points to the points used to find this
            % projection in a previous j-th iteration
            j = find(inewViableProjections <= nProjCumVec(1:(i-1)), 1);
            assert(~isempty(j));
            viablePointsCell{j} = unique(vertcat(viablePointsCell{j}, viablePointsCell{i}),'rows');
            % TODO: these are already filtered points - they shouldn't, but
            %       unfortunately will be filtered and tested again
            %       (for one way to resolve it see #23)
            if debugging
                fprintf('[DEBUG] topologicalFilteringRecursiveStepSequential, recursive step.iteration = %d.%d, skipping global viable projection found in previous iterations; param indices: %s.\n',...
                    nRecursiveSteps,i,mat2str(evalModelOPTIONS.modifiableModParamIdxs(currentProjectionIter)));
            end
        end
        nProjTot = nProjTot + nProjVec(i); % nProjTot == sum(nProjVec(1:i))
        nProjCumVec(i) = nProjTot; % nProjCumVec(1:i) == cumsum(nProjVec(1:i))

    end

    % For better parallelization prepare points in one go at the end of the
    % recursive step (instead of preparing them in each iteration).
    % Only replicate points, sine:
    %  1) no (global) duplicates here - ensured by addAll() calls above;
    %  2) source viablePoints already directly merged (else branche above)
    viablePointsUniqueCell = replicatePoints(viablePointsCell, nProjVec);
    % prepare points (only for the globally new projections)
    newViablePointsCell = prepareViablePointsConditionally(...
        newViableProjections, viablePointsUniqueCell,...
        threshold, options,...
        paramSpecs, evalModelOPTIONS...
    );

    fprintf('[INFO] topologicalFilteringRecursiveStepSequential, post-recursive step #%d: nr of new viable projections found = %d.\n',...
        nRecursiveSteps, nProjTot);

end



function outPointsPerNewProjFlatCell = replicatePoints(...
    pointsPerProjSetCell, nProjPerProjSetVec,...
    varargin...
)
%
% varargin: IFlatProjNew, IFlatProjPresent, IIFlatProjNewDuplicates
%     Indicies instructing points merges across the projections sets.
%
debugging = (debuglevel > 0);

    nProjTot = sum(nProjPerProjSetVec);
    if debugging
        assert(numel(pointsPerProjSetCell) == numel(nProjPerProjSetVec));
    end

    if isempty(varargin)
        IFlatProjNew = 1:nProjTot;
        IFlatProjPresent = [];
        IIFlatProjNewDuplicates = IFlatProjNew;
    elseif numel(varargin) < 3
        error('REPLICATEPOINTS:InvalidArgument','Expecting exactly 0 or 3 optional arguments with indices instructing merges of points across the projection sets.');
    else
        IFlatProjNew = varargin{1};
        IFlatProjPresent = varargin{2};
        IIFlatProjNewDuplicates = varargin{3};
        if debugging
            assert(all(ismember(IFlatProjPresent,1:nProjTot)));
            assert(all(ismember(IFlatProjNew,1:nProjTot)));
            assert(all(ismember(IIFlatProjNewDuplicates,1:numel(IFlatProjNew))));
            assert(all(ismember(1:numel(IFlatProjNew),IIFlatProjNewDuplicates)),...
                'Expecting that each new projection is its own duplicate.');
            assert(numel(IFlatProjPresent) + numel(IIFlatProjNewDuplicates) == nProjTot);
        end
    end

    % IprojSetPerProjFlat(i)=j: i-th projection came from j-th set, j<=i
    IprojSetPerProjFlat = zeros(nProjTot,1);
    nProjCum = 0; % temporary cumulative sum
    for i=1:numel(nProjPerProjSetVec)
        ni = nProjPerProjSetVec(i);
        IprojSetPerProjFlat((nProjCum+1):(nProjCum+ni)) = i*ones(ni,1);
        nProjCum = nProjCum+ni;
    end
    IFlatProjNewDuplicates = setdiff((1:nProjTot), IFlatProjPresent);
    outPointsPerNewProjFlatCell = cell(numel(IFlatProjNew),1);
    for i=1:numel(IFlatProjNew),
        IpointsPerProjSetCell = unique(IprojSetPerProjFlat(IFlatProjNewDuplicates(IIFlatProjNewDuplicates == i)));
        outPointsPerNewProjFlatCell{i} = unique(vertcat(pointsPerProjSetCell{IpointsPerProjSetCell}),'rows');
    end
end



function newViableProjections = topologicalFilteringIteration(currentProjection, viablePoints, threshold, allViableProjections, options, paramSpecs, evalModelOPTIONS)
%TOPOLOGICALFILTERINGITERATION Returns new viable projections (with
%    parameter point) discovered by a single `getViableProjections` call,
%    starting at `currentProjection`.
%
debugging = (debuglevel > 0);

    projectedParameterIdxs = evalModelOPTIONS.modifiableModParamIdxs(currentProjection);
    localModifiableParameterIdxs = find(~currentProjection); % remaining parameters

    if debugging
        fprintf('[DEBUG] topologicalFilteringIteration, projected parameter indices: %s.\n', mat2str(projectedParameterIdxs));
        fprintf('[DEBUG] topologicalFilteringIteration, currentProjection = %s.\n', mat2str(int8(currentProjection)));
        fprintf('[DEBUG] topologicalFilteringIteration, modifiable parameter indices = %s.\n', mat2str(localModifiableParameterIdxs));
        fprintf('[DEBUG] topologicalFilteringIteration, projectable parameter indices = %s.\n', mat2str(evalModelOPTIONS.possibleToFixSpecParamIdxs));
        fprintf('[DEBUG] topologicalFilteringIteration, size(viablePoints) = %s.\n', mat2str(size(viablePoints)));
    end

    noViablePoints = (size(viablePoints,1) == 0);
    if noViablePoints
    % Attention: shouldn't happen, i.e. to get here, current projection must
    %            have been viable for at least one point in previous iteration.
        warning('TOPOLOGICALFILTERINGITERATION:MissingViablePoints', 'No viable points left.');
    end
    if (numel(localModifiableParameterIdxs) == 0) || noViablePoints
        newViableProjections = options.projectionSetCls();
        return
    end

    % Project modfiable axes and check viability (getViableProjections)
    % TODO: to avoid duplicate model evaluations for some of the points when
    %      filtering them for projections further in perpareViablePoints,
    %      getViableProjections could take an option to return points
    %      already filtered for each of the found projections, or to stop
    %      on the first witness point found (as it currently does).
    %      Decision if to not stop on the first witness would be then same as:
    %          options.prepareViablePoints
    newReducedViableProjections = getViableProjections(viablePoints, threshold, currentProjection, allViableProjections, options.getViableProjectionsOPTIONS, evalModelOPTIONS);

    % Extend projections
    % TODO: extending procedure can be eliminated by always having a final
    %      fixed dimension ProjectionSet with a current projection mask
    %      (currentProjection; cf. getViableProjections) and
    %      default parameter sample values (options.projections ? == evalModelOPTIONS.paramFixValues)
    % Update: now getViableProjections takes always root model size viablePoints
    %         and currentProjection, so this should be easy eliminate once
    %         saving projection in getViableProjections is adjusted (no need to
    %         extend vectors anymore)
    newViableProjections = extendProjectionSet(newReducedViableProjections, currentProjection, paramSpecs.projections, options);
end

% TODO: if not eliminated (cf. TODO comment at the call):
%      mv to ProjectionSet as a method + add unit tests
function extendedProjectionSet = extendProjectionSet(reducedProjections, projectionMask, paramSampleDefault, options)
    extendedProjectionSet = options.projectionSetCls(numel(projectionMask), reducedProjections.num);
    for i = 1:reducedProjections.num
        [projectionI, paramSampleI] = reducedProjections.at(i);
        extendedProjectionSet.add(...
            extendVec(projectionMask, projectionMask, projectionI),...
            extendVec(projectionMask, paramSampleDefault, paramSampleI)...
        );
    end
end
