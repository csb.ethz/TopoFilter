function [simdata, tps] = adaptation_extractSimData(nsol, name, expdata, varargin)

    tps = adaptation_getPeakTimePoints(nsol, name);
    simt = nsol.time';
    simd = getIQMsimulateValues(nsol, name);
    simdata = interp1(simt, simd, tps);

end
