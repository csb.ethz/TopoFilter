function [ idxs ] = getIQMstatesProps(model, names)
% GETIQMSTATESPROPS ...
%
%

    n = numel(names);
    idxs = zeros(1, n);
    modelNames = IQMstates(model);
    for i = 1:n
        newIdx = find(strcmp(modelNames,names(i)));
        if (~isempty(newIdx))
            idxs(i) = newIdx;
        else
            error('GETIQMSTATESPROPS:InvalidName', 'Invalid name: state ''%s'' not found in the model.', names{i});
        end
    end

end
