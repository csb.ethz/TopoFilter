function couplingMatrix = paramCouplingCaToMatrix(couplingCa, d)
%PARAMCOUPLINGCATOMATRIX Convert params coupling cell array with n rows (see
% paramCoupling) to two logical n x d arrays of coupling premises and coupling
% conclusions, returned as a single n x d x 2 array.
%

    assert(iscell(couplingCa) && (size(couplingCa, 1) == 0 || size(couplingCa, 2) == 2), 'Expected a two-column cell array of params coupling.');
    nCouplings = size(couplingCa, 1);
    if nargin < 2
        d = max(max(cellfun(@max, couplingCa)));
    end
    assert(isposint(d), 'Expected a positive integer dimension d.')
    couplingMatrix = false(nCouplings, d, 2);
    for i = 1:nCouplings
        couplingMatrix(i, couplingCa{i,1}, 1) = true;
        couplingMatrix(i, couplingCa{i,2}, 2) = true;
    end

end % function
