%%
close all
clearvars
clc

%% setup
% Below, default setting for running this script in the toy experiment
% folder - adjust to your needs. Also, if necessary, adjust further down:
%    costToLikelihood, threshold, nsamp
% and whaterver else needs adjustment in your setup.
%

% toy-ligand-fluorescent
% integration pieces: model, data, cost function & param spec
settings.modelFilename = 'model.txt';
settings.expFilename = {'exp-SteadyState.exp'};
settings.expDataFilename = 'expData.xls';
settings.paramSpecsFilename = 'paramSpecs_4a.txt';
% saved viable points
folderPath = fullfile('results');
fileNamePattern = 'TFrankProjectionsTest_viablePoints_*.mat';


% % claude-decoding
% % integration pieces: model, data, cost function & param spec
% modid = 'decoding_011';
% settings.modelFilename = sprintf('%s.txtbc',modid);
% settings.expFilename = {'exp-control.exp','exp-alpha.exp','exp-input.exp'};
% settings.expDataFilename = 'expData.xls';
% settings.calcCostFcn = @decoding_calcCost;
% settings.calcThresholdFcn = @decoding_calcThreshold;
% settings.paramSpecsFilename = 'paramSpecs.txt';
% % saved viable points
% timestamp = 'EDIT ME';
% folderPath = fullfile('results', timestamp, modid);
% fileNamePattern = sprintf('%s_viablePoints_*.mat',timestamp);


fileNames = dir(fullfile(folderPath, fileNamePattern));
fileNames = {fileNames.name};
assert(~isempty(fileNames),'No files found. Are you in the correct folder? If yes, check the path and the file name pattern?')

rmDuplicateProjections=true; % Out of duplicates, take viable points with the largest sample

%% load saved viable points
nMod = numel(fileNames);
for iPar=nMod:-1:1
    fn = fileNames{iPar};
    loaded = load(fullfile(folderPath,fn));
    viablePointsArray(iPar) = loaded.viablePoints;
    % for duplicates removal
    projectionArray(iPar,:) = loaded.viablePoints.projection';
end

% Remove duplicates
if rmDuplicateProjections
    % Save init set for reference
    fileNames0 = fileNames;
    viablePointsArray0 = viablePointsArray;

    [uniqueProjectionArray, uniqueProjectionRowIdxs] = unique(projectionArray,'rows','stable');
    repeatsProjectionRowIdxs = setdiff(1:nMod, uniqueProjectionRowIdxs);
    repeatsProjectionArray = projectionArray(repeatsProjectionRowIdxs,:);
    nMod = numel(uniqueProjectionRowIdxs);
    for iPar=nMod:-1:1
        projectionIdx = uniqueProjectionRowIdxs(iPar);
        projection = projectionArray(projectionIdx,:);
        repeatsIdxs = find(ismember(repeatsProjectionArray, projection, 'rows'));
        %disp(repeatsProjectionRowIdxs(repeatsIdxs));
        maxSampSize = numel(viablePointsArray(projectionIdx).cost);
        for iRep=1:numel(repeatsIdxs)
            repeatIdx = repeatsProjectionRowIdxs(repeatsIdxs(iRep));
            repeatSampSize = numel(viablePointsArray(repeatIdx).cost);
            if repeatSampSize > maxSampSize
                maxSampSize = repeatSampSize;
                projectionIdx = repeatIdx;
            end
        end
        uniqueProjectionRowIdxs(iPar) = projectionIdx;
    end

    viablePointsArray = viablePointsArray(uniqueProjectionRowIdxs);
    fileNames = fileNames(uniqueProjectionRowIdxs);
end

%% prepare models and data

% TODO: prepareModels wrapper funcion (here + TFsimulate + TFmain)
model = loadModel(settings);
experiments = loadExperiments(settings);

nExp = numel(experiments);
mexNames = cell(nExp,1);
mexCleanups = cell(nExp,1);
for iPar = 1:nExp
    experiment = experiments{iPar};
    expmodel = IQMmergemodexp(model,experiment);
    [mexNames{iPar}, mexCleanups{iPar}] = mexModel(expmodel);
end

expData = loadExpData(settings);


%% Compute "evidence" for a model, assuming unifrom priors
% evidence = integrated likelihood over viable space
%
% If we had a cost-based sampling, we need to define a map of the cost to
% the likelihood (higher is better). For the negative log-likelihood it's:
costToLikelihood = @(c) exp(-c);

% Attention: Here threshold is computed with default options (method and
%            Chi2 quantile or std dev multiplier); adjust if changed when
%            runnning TFmain, or load from the results file.
threshold = expData.threshold;
paramSpecs = loadParamSpecs(settings);

nsamp = 1e3; % consider also in each iteration: nsamp = 10*npoints
plot2dSamples = true; % cf. scatterellipse

for iMod=nMod:-1:1
    points = viablePointsArray(iMod);
    % projection strings
    projectionStr{iMod} = strrep(mat2str(int8(points.projection(:))),';','');
    fprintf('TFrankProjections, Volint #%d/%d, projection = %s .. \n',nMod-iMod+1,nMod,projectionStr{iMod});

    % prepare points
    variableParamValuesHeadRowmat = points.rowmat;
    [npoints,nparvar] = size(variableParamValuesHeadRowmat);
    if ~isfield(points, 'projected')
        projected.names = {};
        projected.values = [];
    else
        projected = points.projected;
    end
    nparfix = length(projected.names);
    paramNames = vertcat(points.colnames, projected.names);
    npartot = numel(paramNames);
    fixedParamValuesTail = projected.values;
    if ~isfield(points, 'islog') || isempty(points.islog)
        islog = false(1,nparvar);
    else
        islog = points.islog;
    end

    % setup persistent data in the cost function
    evalModelDirect(mexNames, expData, paramNames, islog, fixedParamValuesTail);

    % Get bmin and bmax for the variable parameters
    % 1) map from the parameters specification order to the order where
    %    first come variable and then fixed parameters
    paramNamesIdx = zeros(1,npartot);
    for iPar=1:npartot
        pName = paramNames{iPar};
        paramNamesIdx(iPar) = find(strcmp(pName,paramSpecs.names));
    end
    bmax = paramSpecs.bmax(paramNamesIdx)';
    bmin = paramSpecs.bmin(paramNamesIdx)';
    % 2) drop fixed parameters
    bmax = bmax(1:nparvar);
    bmin = bmin(1:nparvar);
    % 3) set log-scales
    bmax(islog) = log10(bmax(islog));
    bmin(islog) = log10(bmin(islog));

    % parameter space volume
    spacevol(iMod) = prod(bmax - bmin);

    % sample for cost and to est. viable space volume
    try
        OutV = Volint('evalModelDirect', threshold, variableParamValuesHeadRowmat, bmax, bmin, nsamp);
        if isempty(OutV.cost)
            error('Volint has not found a single viable point.');
        end
    catch ME
        relvvol(iMod) = 0;
        evidence(iMod)=Inf;
        evidence_err(iMod)=0;
        fprintf('\n\t .. error:\n\t%s: %s\n', ME.identifier, ME.message);
        continue;
    end

    % est. vol. of the viable space relative to the parameter space vol.
    relvvol(iMod) = OutV.vol / spacevol(iMod);

    % ranking values
    OutV.likelihood = costToLikelihood(OutV.cost);
    evidence(iMod) = relvvol(iMod) * mean(OutV.likelihood);
    evidence_err(iMod) = relvvol(iMod) * std(OutV.likelihood)/sqrt(numel(OutV.likelihood));

    if (nparvar==2) && plot2dSamples
        scatterellipse(variableParamValuesHeadRowmat, OutV.V, OutV.cost, OutV.ellip.A, OutV.ellip.c,...
            sprintf('Model #%d; original sample (black) and re-sampled (colored by cost)', iMod));
    end


    % for further analysis
    OutVArray(iMod) = OutV;

    fprintf('\n\t .. done\n');
end

%% Plot evidence

[evS, evI] = sort(evidence,'descend');
figure
hold on
bar(evS);
set(gca, 'XTick', 1:numel(evidence), 'XTickLabel', projectionStr(evI));
errorbar(1:numel(evidence),evS,evidence_err(evI),evidence_err(evI),'r.');
xlabel('Projection'); ylabel('Evidence for describing data');
title('Ranking of models (projections)');
