function [ mexFn, cleanupObj ] = mexModel( model, settings, int_suffix )
%MEXMODEL Create mex versions of the model for simulation.
%
%    Returns a callable MEX name (i.e. w/o folder path and w/o the extension),
%    which consists of the given name prefix and a unique integer suffix (clock
%    tick in milliseconds if not given explicitly)
%
%    Moreover a cleanup handle is returned as a second output if  and it has to be
%    assigned, othwerwise MEX will be deleted on exit from this function.
%
%   `settings` struct fields used:
%       `modelFilename` If set as a string, use as the file name part as
%                       a prefix for the MEX file. Otherwise, model name is
%                       used.
%       `parallelize`   If true ,attach mex file to a `gcp` pool.
%

    if nargin < 2
        settings = struct();
    end
    if nargin < 3
        int_suffix = clockStamp();
    elseif ~isint(int_suffix)
        error('MEXMODEL:IllegalArgument',...
            'Expecting suffix of the MEX model to be an integer.');
    end

    if ~(ischar(model) && exist(model,'file') == 3)

        % Get mex file name w/o ext
        if isfield(settings, 'modelFilename') && ischar(settings.modelFilename)
            [~,mfn] = fileparts(settings.modelFilename);
            mfi = dir(which(settings.modelFilename));
            model_dt = datetime(mfi.datenum, 'ConvertFrom','datenum');
        else
            modstruct = IQMstruct(model);
            mfn = modstruct.name;
            % model is not a file - assuming it's newest possible
            model_dt = datetime(now, 'ConvertFrom', 'datenum');
        end
        % Create mex versions of the model for simulation
        mexFn = genvarname(sprintf('%s_%d',mfn,int_suffix));
        % .. if MEX model was not generated already
        gen_mex = ~(exist(mexFn,'file') == 3);
        % .. or it was generated but model file is newer then the MEX file
        if ~gen_mex
            mfi = dir(which(mexFn));
            mex_dt = datetime(mfi.datenum, 'ConvertFrom','datenum');
            gen_mex = (mex_dt < model_dt);
        end
        % .. then generate MEX model
        if gen_mex
            IQMmakeMEXmodel(model,mexFn,0);
        end
    end
    if isfield(settings, 'cleanupMexFiles') && settings.cleanupMexFiles
        mexPath = fullfile(pwd,mexEqfn(mexFn));
        cleanupObj = onCleanup(@() deleteIfExists(mexPath));
    else
        cleanupObj = NaN;
    end


    % make mex file available for all workers
    if isfield(settings, 'parallelize') && (settings.parallelize > 0)
        pool = gcp;
        addAttachedFiles(pool, mexEqfn(mexFn));
    end

end

function [ ret ] = mexEqfn(name)
%MEXEQFN MEX extension qualified file name.
    ret = sprintf('%s.%s', name, mexext);
end

function deleteIfExists(fn)
    if (exist(fn,'file'))
        delete(fn);
    end
end
