********** MODEL NAME
Model 4: Two branches plus receptor degradation
taking parameters from Model 2

********** MODEL NOTES


********** MODEL STATES
d/dt(EGF) = (-reaction_1+reaction_2)/cell {isSpecie:cell:concentration}                                                                
d/dt(boundEGFR) = (-reaction_0+reaction_0+reaction_1-reaction_2-reaction_25+reaction_25-reaction_30)/cell {isSpecie:cell:concentration}
d/dt(unboundEGFR) = (-reaction_1+reaction_2)/cell {isSpecie:cell:concentration}                                                        
d/dt(activeSos) = (+reaction_0-reaction_3-reaction_4+reaction_4-reaction_12)/cell {isSpecie:cell:concentration}                        
d/dt(inactiveSos) = (-reaction_0+reaction_3-reaction_11)/cell {isSpecie:cell:concentration}                                            
d/dt(activeRas) = (+reaction_4-reaction_5-reaction_6+reaction_6-reaction_28+reaction_28)/cell {isSpecie:cell:concentration}            
d/dt(inactiveRas) = (-reaction_4+reaction_5)/cell {isSpecie:cell:concentration}                                                        
d/dt(cRaf) = (-reaction_6+reaction_7-reaction_13)/cell {isSpecie:cell:concentration}                                                   
d/dt(cRafPP) = (+reaction_6-reaction_7-reaction_8+reaction_8)/cell {isSpecie:cell:concentration}                                       
d/dt(MEK) = (-reaction_8+reaction_9-reaction_24)/cell {isSpecie:cell:concentration}                                                    
d/dt(MEKPP) = (+reaction_8-reaction_9-reaction_10+reaction_10+reaction_24)/cell {isSpecie:cell:concentration}                          
d/dt(ERK) = (-reaction_10+reaction_29)/cell {isSpecie:cell:concentration}                                                              
d/dt(ERKPP) = (+reaction_10-reaction_11+reaction_11-reaction_12+reaction_12-reaction_29)/cell {isSpecie:cell:concentration}            
d/dt(removedSos) = (+reaction_11+reaction_12)/cell {isSpecie:cell:concentration}                                                       
d/dt(PKA) = (-reaction_13+reaction_13+reaction_14+reaction_15-reaction_16)/cell {isSpecie:cell:concentration}                          
d/dt(removedcRaf) = (+reaction_13)/cell {isSpecie:cell:concentration}                                                                  
d/dt(inactivePKA) = (-reaction_14-reaction_15+reaction_16)/cell {isSpecie:cell:concentration}                                          
d/dt(EPAC) = (+reaction_17+reaction_18-reaction_19-reaction_20+reaction_20)/cell {isSpecie:cell:concentration}                         
d/dt(inactiveEPAC) = (-reaction_17-reaction_18+reaction_19)/cell {isSpecie:cell:concentration}                                         
d/dt(activeRap1) = (+reaction_20-reaction_21-reaction_22+reaction_22+reaction_27)/cell {isSpecie:cell:concentration}                   
d/dt(inactiveRap1) = (-reaction_20+reaction_21-reaction_27)/cell {isSpecie:cell:concentration}                                         
d/dt(BRaf) = (-reaction_22+reaction_23-reaction_28)/cell {isSpecie:cell:concentration}                                                 
d/dt(BRafPP) = (+reaction_22-reaction_23-reaction_24+reaction_24+reaction_28)/cell {isSpecie:cell:concentration}                       
d/dt(activeC3G) = (+reaction_25-reaction_26-reaction_27+reaction_27)/cell {isSpecie:cell:concentration}                                
d/dt(inactiveC3G) = (-reaction_25+reaction_26)/cell {isSpecie:cell:concentration}                                                      
d/dt(Gap) = (-reaction_5+reaction_5-reaction_21+reaction_21)/cell {isSpecie:cell:concentration}                                        
d/dt(EPACA) = (-reaction_17+reaction_17)/cell {isSpecie:cell:concentration}                                                            
d/dt(Cilostamide) = (-reaction_15+reaction_15-reaction_18+reaction_18)/cell {isSpecie:cell:concentration}                              
d/dt(PKAA) = (-reaction_14+reaction_14)/cell {isSpecie:cell:concentration}                                                             
d/dt(degradedEGFR) = (+reaction_30)/cell {isSpecie:cell:concentration}                                                                 
                                                                                                                                       
EGF(0) = 0                                                                                                                             
boundEGFR(0) = 0                                                                                                                       
unboundEGFR(0) = 500                                                                                                                   
activeSos(0) = 0                                                                                                                       
inactiveSos(0) = 1200                                                                                                                  
activeRas(0) = 0                                                                                                                       
inactiveRas(0) = 1200                                                                                                                  
cRaf(0) = 1500                                                                                                                         
cRafPP(0) = 0                                                                                                                          
MEK(0) = 3000                                                                                                                          
MEKPP(0) = 0                                                                                                                           
ERK(0) = 10000                                                                                                                         
ERKPP(0) = 0                                                                                                                           
removedSos(0) = 0                                                                                                                      
PKA(0) = 0                                                                                                                             
removedcRaf(0) = 0                                                                                                                     
inactivePKA(0) = 1000                                                                                                                  
EPAC(0) = 0                                                                                                                            
inactiveEPAC(0) = 1000                                                                                                                 
activeRap1(0) = 0                                                                                                                      
inactiveRap1(0) = 1200                                                                                                                 
BRaf(0) = 1500                                                                                                                         
BRafPP(0) = 0                                                                                                                          
activeC3G(0) = 0                                                                                                                       
inactiveC3G(0) = 1200                                                                                                                  
Gap(0) = 2400                                                                                                                          
EPACA(0) = 0                                                                                                                           
Cilostamide(0) = 0                                                                                                                     
PKAA(0) = 0                                                                                                                            
degradedEGFR(0) = 0

********** MODEL PARAMETERS
SosKcat = 0.0038910606000000002 {isParameter}                                                                              
SosKm = 8800.8253999999997 {isParameter}                                                                                   
reaction_0_Kcat = 12.874829 {isParameter}                                                                                  
reaction_0_Km = 6017.3339999999998 {isParameter}                                                                           
reaction_1_k1 = 5.4384395999999997 {isParameter}                                                                           
reaction_2_k1 = 7686.8037000000004 {isParameter}                                                                           
reaction_3_Km = 8378.1142999999993 {isParameter}                                                                           
reaction_3_V = 2123.2066 {isParameter}                                                                                     
reaction_4_Kcat = 0.088991476999999999 {isParameter}                                                                       
reaction_4_Km = 5422.7505000000001 {isParameter}                                                                           
reaction_5_Kcat = 25.048189000000001 {isParameter}                                                                         
reaction_5_Km = 4950.1746999999996 {isParameter}                                                                           
reaction_6_Kcat = 17.657982000000001 {isParameter}                                                                         
reaction_6_Km = 5671.3432000000003 {isParameter}                                                                           
reaction_7_Km = 4511.3239999999996 {isParameter}                                                                           
reaction_7_V = 13119.674000000001 {isParameter}                                                                            
reaction_8_Kcat = 7.1920631000000004 {isParameter}                                                                         
reaction_8_Km = 5007.5312999999996 {isParameter}                                                                           
reaction_9_Km = 6965.9207999999999 {isParameter}                                                                           
reaction_9_V = 632.51188000000002 {isParameter}                                                                            
reaction_10_Kcat = 0.64842801000000005 {isParameter}                                                                       
reaction_10_Km = 450.43603000000002 {isParameter}                                                                          
reaction_13_Kcat = 5.1647993999999997 {isParameter}                                                                        
reaction_13_Km = 12463.313 {isParameter}                                                                                   
reaction_14_Kcat = 12.010199 {isParameter}                                                                                 
reaction_14_Km = 12976.387000000001 {isParameter}                                                                          
reaction_15_Kcat = 1.6605186000000001 {isParameter}                                                                        
reaction_15_Km = 3102.3071 {isParameter}                                                                                   
reaction_16_Km = 9681.2042999999994 {isParameter}                                                                          
reaction_16_V = 13519.007 {isParameter}                                                                                    
reaction_17_Kcat = 4.0753418000000003 {isParameter}                                                                        
reaction_17_Km = 8741.8546999999999 {isParameter}                                                                          
reaction_18_Kcat = 9.3450833000000006 {isParameter}                                                                        
reaction_18_Km = 9699.8675999999996 {isParameter}                                                                          
reaction_19_Km = 8320.3472999999994 {isParameter}                                                                          
reaction_19_V = 872.75656000000004 {isParameter}                                                                           
reaction_20_Kcat = 0.017410964000000001 {isParameter}                                                                      
reaction_20_Km = 2013.9588000000001 {isParameter}                                                                          
reaction_21_Kcat = 12.41417 {isParameter}                                                                                  
reaction_21_Km = 3120.9279999999999 {isParameter}                                                                          
reaction_22_Kcat = 17.21106 {isParameter}                                                                                  
reaction_22_Km = 9665.2664999999997 {isParameter}                                                                          
reaction_23_Km = 571.28412000000003 {isParameter}                                                                          
reaction_23_V = 7.5931647 {isParameter}                                                                                    
reaction_24_Kcat = 0.20545005 {isParameter}                                                                                
reaction_24_Km = 685.66980000000001 {isParameter}                                                                          
reaction_25_Kcat = 2.9457857000000001 {isParameter}                                                                        
reaction_25_Km = 4991.8379000000004 {isParameter}                                                                          
reaction_26_V = 2109.864 {isParameter}                                                                                     
reaction_26_Km = 1075.3993 {isParameter}                                                                                   
reaction_27_Kcat = 2.0823794000000002 {isParameter}                                                                        
reaction_27_Km = 14421.281000000001 {isParameter}                                                                          
reaction_28_Kcat = 5.8584626000000002 {isParameter}                                                                        
reaction_28_Km = 2432.1788999999999 {isParameter}                                                                          
reaction_29_Km = 193.30439000000001 {isParameter}                                                                          
reaction_29_V = 462.00749999999999 {isParameter}                                                                                                   
reaction_30_k1 = 4.2997180000000004 {isParameter}                                                                        
cell = 1 {isCompartment:}

********** MODEL VARIABLES
y1 = ERKPP/(ERK+ERKPP)

********** MODEL REACTIONS
reaction_0 = reaction_0_Kcat * inactiveSos * boundEGFR / (reaction_0_Km + inactiveSos)                                                 
reaction_1 = reaction_1_k1 * EGF * unboundEGFR                                                                                         
reaction_2 = reaction_2_k1 * boundEGFR                                                                                                 
reaction_3 = reaction_3_V * activeSos / (reaction_3_Km + activeSos)                                                                    
reaction_4 = reaction_4_Kcat * inactiveRas * activeSos / (reaction_4_Km + inactiveRas)                                                 
reaction_5 = reaction_5_Kcat * activeRas * Gap / (reaction_5_Km + activeRas)                                                           
reaction_6 = reaction_6_Kcat * cRaf * activeRas / (reaction_6_Km + cRaf)                                                               
reaction_7 = reaction_7_V * cRafPP / (reaction_7_Km + cRafPP)                                                                          
reaction_8 = reaction_8_Kcat * MEK * cRafPP / (reaction_8_Km + MEK)                                                                    
reaction_9 = reaction_9_V * MEKPP / (reaction_9_Km + MEKPP)                                                                            
reaction_10 = reaction_10_Kcat * ERK * MEKPP / (reaction_10_Km + ERK)                                                                  
reaction_11 = SosKcat * inactiveSos * ERKPP / (SosKm + inactiveSos)                                                                    
reaction_12 = SosKcat * activeSos * ERKPP / (SosKm + activeSos)                                                                        
reaction_13 = reaction_13_Kcat * cRaf * PKA / (reaction_13_Km + cRaf)                                                                  
reaction_14 = reaction_14_Kcat * inactivePKA * PKAA / (reaction_14_Km + inactivePKA)                                                   
reaction_15 = reaction_15_Kcat * inactivePKA * Cilostamide / (reaction_15_Km + inactivePKA)                                            
reaction_16 = reaction_16_V * PKA / (reaction_16_Km + PKA)                                                                             
reaction_17 = reaction_17_Kcat * inactiveEPAC * EPACA / (reaction_17_Km + inactiveEPAC)                                                
reaction_18 = reaction_18_Kcat * inactiveEPAC * Cilostamide / (reaction_18_Km + inactiveEPAC)                                          
reaction_19 = reaction_19_V * EPAC / (reaction_19_Km + EPAC)                                                                           
reaction_20 = reaction_20_Kcat * inactiveRap1 * EPAC / (reaction_20_Km + inactiveRap1)                                                 
reaction_21 = reaction_21_Kcat * activeRap1 * Gap / (reaction_21_Km + activeRap1)                                                      
reaction_22 = reaction_22_Kcat * BRaf * activeRap1 / (reaction_22_Km + BRaf)                                                           
reaction_23 = reaction_23_V * BRafPP / (reaction_23_Km + BRafPP)                                                                       
reaction_24 = reaction_24_Kcat * MEK * BRafPP / (reaction_24_Km + MEK)                                                                 
reaction_25 = reaction_25_Kcat * inactiveC3G * boundEGFR / (reaction_25_Km + inactiveC3G)                                              
reaction_26 = reaction_26_V * activeC3G / (reaction_26_Km + activeC3G)                                                                 
reaction_27 = reaction_27_Kcat * inactiveRap1 * activeC3G / (reaction_27_Km + inactiveRap1)                                            
reaction_28 = reaction_28_Kcat * BRaf * activeRas / (reaction_28_Km + BRaf)                                                            
reaction_29 = reaction_29_V * ERKPP / (reaction_29_Km + ERKPP)                                                                         
reaction_30 = boundEGFR * reaction_30_k1

********** MODEL FUNCTIONS


********** MODEL EVENTS


********** MODEL MATLAB FUNCTIONS

