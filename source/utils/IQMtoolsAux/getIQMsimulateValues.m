function values = getIQMsimulateValues(nsol, name)
%GETIQMSIMULATEVALUES ...
%
    % try variables first
    idx = find(strcmp(nsol.variables,name));
    if ~isempty(idx)
        values = nsol.variablevalues(:,idx);
        return
    end
    % try states next
    idx = find(strcmp(nsol.states,name));
    if ~isempty(idx)
        values = nsol.statevalues(:,idx);
        return
    end
    % report error
    error('GETIQMSIMULATEVALUES:InvalidName', sprintf('Invalid model variable or state name: ''%s''',name));
end
