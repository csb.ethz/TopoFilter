function [ datamatrices, names ] = decoupleIQMmeasurementdata( sbexp )
%DECOUPLEMEASUREMENTDATA Return cell array of single observable measurements
%  with time point, value, min. value and max. value columns. String names of
%  observables are returned as a second argument.
%
%  Any measurement with missing value, min value or max value is skipped.
%  In consequence number of measurements per observable (number of rows in data
%  matrix) may differ between observables.
%

    N = nargout(@IQMmeasurementdata);
    assert(N >= 5, 'IQMmeasurementdata interface changed?');

    d = cell(N,1);
    [d{1:N}] = IQMmeasurementdata(sbexp);

    names = d{2};
    n = numel(names);
    datamatrices = cell(1,n);

    tps = d{1};
    vals = d{3};
    minvals = d{4};
    maxvals = d{5};

    nonempty = false(1,n);
    for k = 1:n
        dm = [tps vals(:,k) minvals(:,k) maxvals(:,k)];
        datamatrices{k} = rmNan(dm,1);
        if ~isempty(dm)
            nonempty(k) = true;
        end
    end
    names = names(nonempty);
    datamatrices = datamatrices(nonempty); % not {}, to keep singleton cell arrays

end
