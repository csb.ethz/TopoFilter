function [ bool ] = istabfield(tab, name)
%ISTABFIELD Equivalent of `isfield` for MATLAB tables.
%
%    Note: most likely inefficient so for bigger testing try `table2struct`.
%
    bool = any(strcmp(fieldnames(tab),name));

end
