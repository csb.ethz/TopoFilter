function [S] = strjoin2(C, varargin)
%STRJOIN2 strjoin that works also when C is column, matrix or nested cell array
% of strings,
%
% Examples:
%    strjoin({'a'; 'b'}, ',')
%    strjoin2({'a'; 'b'}, ',')
%    strjoin2({'a' 'c'; 'b' 'd'}, ',')
%    strjoin2({'a' {'b' {'c' 'd'} 'eee'} 'ff'}, ',')
%
    S = strjoin(flattenCell(C), varargin{:});
end
