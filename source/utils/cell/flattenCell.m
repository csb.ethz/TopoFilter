function C = flattenCell(A)
%FLATTENCELL Flatten cell array from:
% http://www.mathworks.com/matlabcentral/fileexchange/27009-flatten-nested-cell-arrays
% with horzcat to cat upgrade from comments.
%
% Examples:
%
%     flattenCell({{1 {2 3}} {4 5} 6})
%     flattenCell({{'a' {'b','c'}} {'d' 'e'} 'f'})
%     flattenCell({{1 {2 3}} 'd' {'e' 'f'} {7 @isempty 9} 10})
%

C = {};
for i=1:numel(A)
    if(~iscell(A{i}))
        Ctail = A(i);
    else
        Ctail = flattenCell(A{i});
    end
    C = cat(2, C, Ctail);
end

end
