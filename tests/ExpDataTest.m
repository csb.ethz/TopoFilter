function tests = ExpDataTest()
%EXPDATATEST Unit tests for ExpData class.
%
% Run with:
%   runtests('ExpDataTest')
%
    tests = functiontests(localfunctions);
end

function setupOnce(testCase)
    testCase.TestData.toyExpFolder = '../examples/toy-ligand-fluorescent';
    testCase.TestData.toyExpDataFilename= 'expData.xls';
end

% function teardownOnce(testCase)
% end


%% Tests

function testToyExpData(testCase)
    import matlab.unittest.fixtures.CurrentFolderFixture
    testCase.applyFixture(CurrentFolderFixture(testCase.TestData.toyExpFolder));
    settings.expDataFilename = testCase.TestData.toyExpDataFilename;

    % Load data using IQMtools API
    measurments = IQMmeasurement(settings.expDataFilename);
    [tps, observables, values, minvals, maxvals] = IQMmeasurementdata(measurments);

    % Load data using ExpData
    expData = loadExpData(settings);

    % Assert equivalence
    assertEqual(testCase, [expData.tps{:}],tps, 'Time points fail.');
    assertEqual(testCase, expData.experiments.observables, observables, 'Names fail.');
    assertEqual(testCase, expData.values, values, 'Names fail.');
    assertEqual(testCase, expData.sd, (maxvals-minvals)/2, 'Names fail.');
end

% TODO: add test for ExpData functions opts; include:
% * using `calcThresholdFcn = @calcThresholdSd` (legacy method) with default args
% * using `calcCostFnc` with custom args
% * same for other two func options
