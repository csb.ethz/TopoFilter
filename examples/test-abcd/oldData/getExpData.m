function expData = readExpData(tps, stdev)

fid = fopen('expDataGeneratedExact300.txt');

% First line contains time points
tmpLine = fgetl(fid);
expData.timePoints = cell2mat(textscan(tmpLine, '%f'));
numTimePoints = length(expData.timePoints);

expData.timePoints = expData.timePoints(numTimePoints/tps:numTimePoints/tps:end);

% Second line contains names of observable species
tmpLine = fgetl(fid);
tmpCell = textscan(tmpLine, '%s');
expData.observables = tmpCell{1};
numObservables = length(expData.observables);

% The following numObservable lines each contain numTimePoints measurements
expData.measurements = zeros(numObservables, tps);
for i = 1:numObservables
    tmpLine = fgetl(fid);
    tmpRow = cell2mat(textscan(tmpLine, '%f'));
    expData.measurements(i,:) = tmpRow(numTimePoints/tps:numTimePoints/tps:end);
end

expData.S = zeros(numObservables*tps);
baseError = max(expData.measurements,[],2)*0.02;
for i = 1:tps
    for j = 1:numObservables
        pos = (i-1)*numObservables + j;
        expData.S(pos, pos) = (expData.measurements(j, i)*stdev + baseError(j))^2;
    end
end

fclose(fid);