function [ optStruct ] = getViableProjectionsOptions( settings )
%GETVIABLEPROJECTIONSOPTIONS Prepare options struct for the
%  `getViableProjections` function.
%

optStruct.parallelize = settings.parallelize;
optStruct.numWorkers = settings.numWorkers;

optStruct.couplingMatrix = settings.couplingMatrix;
optStruct.paramCouplingApplyFcn = settings.paramCouplingApplyFcn;

optStruct.dropOATProjections = settings.dropOATProjections;
optStruct.projectionSetCls = settings.projectionSetCls;

optStruct.exhaustiveRank = settings.exhaustiveRank;
optStruct.backtrack = settings.backtrack;
optStruct.pointsAsOuter = settings.pointsAsOuter;
% optStruct.useOATheuristics = settings.useOATheuristics;

end
