# Example: biochemical adaptation

## Info

This is a small representation of the case study described in:

> Lormeau, C., et al. (2017) "Multi-objective design of synthetic biological
> circuits", IFAC-PapersOnLine. (20th IFAC World Congress), 50(1), pp.
> 9871–9876. DOI: 10.1016/j.ifacol.2017.08.1601.

In order to illustrate the case study in a short runtime the representation
includes 4 activation/repression pattern root models for the filtering, which is
set up with a small sample size. The actual case study was run: a) for 512
different models enumerating all possible activation/repression signs, b) with
large enough sample sizes to discover as much projections as possible and to
estimate scoring measures as reliable as possible.

## Running

You need to run in this order and in this folder:

    >> adaptation_main
    >> adaptation_merge
    >> adaptation_rank

Note: to avoid merging results from different runs, call first:

    >> adaptation_cleanup

The functions/scripts above use the following staging folders that cleanup
deletes:
 * `adaptation_main` - out: "results/"
 * `adaptation_merge` - in: "results/"; out: "merged/"
 * `adaptation_rank` - in: "merged"; out: "OutV", "Arrays/"
