function [ essentialParameters, projections, paramSamples, p0, error0 ] = ...
    topologicalFilteringWrapper( ...
        paramSpecs, threshold, dryRun, topoFilterOPTIONS, evalModelOPTIONS )
%TOPOLOGICALFILTERINGWRAPPER Do the topological filtering, and prepare results
%

debugging = (debuglevel > 0);

    % Transform parameters to log space where required
    paramSpecs = logParamSpecs(paramSpecs);

    % Reset evalModel options due to paramSpecs change
    evalModelOPTIONS = EvalModelOptions(evalModelOPTIONS.mexNames, ...
        evalModelOPTIONS.expData, paramSpecs, evalModelOPTIONS.odeIntegratorOptions);

    % Get a starting point for the parameter space exploration
    [p0, error0] = getViableInitPoint(paramSpecs, threshold);

    if debugging
        fprintf('[DEBUG] topologicalFilteringWrapper, p0 = %s (log-space = %s).\n', mat2str(p0), mat2str(int8(paramSpecs.islog)));
    end

    % Perform topological filtering
    if dryRun
        d = size(p0, 2);
        projections = false(0, d);
        paramSamples = zeros(0 ,d);
    else
        viableProjections = topologicalFiltering(threshold, p0, ...
            topoFilterOPTIONS, paramSpecs, evalModelOPTIONS);
        projections = viableProjections.rowmat;
        paramSamples = viableProjections.parrowmat;
    end

    % Transform parameter values back from the log space
    paramSamples(:, evalModelOPTIONS.inLogSpace) = 10.^paramSamples(:, evalModelOPTIONS.inLogSpace);
    p0(:, evalModelOPTIONS.inLogSpace) = 10.^p0(:, evalModelOPTIONS.inLogSpace);

    % Change parameter values to projection values
    for i = 1:size(projections, 1)
        paramSamples(i, projections(i, :)) = evalModelOPTIONS.paramFixValues(projections(i, :));
    end

    % Extract essential parameters
    essentialParameterIdxs = ~any(projections, 1);
    if (any(essentialParameterIdxs)) % if there are any essential parameters
        essentialParameters = paramSpecs.names(essentialParameterIdxs);
    else
        essentialParameters = [];
    end

end

% Move to an ParamteresSpecification object if such will be created.
function paramSpecs = logParamSpecs(paramSpecs)
    % Requirement: if the `islog` is set to true all the respective param
    % boundaries `bmin`/`bmax` initial point `p0` and model values `values` must
    % be strictly positive, but the projection values `projections` might be
    % equal to 0.
    % Because of the latter and because the projection values are only used in
    % model evaluation, NOT mapping projection values into the log space.
    validateLogParamSpecs(paramSpecs)
    paramSpecs.bmin(paramSpecs.islog) = log10(paramSpecs.bmin(paramSpecs.islog));
    paramSpecs.bmax(paramSpecs.islog) = log10(paramSpecs.bmax(paramSpecs.islog));
    paramSpecs.p0(paramSpecs.islog, :) = log10(paramSpecs.p0(paramSpecs.islog, :));
    paramSpecs.values(paramSpecs.islog) = log10(paramSpecs.values(paramSpecs.islog));
end

function [] = validateLogParamSpecs(paramSpecs)
    specName = {'bmin', 'bmax', 'values'};
    specDesc = {'lower bound', 'upper bound', 'model value'};
    paramName = paramSpecs.names;
    paramIdx = find(paramSpecs.islog);
    for i=1:numel(paramIdx)
        pIdx = paramIdx(i);
        if (any(paramSpecs.p0(pIdx, :) <= 0))
            error('VALIDATELOGPARAMSPECS:InvalidParamSpecs',...
                'Parameter "%s" to be analysed in log-space, but it also has a non-positive initial value value specified\n',...
                paramName{pIdx});
        end
        for specIdx = 1:numel(specName)
            if (paramSpecs.(specName{specIdx})(pIdx) <= 0)
                error('VALIDATELOGPARAMSPECS:InvalidParamSpecs',...
                    'Parameter "%s" to be analysed in log-space, but it also has a non-positive %s value specified\n',...
                    paramName{pIdx}, specDesc{specIdx});
            end
        end
    end
end
