function [ idxs, values ] = getIQMparametersProps(model, names)
% GETIQMPARAMETERSPROPS ...
%
%

    n = numel(names);
    idxs = zeros(n,1);
    values = zeros(n,1);
    [modelNames,modelValues] = IQMparameters(model);
    for i = 1:n
        newIdx = find(strcmp(modelNames,names(i)));
        if (~isempty(newIdx))
            idxs(i) = newIdx;
            values(i) = modelValues(newIdx);
        else
            error('GETIQMPARAMETERSPROPS:InvalidName', 'Invalid name: parameter ''%s'' not found in the model.', names{i});
        end
    end

end
