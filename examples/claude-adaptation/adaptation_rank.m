function[] = adaptation_rank()
% Based on TFrankProjections.m script

%debuglevel(2);
%diary(sprintf('debug_%s.txt',datestr(now, 'yymmdd_HHMM')));

    % Init parpool & rand seed
    numWorkers = 0;
    try
        ppool = gcp('nocreate'); % If no pool, do not create new one.
        if ~isempty(ppool)
            numWorkers = ppool.NumWorkers;
        end
    catch ME
        warning('Parallel Computing Toolbox not found.\nThe follwoing error was caught while calling gcp:\n\t%s: %s\n',ME.identifier, ME.message);
    end
    rng('shuffle')


    %% setup

    InputFolderPath = 'merged';
    fileNames = dir(fullfile(InputFolderPath,'*.mat'));
    fileNames = {fileNames.name};

    OutVFolderPath = 'OutV';
    mkdir(OutVFolderPath)
    ArraysFolderPath = 'Arrays';

    assert(~isempty(fileNames),'No files found. Are you in the correct folder? If yes, check the path and the file name pattern?')


    %% loop on all projections
    nMod = numel(fileNames);
    %nMod = 1;

    parfor (iPar=1:nMod, numWorkers)
    %for iPar=1:nMod

        %% load topology
        fn = fileNames{iPar};
        loaded = load(fullfile(InputFolderPath,fn));
        toponame = fn(1:18);
        projinfo = loaded.projinfo;
        points = projinfo.viablePoints;
        modelname = ['model_' projinfo.models{1} '.txtbc'];

        %% status
        fprintf('TFrankProjections, Volint #%d/%d, projection = %s .. ',iPar,nMod,fn);

        %% set topovector and check motifs

        topovector = zeros(1,9);
        for k = 2:2:18
            topovector(k/2) = str2double(toponame((k-1):k));
        end

        sAA = topovector(1);
        sAB = topovector(2);
        sAC = topovector(3);
        sBA = topovector(4);
        sBB = topovector(5);
        sBC = topovector(6);
        sCA = topovector(7);
        sCB = topovector(8);
        sCC = topovector(9);

        IFF = (sBC*sAB == -sAC);
        NFBLB = (min([sAB*sBC*sCA sAC*sCB*sBA sAB*sBA sBC*sCB]) == -1);


        %% complexity

        bmin = [ 0.01  0.01   0.01   0.01  0.01  0.01  0.01  0.01  0.01 0.01   0.01 0.01  0.01 ...
                          0.001 0.001   0.001 0.001 0.001 0.001 0.001 0.001 0.001 0.001 0.001 0.001 0.001 ];
        bmax = [  10    10    10    10    10    10    10    10    10    10    10    10    10   ...
                           10000 10000 10000 10000 10000 10000 10000 10000 10000 10000 10000 10000 10000 ];


        Projected = [zeros(1,17) topovector==0];
        bmin_iPar = log10(bmin(~Projected));
        bmax_iPar = log10(bmax(~Projected));


        ParamNumber = length(bmin_iPar);
        Complexity = ParamNumber - 17; % maximum 9 interactions




        %% OutV

        %% cost function & integration pieces
        settings = {};
        settings.expFilename = {'experiment.exp'};
        settings.expDataFilename = 'expData.xls';
        settings.paramSpecsFilename = 'paramSpecs.txt';
        settings.calcCostFcn = @adaptation_calcCost;
        settings.extractSimDataFcn = @adaptation_extractSimData;
        threshold = adaptation_calcThreshold();
        %paramSpecs = loadParamSpecs(settings);
        expData = loadExpData(settings);
        experiments = loadExperiments(settings);
        nsamp = 1e3; % consider also in each iteration: nsamp = 10*npoints


        % load saved viable points

        %% prepare model
        settings.modelFilename = modelname;
        model = loadModel(settings);
        nExp = numel(experiments);
        mexNames = cell(nExp,1);
        mexCleanups = cell(nExp,1);
        for iExp = 1:nExp
            experiment = experiments{iExp};
            expmodel = IQMmergemodexp(model,experiment);
            [mexNames{iExp}, mexCleanups{iExp}] = mexModel(expmodel,settings,iExp);
        end

        %% prepare points
        variableParamValuesHeadRowmat = points.rowmat;
        [npoints,nparvar] = size(variableParamValuesHeadRowmat);
        if ~isfield(points, 'projected')
            projected.names = {};
            projected.values = [];
        else
            projected = points.projected;
        end
        nparfix = length(projected.names);
        paramNames = vertcat(points.colnames, projected.names);
        npartot = numel(paramNames);
        fixedParamValuesTail = projected.values;
        if ~isfield(points, 'islog') || isempty(points.islog)
            islog = false(1,nparvar);
        else
            islog = points.islog;
        end

        % setup persistent data in the cost function
        opts = struct();
        opts.solNrTimepoints = expData.maxT; % time step = 1
        opts.simOpts = struct();
        opts.simOpts.reltol = 1e-8;
        opts.simOpts.abstol = 1e-8;
        opts.simOpts.maxnumsteps = 1e6;
        evalModelDirect(mexNames, expData, paramNames, islog, fixedParamValuesTail, opts);

        %% Get bmin and bmax

        bmax_volint = max(variableParamValuesHeadRowmat,[],1);
        bmin_volint = min(variableParamValuesHeadRowmat,[],1);

        %% Compute posterior distribution assuming unifrom priors

        %pTemp = [1.0000   -1.8913   -1.8517    0.7974    0.9642   -0.8600    0.4346    0.8143   -1.9133   -1.9511   -0.5406   -0.2958   -0.6055 -2.9910    3.8271   -2.2688    2.0623   -2.6277    0.0281   -2.9832];


        OutV = Volint(@evalModelDirect, threshold, variableParamValuesHeadRowmat, bmax_volint, bmin_volint, nsamp);
        iSaveOutV(fullfile(OutVFolderPath,sprintf('OutV_%d',iPar)),OutV);

        %% calculate performance

        N = length(OutV.V);
        f = sum(exp(-OutV.cost))/N;
        f2 = sum(exp(-2*OutV.cost))/N;
        PerfError = sqrt((f2 - f^2)/N);
        Performance = f;

        %% calculate robustness

        Robustness = log(OutV.vol/prod(bmax_iPar-bmin_iPar));
        RobError =  OutV.err/OutV.vol;



        %% feasibility

        pmax = zeros(1,ParamNumber);
        pmin = zeros(1,ParamNumber);

        for l = 1:ParamNumber
            pmax(l) = max([-1e5; OutV.V(:,l)]);
            pmin(l) = min([1e9; OutV.V(:,l)]);
        end

        Feasibility = min((pmax - pmin)./(bmax_iPar-bmin_iPar));

        %% output

        ListTopo_id{iPar} = iPar;
        ListTopo_npoints{iPar} = length(OutV.V);
        ListTopo_topovector{iPar} = topovector;
        ListTopo_IFF{iPar} = IFF;
        ListTopo_NFBLB{iPar} = NFBLB;
        ListTopo_Complexity{iPar} = Complexity;
        ListTopo_Feasibility{iPar} = Feasibility;
        ListTopo_Performance{iPar} = Performance;
        ListTopo_PerfError{iPar} = PerfError;
        ListTopo_Robustness{iPar} = Robustness;
        ListTopo_RobError{iPar} = RobError;


        %status
        fprintf('done\n');
    end

    ListTopo = struct('id',ListTopo_id,'npoints', ListTopo_npoints,'topovector',ListTopo_topovector,'IFF',ListTopo_IFF,'NFBLB',ListTopo_NFBLB,'Complexity',ListTopo_Complexity,'Feasibility',ListTopo_Feasibility,'Performance',ListTopo_Performance,'PerfError',ListTopo_PerfError,'Robustness',ListTopo_Robustness,'RobError',ListTopo_RobError);
    mkdir(ArraysFolderPath);
    save(fullfile(ArraysFolderPath,'ListTopo.mat'),'ListTopo');

end
